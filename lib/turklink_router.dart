import 'package:flutter/material.dart';
import 'package:turklink/views/pages/auth/forget_password_screen.dart';
import 'package:turklink/views/pages/auth/login_screen.dart';
import 'package:turklink/views/pages/auth/register_screen.dart';
import 'package:turklink/views/pages/main/main_screen.dart';
import 'package:turklink/views/pages/main/my_properties/add_property_screen.dart';
import 'package:turklink/views/pages/main/notification/notification_screen.dart';
import 'package:turklink/views/pages/splash_screen.dart';

class TurklinkRouter {

  static const String Splash = '/splash';
  static const String Register = '/register';
  static const String Login = '/login';
  static const String ForgetPassword = '/forgetPassword';
  static const String Main = '/main';
  static const String MyPropertyDetails = '/myPropertyDetails';
  static const String AddProperty = '/addPropery';
  static const String Requests = '/requests';
  static const String Notification = '/notification';

  static Map<String, WidgetBuilder> getRoutes() {
    return {
      TurklinkRouter.Splash: (context) => SplashScreen(),
      TurklinkRouter.Register: (context) => RegisterScreen(),
      TurklinkRouter.Login: (context) => LoginScreen(),
      TurklinkRouter.ForgetPassword: (context) => ForgetPasswordScreen(),
      TurklinkRouter.Main: (context) => MainScreen(),
      TurklinkRouter.AddProperty: (context) => AddPropertyScreen(),
      TurklinkRouter.Notification: (context) => NotificationScreen(),
    };
  }
}