import 'dart:convert';

class NotificaitonModel{
 String? id;
 String? userId;
 String? title_tr;
 String? title_en;
 String? title_ar;
 String? message_tr;
 String? message_en;
 String? message_ar;
 DateTime? notificationDate;
 String? notificationType;

 NotificaitonModel({
  this.id,
  this.userId,
  this.title_tr,
  this.title_en,
  this.title_ar,
  this.message_tr,
  this.message_en,
  this.message_ar,
  this.notificationDate,
  this.notificationType,
 });


 factory NotificaitonModel.fromJson(Map<String,dynamic> parsedJson){
  return NotificaitonModel(
    id : parsedJson['id'] == null ? null : parsedJson['id'],
    userId : parsedJson['userId'] == null ? null : parsedJson['userId'],
    title_tr : parsedJson['title_tr'] == null ? null : parsedJson['title_tr'],
    title_en : parsedJson['title_en'] == null ? null : parsedJson['title_en'],
    title_ar : parsedJson['title_ar'] == null ? null : parsedJson['title_ar'],
    message_tr : parsedJson['message_tr'] == null ? null : parsedJson['message_tr'],
    message_en : parsedJson['message_en'] == null ? null : parsedJson['message_en'],
    message_ar : parsedJson['message_ar'] == null ? null : parsedJson['message_ar'],
    notificationDate : parsedJson['notificationDate'] == null ? null : parsedJson['notificationDate'].toDate(),
    notificationType : parsedJson['notificationType'] == null ? null : parsedJson['notificationType'],
  );
 }

 static List<NotificaitonModel> listFromJson(List<dynamic> list) {
  List<NotificaitonModel> rows = list.map((i) => NotificaitonModel.fromJson(i)).toList();
  return rows;
 }

 static List<NotificaitonModel> listFromString(String responseBody) {
  final parsed = json.decode(responseBody).cast<Map<String, dynamic>>();
  return parsed.map<NotificaitonModel>((json) => NotificaitonModel.fromJson(json)).toList();
 }

  Map<String,dynamic> toJson()=>{
   'id' : id,
   'userId' : userId,
   'title_tr' : title_tr,
   'title_en' : title_en,
   'title_ar' : title_ar,
   'message_tr' : message_tr,
   'message_en' : message_en,
   'message_ar' : message_ar,
   'notificationDate' : notificationDate,
   'notificationType' : notificationType,
 };

}