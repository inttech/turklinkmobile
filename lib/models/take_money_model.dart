import 'dart:convert';

class TakeMoneyModel{
  String? id;
  String? userId;
  int? status;
  int? isDeleted;
  DateTime? createdDate;
  DateTime? approvedDate;
  String? iban;
  int? amount;

  TakeMoneyModel({
    this.id,
    this.userId,
    this.status,
    this.isDeleted,
    this.createdDate,
    this.approvedDate,
    this.iban,
    this.amount
  });

  factory TakeMoneyModel.fromJson(Map<String,dynamic> parsedJson){
  return TakeMoneyModel(
    id : parsedJson['id'] == null ? null : parsedJson['id'],
    userId : parsedJson['userId'] == null ? null : parsedJson['userId'],
    status : parsedJson['status'] == null ? null : parsedJson['status'],
    createdDate : parsedJson['createdDate'] == null ? null : parsedJson['createdDate'].toDate(),
    approvedDate : parsedJson['approvedDate'] == null ? null : parsedJson['approvedDate'].toDate(),
    isDeleted : parsedJson['isDeleted'] == null ? null : parsedJson['isDeleted'],
    iban : parsedJson['iban'] == null ? null : parsedJson['iban'],
    amount : parsedJson['amount'] == null ? null : parsedJson['amount'],
  );
 }

 static List<TakeMoneyModel> listFromJson(List<dynamic> list) {
  List<TakeMoneyModel> rows = list.map((i) => TakeMoneyModel.fromJson(i.data())).toList();
  return rows;
 }

 static List<TakeMoneyModel> listFromString(String responseBody) {
  final parsed = json.decode(responseBody).cast<Map<String, dynamic>>();
  return parsed.map<TakeMoneyModel>((json) => TakeMoneyModel.fromJson(json)).toList();
 }

 Map<String,dynamic> toJson()=>{
   'id' : id,
   'userId' : userId,
   'status' : status,
   'createdDate' : createdDate,
   'approvedDate' : approvedDate,
   'isDeleted' : isDeleted,
   'iban' : iban,
   'amount' : amount,
 };

}