import 'dart:convert';

class CreditCardsPaymentModel {
  String? id;
  String? userId;
  int? financeType;
  int? amount;
  String? financeDescription;
  String? financeDescriptionText;
  String? propertyId;
  String? requestId;
  DateTime? operationDate;
  String? userEmail;
  String? userName;
  String? phone;
  int? status;

  CreditCardsPaymentModel(
      {this.id,
      this.userId,
      this.userEmail,
      this.financeType,
      this.userName,
      this.amount,
      this.financeDescription,
      this.propertyId,
      this.requestId,
      this.operationDate,
      this.phone,
      this.status,
      this.financeDescriptionText});

  factory CreditCardsPaymentModel.fromJson(Map<String, dynamic> parsedJson) {
    return CreditCardsPaymentModel(
      id: parsedJson['id'] == null ? null : parsedJson['id'],
      userId: parsedJson['userId'] == null ? null : parsedJson['userId'],
      userEmail:
          parsedJson['userEmail'] == null ? null : parsedJson['userEmail'],
      userName: parsedJson['userName'] == null
          ? null
          : parsedJson['userName'],
      financeType:
          parsedJson['financeType'] == null ? null : parsedJson['financeType'],
      amount: parsedJson['amount'] == null ? null : parsedJson['amount'],
      financeDescription: parsedJson['financeDescription'] == null
          ? null
          : parsedJson['financeDescription'],
      propertyId:
          parsedJson['propertyId'] == null ? null : parsedJson['propertyId'],
      requestId:
          parsedJson['requestId'] == null ? null : parsedJson['requestId'],
      phone:
          parsedJson['phone'] == null ? null : parsedJson['phone'],
      operationDate: parsedJson['operationDate'] == null
          ? null
          : parsedJson['operationDate'].toDate(),
      status:
          parsedJson['status'] == null ? null : parsedJson['status'],
      financeDescriptionText:
          parsedJson['financeDescriptionText'] == null ? null : parsedJson['financeDescriptionText'],
    );
  }

  static List<CreditCardsPaymentModel> listFromJson(List<dynamic> list) {
    List<CreditCardsPaymentModel> rows =
        list.map((i) => CreditCardsPaymentModel.fromJson(i)).toList();
    return rows;
  }

  static List<CreditCardsPaymentModel> listFromString(String responseBody) {
    final parsed = json.decode(responseBody).cast<Map<String, dynamic>>();
    return parsed
        .map<CreditCardsPaymentModel>((json) => CreditCardsPaymentModel.fromJson(json))
        .toList();
  }

  Map<String, dynamic> toJson() => {
        'id': id,
        'userId': userId,
        'userEmail': userEmail,
        'userName': userName,
        'financeType': financeType,
        'amount': amount,
        'financeDescription': financeDescription,
        'propertyId': propertyId,
        'requestId': requestId,
        'phone': phone,
        'operationDate': operationDate,
        'status': status,
        'financeDescriptionText': financeDescriptionText,
      };
}
