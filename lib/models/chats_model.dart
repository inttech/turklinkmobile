import 'dart:convert';

class ChatsModel{
  String? id;
  String? userId;
  int? status;
  int? isDeleted;
  int? date;
  String? filePath;
  String? fileType;
  String? message;

  ChatsModel({
    this.id,
    this.userId,
    this.status,
    this.isDeleted,
    this.filePath,
    this.fileType,
    this.date,
    this.message
  });

  factory ChatsModel.fromJson(Map<String,dynamic> parsedJson){
  return ChatsModel(
    id : parsedJson['id'] == null ? null : parsedJson['id'],
    userId : parsedJson['userId'] == null ? null : parsedJson['userId'],
    status : parsedJson['status'] == null ? null : parsedJson['status'],
    date : parsedJson['date'] == null ? null : parsedJson['date'],
    isDeleted : parsedJson['isDeleted'] == null ? null : parsedJson['isDeleted'],
    filePath : parsedJson['filePath'] == null ? null : parsedJson['filePath'],
    fileType : parsedJson['fileType'] == null ? null : parsedJson['fileType'],
    message : parsedJson['message'] == null ? null : parsedJson['message'],
  );
 }

 static List<ChatsModel> listFromJson(List<dynamic> list) {
  List<ChatsModel> rows = list.map((i) => ChatsModel.fromJson(i.data())).toList();
  return rows;
 }

 static List<ChatsModel> listFromString(String responseBody) {
  final parsed = json.decode(responseBody).cast<Map<String, dynamic>>();
  return parsed.map<ChatsModel>((json) => ChatsModel.fromJson(json)).toList();
 }

 Map<String,dynamic> toJson()=>{
   'id' : id,
   'userId' : userId,
   'status' : status,
   'date' : date,
   'isDeleted' : isDeleted,
   'filePath' : filePath,
   'fileType' : fileType,
   'message' : message,
 };

}