import 'dart:convert';
import 'dart:io';

import 'package:turklink/models/key_value_model.dart';

class PropertiesModel{
 String? id;
 String? userId;
 int? status;
 int? isDeleted;
 DateTime? createdDate;
 DateTime? updatedDate;
 String? title;
 String? imagePath;
 String? deedPath; // tapu foroğraf pathi
 String? rentContractPath; 
 KeyValueModel? city;
 KeyValueModel? district;
 KeyValueModel? place;
 String? neighborhood;
 String? street;
 String? addressNumber;
 String? doorNumber;
 KeyValueModel? propertyType;
 KeyValueModel? propertyRoom;
 String? propertySize;
 KeyValueModel? propertyFurnished;
 KeyValueModel? propertyHirer;
 KeyValueModel? propertyIsPersonelUse;
 DateTime? leaseDate;
 int? leaseStatus;
 int? leaseAmount;
 int? leaseTerm;
 int? leaseProcessStatus;
 String? uavtCode;
 String? contractPath;
 int? estimatedRentIncome;
 int? propertyTaxYear;
 int? propertyTaxInstallment;
 int? propertyTaxTxStatus;
 int? propertyTaxAmount;
 int? propertyTaxProcessStatus;
 String? earthquakeInsurancePath;
 int? earthquakeInsuranceAmount;
 int? earthquakeProcessStatus;
 int? managementFee;
 int? propertyRentAmount;
 String? propertyRentFinishDate;
 String? propertyLinkSahibinden;

 File? imageFile;
 File? deedFile;
 File? rentContractFile;

 PropertiesModel({
  this.id,
  this.userId,
  this.status,
  this.isDeleted,
  this.createdDate,
  this.updatedDate,
  this.title,
  this.imagePath,
  this.deedPath,
  this.rentContractPath,
  this.city,
  this.district,
  this.place,
  this.neighborhood,
  this.street,
  this.addressNumber,
  this.doorNumber,
  this.propertyType,
  this.propertyRoom,
  this.propertySize,
  this.propertyFurnished,
  this.propertyHirer,
  this.propertyIsPersonelUse,
  this.leaseStatus,
  this.leaseDate,
  this.leaseTerm,
  this.leaseAmount,
  this.leaseProcessStatus,
  this.uavtCode,
  this.contractPath,
  this.estimatedRentIncome,
  this.propertyTaxYear,
  this.propertyTaxInstallment,
  this.propertyTaxAmount,
  this.propertyTaxTxStatus,
  this.propertyTaxProcessStatus,
  this.earthquakeInsurancePath,
  this.earthquakeInsuranceAmount,
  this.earthquakeProcessStatus,
  this.managementFee,
  this.propertyRentAmount,
  this.propertyRentFinishDate,
  this.propertyLinkSahibinden,

  this.imageFile,
  this.deedFile,
  this.rentContractFile
 });


 factory PropertiesModel.fromJson(Map<String,dynamic> parsedJson){
  return PropertiesModel(
    id : parsedJson['id'] == null ? null : parsedJson['id'],
    userId : parsedJson['userId'] == null ? null : parsedJson['userId'],
    status : parsedJson['status'] == null ? null : parsedJson['status'],
    isDeleted : parsedJson['isDeleted'] == null ? null : parsedJson['isDeleted'],
    createdDate : parsedJson['createdDate'] == null ? null : parsedJson['createdDate'].toDate(),
    updatedDate : parsedJson['updatedDate'] == null ? null : parsedJson['updatedDate'].toDate(),
    title : parsedJson['title'] == null ? null : parsedJson['title'],
    imagePath : parsedJson['imagePath'] == null ? null : parsedJson['imagePath'],
    deedPath : parsedJson['deedPath'] == null ? null : parsedJson['deedPath'],
    rentContractPath : parsedJson['rentContractPath'] == null ? null : parsedJson['rentContractPath'],
    city : parsedJson['city'] == null ? null : KeyValueModel.fromJson(parsedJson['city']),
    district : parsedJson['district'] == null ? null : KeyValueModel.fromJson(parsedJson['district']),
    place : parsedJson['place'] == null ? null : KeyValueModel.fromJson(parsedJson['place']),
    neighborhood : parsedJson['neighborhood'] == null ? null : parsedJson['neighborhood'],
    street : parsedJson['street'] == null ? null : parsedJson['street'],
    addressNumber : parsedJson['addressNumber'] == null ? null : parsedJson['addressNumber'],
    doorNumber : parsedJson['doorNumber'] == null ? null : parsedJson['doorNumber'],
    propertyType : parsedJson['propertyType'] == null ? null :  KeyValueModel.fromJson(parsedJson['propertyType']),
    propertyRoom : parsedJson['propertyRoom'] == null ? null : KeyValueModel.fromJson(parsedJson['propertyRoom']),
    propertySize : parsedJson['propertySize'] == null ? null : parsedJson['propertySize'],
    propertyFurnished : parsedJson['propertyFurnished'] == null ? null : KeyValueModel.fromJson(parsedJson['propertyFurnished']),
    propertyHirer : parsedJson['propertyHirer'] == null ? null : KeyValueModel.fromJson(parsedJson['propertyHirer']),
    propertyIsPersonelUse : parsedJson['propertyIsPersonelUse'] == null ? null : KeyValueModel.fromJson(parsedJson['propertyIsPersonelUse']),
    leaseDate : parsedJson['leaseDate'] == null ? null : parsedJson['leaseDate'].toDate(),
    leaseStatus : parsedJson['leaseStatus'] == null ? null : parsedJson['leaseStatus'],
    leaseProcessStatus : parsedJson['leaseProcessStatus'] == null ? null : parsedJson['leaseProcessStatus'],
    leaseAmount : parsedJson['leaseAmount'] == null ? null : parsedJson['leaseAmount'],
    leaseTerm : parsedJson['leaseTerm'] == null ? null : parsedJson['leaseTerm'],
    uavtCode : parsedJson['uavtCode'] == null ? null : parsedJson['uavtCode'],
    contractPath : parsedJson['contractPath'] == null ? null : parsedJson['contractPath'],
    estimatedRentIncome : parsedJson['estimatedRentIncome'] == null ? null : parsedJson['estimatedRentIncome'],
    propertyTaxYear : parsedJson['propertyTaxYear'] == null ? null : parsedJson['propertyTaxYear'],
    propertyTaxInstallment : parsedJson['propertyTaxInstallment'] == null ? null : parsedJson['propertyTaxInstallment'],
    propertyTaxAmount : parsedJson['propertyTaxAmount'] == null ? null : parsedJson['propertyTaxAmount'],
    propertyTaxTxStatus : parsedJson['propertyTaxTxStatus'] == null ? null : parsedJson['propertyTaxTxStatus'],
    propertyTaxProcessStatus : parsedJson['propertyTaxProcessStatus'] == null ? null : parsedJson['propertyTaxProcessStatus'],
    earthquakeInsurancePath : parsedJson['earthquakeInsurancePath'] == null ? null : parsedJson['earthquakeInsurancePath'],
    earthquakeInsuranceAmount : parsedJson['earthquakeInsuranceAmount'] == null ? null : parsedJson['earthquakeInsuranceAmount'],
    earthquakeProcessStatus : parsedJson['earthquakeProcessStatus'] == null ? null : parsedJson['earthquakeProcessStatus'],
    managementFee : parsedJson['managementFee'] == null ? null : parsedJson['managementFee'],
    propertyRentAmount : parsedJson['propertyRentAmount'] == null ? null : parsedJson['propertyRentAmount'],
    propertyRentFinishDate : parsedJson['propertyRentFinishDate'] == null ? null : parsedJson['propertyRentFinishDate'],
    propertyLinkSahibinden : parsedJson['propertyLinkSahibinden'] == null ? null : parsedJson['propertyLinkSahibinden'],
  );
 }

 static List<PropertiesModel> listFromJson(List<dynamic> list) {
  List<PropertiesModel> rows = list.map((i) => PropertiesModel.fromJson(i)).toList();
  return rows;
 }

 static List<PropertiesModel> listFromString(String responseBody) {
  final parsed = json.decode(responseBody).cast<Map<String, dynamic>>();
  return parsed.map<PropertiesModel>((json) => PropertiesModel.fromJson(json)).toList();
 }

  Map<String,dynamic> toJson()=>{
   'id' : id,
   'userId' : userId,
   'status' : status,
   'isDeleted' : isDeleted,
   'createdDate' : createdDate,
   'updatedDate' : updatedDate,
   'title' : title,
   'imagePath' : imagePath,
   'deedPath' : deedPath,
   'rentContractPath' : rentContractPath,
   'city' : city != null ? city!.toJson() : null,
   'district' : district != null ? district!.toJson() : null,
   'place' : place != null ? place!.toJson() : null,
   'neighborhood' : neighborhood,
   'street' : street,
   'addressNumber' : addressNumber,
   'doorNumber' : doorNumber,
   'propertyType' : propertyType != null ? propertyType!.toJson() : null,
   'propertyRoom' : propertyRoom != null ? propertyRoom!.toJson() : null,
   'propertySize' : propertySize,
   'propertyFurnished' : propertyFurnished != null ? propertyFurnished!.toJson() : null,
   'propertyHirer' : propertyHirer != null ? propertyHirer!.toJson() : null,
   'propertyIsPersonelUse ' : propertyIsPersonelUse != null ? propertyIsPersonelUse!.toJson() : null,
   'leaseStatus' : leaseStatus,
   'leaseProcessStatus' : leaseProcessStatus,
   'leaseDate' : leaseDate,
   'leaseAmount' : leaseAmount,
   'leaseTerm' : leaseTerm,
   'uavtCode' : uavtCode,
   'contractPath' : contractPath,
   'estimatedRentIncome' : estimatedRentIncome,
   'propertyTaxYear' : propertyTaxYear,
   'propertyTaxInstallment' : propertyTaxInstallment,
   'propertyTaxAmount' : propertyTaxAmount,
   'propertyTaxTxStatus' : propertyTaxTxStatus,
   'propertyTaxProcessStatus' : propertyTaxProcessStatus,
   'earthquakeInsurancePath' : earthquakeInsurancePath,
   'earthquakeInsuranceAmount' : earthquakeInsuranceAmount,
   'earthquakeProcessStatus' : earthquakeProcessStatus,
   'managementFee' : managementFee,
   'propertyRentAmount' : propertyRentAmount,
   'propertyRentFinishDate' : propertyRentFinishDate,
   'propertyLinkSahibinden' : propertyLinkSahibinden,
 };

}