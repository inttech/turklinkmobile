import 'dart:convert';
import 'dart:io';

import 'package:turklink/models/key_value_model.dart';
import 'package:turklink/models/properties_model.dart';

class RequestModel{
  String? id;
  String? userId;
  String? propertiesId;
  int? status;
  int? isDeleted;
  DateTime? createdDate;
  DateTime? updatedDate;
  DateTime? estimatedDate;

  PropertiesModel? propertiesModel;

  KeyValueModel? mainRequestType;
  KeyValueModel? subRequestType;
  int? requestAmount;
  String? description;
  String? imagePath1;
  String? imagePath2;
  String? imagePath3;
  String? imagePath4;


  String? processFilePath1;
  String? processFilePath2;
  String? processFilePath3;
  String? processFilePath4;

  File? imageFile1;
  File? imageFile2;
  File? imageFile3;
  File? imageFile4;

  RequestModel({
    this.id,
    this.userId,
    this.propertiesId,
    this.status,
    this.isDeleted,
    this.createdDate,
    this.updatedDate,
    this.estimatedDate,

    this.propertiesModel,
    this.mainRequestType,
    this.subRequestType,
    this.requestAmount,
    this.description,
    this.imagePath1,
    this.imagePath2,
    this.imagePath3,
    this.imagePath4,
    this.processFilePath1,
    this.processFilePath2,
    this.processFilePath3,
    this.processFilePath4,

    this.imageFile1,
    this.imageFile2,
    this.imageFile3,
    this.imageFile4
  });

  factory RequestModel.fromJson(Map<String,dynamic> parsedJson){
  return RequestModel(
    id : parsedJson['id'] == null ? null : parsedJson['id'],
    userId : parsedJson['userId'] == null ? null : parsedJson['userId'],
    propertiesId : parsedJson['propertiesId'] == null ? null : parsedJson['propertiesId'],
    status : parsedJson['status'] == null ? null : parsedJson['status'],
    isDeleted : parsedJson['isDeleted'] == null ? null : parsedJson['isDeleted'],
    createdDate : parsedJson['createdDate'] == null ? null : parsedJson['createdDate'].toDate(),
    updatedDate : parsedJson['updatedDate'] == null ? null : parsedJson['updatedDate'].toDate(),
    estimatedDate : parsedJson['estimatedDate'] == null ? null : parsedJson['estimatedDate'].toDate(),
    propertiesModel : parsedJson['propertiesModel'] == null ? null :  PropertiesModel.fromJson(parsedJson['propertiesModel']),
    mainRequestType : parsedJson['mainRequestType'] == null ? null :  KeyValueModel.fromJson(parsedJson['mainRequestType']),
    subRequestType : parsedJson['subRequestType'] == null ? null :  KeyValueModel.fromJson(parsedJson['subRequestType']),
    requestAmount : parsedJson['requestAmount'] == null ? null : parsedJson['requestAmount'],
    description : parsedJson['description'] == null ? null : parsedJson['description'],
    imagePath1 : parsedJson['imagePath1'] == null ? null : parsedJson['imagePath1'],
    imagePath2 : parsedJson['imagePath2'] == null ? null : parsedJson['imagePath2'],
    imagePath3 : parsedJson['imagePath3'] == null ? null : parsedJson['imagePath3'],
    imagePath4 : parsedJson['imagePath4'] == null ? null : parsedJson['imagePath4'],
    processFilePath1 : parsedJson['processFilePath1'] == null ? null : parsedJson['processFilePath1'],
    processFilePath2 : parsedJson['processFilePath2'] == null ? null : parsedJson['processFilePath2'],
    processFilePath3 : parsedJson['processFilePath3'] == null ? null : parsedJson['processFilePath3'],
    processFilePath4 : parsedJson['processFilePath4'] == null ? null : parsedJson['processFilePath4'],
  );
 }

 static List<RequestModel> listFromJson(List<dynamic> list) {
  List<RequestModel> rows = list.map((i) => RequestModel.fromJson(i)).toList();
  return rows;
 }

 static List<RequestModel> listFromString(String responseBody) {
  final parsed = json.decode(responseBody).cast<Map<String, dynamic>>();
  return parsed.map<RequestModel>((json) => RequestModel.fromJson(json)).toList();
 }

 Map<String,dynamic> toJson()=>{
   'id' : id,
   'userId' : userId,
   'propertiesId' : propertiesId,
   'status' : status,
   'isDeleted' : isDeleted,
   'createdDate' : createdDate,
   'updatedDate' : updatedDate,
   'estimatedDate' : estimatedDate,
   'propertiesModel' : propertiesModel != null ? propertiesModel!.toJson() : null,
   'mainRequestType' : mainRequestType != null ? mainRequestType!.toJson() : null,
   'subRequestType' : subRequestType != null ? subRequestType!.toJson() : null,
   'requestAmount' : requestAmount,
   'description' : description,
   'imagePath1' : imagePath1,
   'imagePath2' : imagePath2,
   'imagePath3' : imagePath3,
   'imagePath4' : imagePath4,
   'processFilePath1' : processFilePath1,
   'processFilePath2' : processFilePath2,
   'processFilePath3' : processFilePath3,
   'processFilePath4' : processFilePath4,
 };

}