import 'dart:convert';

import 'package:turklink/models/key_value_model.dart';
class MessagesModel{
  String? id;
  String? userId;
  String? title;
  KeyValueModel? request;
  int? status;
  int? isDeleted;
  DateTime? createdDate;
  DateTime? updatedDate;
  String? lastMessage;


  MessagesModel({
    this.id,
    this.userId,
    this.title,
    this.request,
    this.status,
    this.isDeleted,
    this.createdDate,
    this.updatedDate,
    this.lastMessage
  });

  factory MessagesModel.fromJson(Map<String,dynamic> parsedJson){
  return MessagesModel(
    id : parsedJson['id'] == null ? null : parsedJson['id'],
    userId : parsedJson['userId'] == null ? null : parsedJson['userId'],
    title : parsedJson['title'] == null ? null : parsedJson['title'],
    request : parsedJson['request'] == null ? null :  KeyValueModel.fromJson(parsedJson['request']),
    status : parsedJson['status'] == null ? null : parsedJson['status'],
    isDeleted : parsedJson['isDeleted'] == null ? null : parsedJson['isDeleted'],
    lastMessage : parsedJson['lastMessage'] == null ? null : parsedJson['lastMessage'],
    createdDate : parsedJson['createdDate'] == null ? null : parsedJson['createdDate'].toDate(),
    updatedDate : parsedJson['updatedDate'] == null ? null : parsedJson['updatedDate'].toDate(),
  );
 }

 static List<MessagesModel> listFromJson(List<dynamic> list) {
  List<MessagesModel> rows = list.map((i) => MessagesModel.fromJson(i)).toList();
  return rows;
 }

 static List<MessagesModel> listFromString(String responseBody) {
  final parsed = json.decode(responseBody).cast<Map<String, dynamic>>();
  return parsed.map<MessagesModel>((json) => MessagesModel.fromJson(json)).toList();
 }

 Map<String,dynamic> toJson()=>{
   'id' : id,
   'userId' : userId,
   'title' : title,
   'request' :  request != null ? request!.toJson() : null,
   'status' : status,
   'isDeleted' : isDeleted,
   'lastMessage' : lastMessage,
   'createdDate' : createdDate,
   'updatedDate' : updatedDate,
 };

}