import 'dart:convert';

class ContractsModel{
 String? id;
 String? contractContent;
 bool? isActive;

 ContractsModel({
  this.id,
  this.contractContent,
  this.isActive
 });


 factory ContractsModel.fromJson(Map<String,dynamic> parsedJson){
  return ContractsModel(
    id : parsedJson['id'] == null ? null : parsedJson['id'],
    contractContent : parsedJson['contractContent'] == null ? null : parsedJson['contractContent'],
    isActive : parsedJson['isActive'] == null ? null : parsedJson['isActive'],
  );
 }

 static List<ContractsModel> listFromJson(List<dynamic> list) {
  List<ContractsModel> rows = list.map((i) => ContractsModel.fromJson(i)).toList();
  return rows;
 }

 static List<ContractsModel> listFromString(String responseBody) {
  final parsed = json.decode(responseBody).cast<Map<String, dynamic>>();
  return parsed.map<ContractsModel>((json) => ContractsModel.fromJson(json)).toList();
 }

  Map<String,dynamic> toJson()=>{
   'id' : id,
   'contractContent' : contractContent,
   'isActive' : isActive,
 };

}