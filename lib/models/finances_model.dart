import 'dart:convert';

class FinancesModel{
 String? id;
 String? userId;
 String? direction;
 int? financeType;
 String? paymentMethod;
 int? amount;
 String? financeDescription;
 String? propertyId;
 String? requestId;
 DateTime? operationDate;
 String? invoicePath;

 FinancesModel({
  this.id,
  this.userId,
  this.direction,
  this.financeType,
  this.paymentMethod,
  this.amount,
  this.financeDescription,
  this.propertyId,
  this.requestId,
  this.operationDate,
  this.invoicePath
 });


 factory FinancesModel.fromJson(Map<String,dynamic> parsedJson){
  return FinancesModel(
    id : parsedJson['id'] == null ? null : parsedJson['id'],
    userId : parsedJson['userId'] == null ? null : parsedJson['userId'],
    direction : parsedJson['direction'] == null ? null : parsedJson['direction'],
    paymentMethod : parsedJson['paymentMethod'] == null ? null : parsedJson['paymentMethod'],
    financeType : parsedJson['financeType'] == null ? null : parsedJson['financeType'],
    amount : parsedJson['amount'] == null ? null : parsedJson['amount'],
    financeDescription : parsedJson['financeDescription'] == null ? null : parsedJson['financeDescription'],
    propertyId : parsedJson['propertyId'] == null ? null : parsedJson['propertyId'],
    requestId : parsedJson['requestId'] == null ? null : parsedJson['requestId'],
    invoicePath : parsedJson['invoicePath'] == null ? null : parsedJson['invoicePath'],
    operationDate : parsedJson['operationDate'] == null ? null : parsedJson['operationDate'].toDate(),
  );
 }

 static List<FinancesModel> listFromJson(List<dynamic> list) {
  List<FinancesModel> rows = list.map((i) => FinancesModel.fromJson(i)).toList();
  return rows;
 }

 static List<FinancesModel> listFromString(String responseBody) {
  final parsed = json.decode(responseBody).cast<Map<String, dynamic>>();
  return parsed.map<FinancesModel>((json) => FinancesModel.fromJson(json)).toList();
 }

  Map<String,dynamic> toJson()=>{
   'id' : id,
   'userId' : userId,
   'direction' : direction,
   'paymentMethod' : paymentMethod,
   'financeType' : financeType,
   'amount' : amount,
   'financeDescription' : financeDescription,
   'propertyId' : propertyId,
   'requestId' : requestId,
   'invoicePath' : invoicePath,
   'operationDate' : operationDate,
 };

}