import 'dart:convert';
import 'dart:io';

class UserModel{
 String? id;
 String? name;
 String? email;
 String? password;
 String? imagePath;
 DateTime? createdDate;
 DateTime? lastLoginDate;
 String? phone;
 String? address;
 String? cardNumber;
 String? iban;
 int? tryBalance;
 bool? isShowNotification;
 String? langCode;
 bool? isAdmin;
 String? citizenImagePath;
 bool? isVerificationUser;
 String? initialCountryCode;
 String? fcmToken;
 int? incomeTax;
 String? approveKvkkContractId;
 String? approveConfContractId;
 int? verificationStatus;

 File? imageFile;
 File? passportFile;

 UserModel({
  this.id,
  this.name,
  this.email,
  this.password,
  this.imagePath,
  this.createdDate,
  this.phone,
  this.address,
  this.lastLoginDate,
  this.cardNumber,
  this.iban,
  this.tryBalance,
  this.isShowNotification,
  this.langCode,
  this.imageFile,
  this.isAdmin,
  this.citizenImagePath,
  this.isVerificationUser,
  this.passportFile,
  this.initialCountryCode,
  this.fcmToken,
  this.incomeTax,
  this.approveConfContractId,
  this.approveKvkkContractId,
  this.verificationStatus
 });


 factory UserModel.fromJson(Map<String,dynamic> parsedJson){
  return UserModel(
   id : parsedJson['id'],
   name :  parsedJson['name'] == null ? null : parsedJson['name'],
   email :  parsedJson['email'] == null ? null : parsedJson['email'],
   password :  parsedJson['password'] == null ? null : parsedJson['password'],
   imagePath :  parsedJson['imagePath'] == null ? null : parsedJson['imagePath'],
   createdDate : parsedJson['createdDate'] == null ? null : parsedJson['createdDate'].toDate(),
   lastLoginDate : parsedJson['lastLoginDate'] == null ? null : parsedJson['lastLoginDate'].toDate(),
   phone:  parsedJson['phone'] == null ? null : parsedJson['phone'],
   address:  parsedJson['address'] == null ? null : parsedJson['address'],
   cardNumber:  parsedJson['cardNumber'] == null ? null : parsedJson['cardNumber'],
   initialCountryCode:  parsedJson['initialCountryCode'] == null ? null : parsedJson['initialCountryCode'],
   iban:  parsedJson['iban'] == null ? null : parsedJson['iban'],
   isShowNotification:  parsedJson['isShowNotification'] == null ? null : parsedJson['isShowNotification'],
   langCode:  parsedJson['langCode'] == null ? null : parsedJson['langCode'],
   fcmToken:  parsedJson['fcmToken'] == null ? null : parsedJson['fcmToken'],
   tryBalance:  parsedJson['tryBalance'] == null ? 0 : parsedJson['tryBalance'],
   isAdmin:  parsedJson['isAdmin'] == null ? 0 : parsedJson['isAdmin'],
   citizenImagePath:  parsedJson['citizenImagePath'] == null ? 0 : parsedJson['citizenImagePath'],
   isVerificationUser:  parsedJson['isVerificationUser'] == null ? 0 : parsedJson['isVerificationUser'],
   incomeTax:  parsedJson['incomeTax'] == null ? 0 : parsedJson['incomeTax'],
   approveConfContractId:  parsedJson['approveConfContractId'] == null ? null : parsedJson['approveConfContractId'],
   approveKvkkContractId:  parsedJson['approveKvkkContractId'] == null ? null : parsedJson['approveKvkkContractId'],
   verificationStatus:  parsedJson['verificationStatus'] == null ? 0 : parsedJson['verificationStatus'],
  );
 }

 static List<UserModel> listFromJson(List<dynamic> list) {
  List<UserModel> rows = list.map((i) => UserModel.fromJson(i)).toList();
  return rows;
 }

 static List<UserModel> listFromString(String responseBody) {
  final parsed = json.decode(responseBody).cast<Map<String, dynamic>>();
  return parsed.map<UserModel>((json) => UserModel.fromJson(json)).toList();
 }

  Map<String,dynamic> toJson()=>{
   'id' : id,
   'name' : name,
   'email' : email,
   'password' : password,
   'imagePath' : imagePath,
   'createdDate' : createdDate,
   'lastLoginDate' : lastLoginDate,
   'phone' : phone,
   'initialCountryCode' : initialCountryCode,
   'address' : address,
   'cardNumber' : cardNumber,
   'iban' : iban,
   'isShowNotification' : isShowNotification,
   'langCode' : langCode,
   'fcmToken' : fcmToken,
   'tryBalance' : tryBalance,
   'isAdmin' : isAdmin,
   'citizenImagePath' : citizenImagePath,
   'isVerificationUser' : isVerificationUser,
   'incomeTax' : incomeTax,
   'approveConfContractId' : approveConfContractId,
   'approveKvkkContractId' : approveKvkkContractId,
   'verificationStatus' : verificationStatus,
 };

}