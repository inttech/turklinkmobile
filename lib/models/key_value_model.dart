class KeyValueModel {
  String? key;
  String? value;

  KeyValueModel({this.key,this.value});

  Map<String,dynamic> toJson()=>{
   'key' : key,
   'value' : value,
 };

  factory KeyValueModel.fromJson(Map<String,dynamic> parsedJson){
    return KeyValueModel(
      key : parsedJson['key'] == null ? null : parsedJson['key'],
      value : parsedJson['value'] == null ? null : parsedJson['value'],
    );
  }

  factory KeyValueModel.fromJsonCity(Map<String,dynamic> parsedJson){
    return KeyValueModel(
      key : parsedJson['id'] == null ? null : parsedJson['id'],
      value : parsedJson['il'] == null ? null : parsedJson['il'],
    );
  }

  static List<KeyValueModel> listFromJsonCity(List<dynamic> list) {
    List<KeyValueModel> rows = list.map((i) => KeyValueModel.fromJsonCity(i)).toList();
    return rows;
  }

  factory KeyValueModel.fromJsonDistrict(Map<String,dynamic> parsedJson){
    return KeyValueModel(
      key : parsedJson['id'] == null ? null : parsedJson['id'],
      value : parsedJson['ilce'] == null ? null : parsedJson['ilce'],
    );
  }

  static List<KeyValueModel> listFromJsonDistrict(List<dynamic> list) {
    List<KeyValueModel> rows = list.map((i) => KeyValueModel.fromJsonDistrict(i)).toList();
    return rows;
  }

  factory KeyValueModel.fromJsonPlace(Map<String,dynamic> parsedJson){
    return KeyValueModel(
      key : parsedJson['id'] == null ? null : parsedJson['id'],
      value : parsedJson['semtBucakBelde'] == null ? null : parsedJson['semtBucakBelde'],
    );
  }

  static List<KeyValueModel> listFromJsonPlace(List<dynamic> list) {
    List<KeyValueModel> rows = list.map((i) => KeyValueModel.fromJsonPlace(i)).toList();
    return rows;
  }

  factory KeyValueModel.fromJsonNeighborhood(Map<String,dynamic> parsedJson){
    return KeyValueModel(
      key : parsedJson['mahalle'] == null ? null : parsedJson['mahalle'],
      value : parsedJson['mahalle'] == null ? null : parsedJson['mahalle'],
    );
  }

  static List<KeyValueModel> listFromJsonNeighborhood(List<dynamic> list) {
    List<KeyValueModel> rows = list.map((i) => KeyValueModel.fromJsonNeighborhood(i)).toList();
    return rows;
  }


}