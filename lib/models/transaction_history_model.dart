import 'dart:convert';

class TransactionHistoryModel{
 String? id;
 String? userId;
 String? financeId;
 int? financeType;
 String? paymentMethod;
 int? amount;
 int? txStatus;
 String? propertyId;
 String? requestId;
 DateTime? operationDate;

 TransactionHistoryModel({
  this.id,
  this.userId,
  this.financeId,
  this.financeType,
  this.paymentMethod,
  this.amount,
  this.txStatus,
  this.propertyId,
  this.requestId,
  this.operationDate
 });


 factory TransactionHistoryModel.fromJson(Map<String,dynamic> parsedJson){
  return TransactionHistoryModel(
    id : parsedJson['id'] == null ? null : parsedJson['id'],
    userId : parsedJson['userId'] == null ? null : parsedJson['userId'],
    financeId : parsedJson['financeId'] == null ? null : parsedJson['financeId'],
    paymentMethod : parsedJson['paymentMethod'] == null ? null : parsedJson['paymentMethod'],
    financeType : parsedJson['financeType'] == null ? null : parsedJson['financeType'],
    txStatus : parsedJson['txStatus'] == null ? null : parsedJson['txStatus'],
    amount : parsedJson['amount'] == null ? null : parsedJson['amount'],
    propertyId : parsedJson['propertyId'] == null ? null : parsedJson['propertyId'],
    requestId : parsedJson['requestId'] == null ? null : parsedJson['requestId'],
    operationDate : parsedJson['operationDate'] == null ? null : parsedJson['operationDate'].toDate(),
  );
 }

 static List<TransactionHistoryModel> listFromJson(List<dynamic> list) {
  List<TransactionHistoryModel> rows = list.map((i) => TransactionHistoryModel.fromJson(i)).toList();
  return rows;
 }

 static List<TransactionHistoryModel> listFromString(String responseBody) {
  final parsed = json.decode(responseBody).cast<Map<String, dynamic>>();
  return parsed.map<TransactionHistoryModel>((json) => TransactionHistoryModel.fromJson(json)).toList();
 }

  Map<String,dynamic> toJson()=>{
   'id' : id,
   'userId' : userId,
   'financeId' : financeId,
   'paymentMethod' : paymentMethod,
   'financeType' : financeType,
   'txStatus' : txStatus,
   'amount' : amount,
   'propertyId' : propertyId,
   'requestId' : requestId,
   'operationDate' : operationDate,
 };

}