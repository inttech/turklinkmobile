import 'dart:convert';

class DollarValueModel{
 double? usdTry;

 DollarValueModel({
  this.usdTry,
 });


 factory DollarValueModel.fromJson(Map<String,dynamic> parsedJson){
  return DollarValueModel(
    usdTry : parsedJson['USD_TRY'] == null ? null : double.parse(parsedJson['USD_TRY']),
  );
 }

 static List<DollarValueModel> listFromJson(List<dynamic> list) {
  List<DollarValueModel> rows = list.map((i) => DollarValueModel.fromJson(i)).toList();
  return rows;
 }

 static List<DollarValueModel> listFromString(String responseBody) {
  final parsed = json.decode(responseBody).cast<Map<String, dynamic>>();
  return parsed.map<DollarValueModel>((json) => DollarValueModel.fromJson(json)).toList();
 }

  Map<String,dynamic> toJson()=>{
   'USD_TRY' : usdTry,
 };

}