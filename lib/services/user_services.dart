import 'dart:math';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:mailer/mailer.dart';
import 'package:mailer/smtp_server.dart';
import 'package:string_extensions/string_extensions.dart';
import 'package:turklink/models/chats_model.dart';
import 'package:turklink/models/key_value_model.dart';
import 'package:turklink/models/messages_model.dart';
import 'package:turklink/models/user_model.dart';
import 'package:http/http.dart' as http;
import 'package:turklink/services/storage_services.dart';
import 'package:turklink/services/utility_services.dart';
import 'dart:convert';

import 'package:turklink/util/utility.dart';

class UserServices {
  String errorMessage = "";
  List<String> errorMessageList = [];

  Future<User?> login(String email, String password) async {
    if (email.isEmpty) {
      errorMessage = "error_messages.login_email_empty";
      return null;
    }
    if (password.isEmpty) {
      errorMessage = "error_messages.login_password_empty";
      return null;
    }

    if (!Utility.isValidEmail(email)) {
      errorMessage = "error_messages.login_email_not_valid";
      return null;
    }

    try {
      var loginResult = await FirebaseAuth.instance
          .signInWithEmailAndPassword(email: email, password: password);

      if(loginResult.user != null)
      {
        var userModel = await this.getUser(loginResult.user!.uid);
        if(userModel != null)
        {
           userModel.lastLoginDate = DateTime.now();
           String? token = await FirebaseMessaging.instance.getToken();
           userModel.fcmToken = token;
           await this.updateUser(userModel);
        }
      }

      return loginResult.user;
    } on FirebaseAuthException catch (error) {
      errorMessage = "error_messages.login_error_messages";

      if (error.code == "user-not-found") {
        errorMessage = "error_messages.login_user_not_found";
      }
      if (error.code == "wrong-password") {
        errorMessage = "error_messages.login_user_wrong_password";
      }
      if (error.code == "user-disabled") {
        errorMessage = "error_messages.login_user_disabled";
      }
      if (error.code == "invalid-email") {
        errorMessage = "error_messages.login_email_not_valid";
      }
      return null;
    }
  }

  Future<UserModel?> signup(
      String name,
      String email,
      String password,
      String rePassword,
      String phoneNumber,
      String emailVerificationCode,
      String emailCodeRequestId,
      String phoneVerificationCode,
      String phoneCodeRequestId,
      String initialCountryCode,
      bool kvkkText,
      bool privacyText
      ) async {

    if(!kvkkText || !privacyText)
    {
      errorMessage = "error_messages.kvkk_or_privacy_not_checked";
      errorMessageList.add(errorMessage);
    }

    if (name.isEmpty ) {
      errorMessage = "error_messages.register_name_empty";
      errorMessageList.add(errorMessage);
    }

    if (email.isEmpty) {
      errorMessage = "error_messages.register_email_empty";
      errorMessageList.add(errorMessage);
    }

    if (!Utility.isValidEmail(email)) {
      errorMessage = "error_messages.register_email_not_valid";
      errorMessageList.add(errorMessage);
    }

    if (email.isNotEmpty && (emailVerificationCode.isEmpty || emailCodeRequestId.isEmpty)) {
      errorMessage = "error_messages.register_emailVerification_empty";
      errorMessageList.add(errorMessage);
    }

    if (emailVerificationCode != emailCodeRequestId) {
      errorMessage = "error_messages.register_emailVerification_not_valid";
      errorMessageList.add(errorMessage);
    }

    if (phoneNumber.isEmpty) {
      errorMessage = "error_messages.register_phoneNumber_empty";
      errorMessageList.add(errorMessage);
    }

    if (phoneNumber.isNotEmpty && (phoneVerificationCode.isEmpty || phoneCodeRequestId.isEmpty)) {
      errorMessage = "error_messages.register_phoneVerification_empty";
      errorMessageList.add(errorMessage);
    }

    if (phoneVerificationCode != phoneCodeRequestId)  {
      errorMessage = "error_messages.register_phoneVerification_not_valid";
      errorMessageList.add(errorMessage);
    }

    if (password.isEmpty) {
      errorMessage = "error_messages.register_password_empty";
      errorMessageList.add(errorMessage);
    }

    if (rePassword.isEmpty) {
      errorMessage = "error_messages.register_repassword_empty";
      errorMessageList.add(errorMessage);
    }

    if (password != rePassword) {
      errorMessage = "error_messages.register_password_repassword_not_valid";
      errorMessageList.add(errorMessage);
    }

    if(errorMessageList.length > 0)
      return null;

    try {

      String? token = await FirebaseMessaging.instance.getToken();

      UtilityServices utilityServices = new UtilityServices();
      var confContract = await utilityServices.getConfContract();
      var kvkkContract = await utilityServices.getKvkkContract();

      var signupResult = await FirebaseAuth.instance
          .createUserWithEmailAndPassword(
              email: email.trim(), password: password);
      DateTime now = DateTime.now();

      var namesUpper = name.toTitleCase;


      var userModel = new UserModel(
          id: signupResult.user!.uid,
          name: namesUpper,
          address: "",
          email: email,
          password: "",
          phone: phoneNumber,
          createdDate: now,
          lastLoginDate: now,
          cardNumber: "",
          imagePath: null,
          isShowNotification: true,
          isAdmin:false,
          tryBalance: 0,
          citizenImagePath: "",
          isVerificationUser: false,
          initialCountryCode: initialCountryCode,
          fcmToken: token,
          approveConfContractId:confContract.id,
          approveKvkkContractId:kvkkContract.id,
          );

      await FirebaseFirestore.instance
          .collection("users")
          .doc(userModel.id)
          .set(userModel.toJson());
      return userModel;
    } on FirebaseAuthException catch (error) {
      errorMessage = "TECHNICAL_ERROR";
      if (error.code == "email-already-in-use") {
        errorMessage = "error_messages.register_email_already_use";
      }
      if (error.code == "weak-password") {
        errorMessage = "error_messages.register_weak_password";
      }
    }

    return null;
  }

  Future<bool> resetPassword(String email) async {
    if (email.isEmpty) {
      errorMessage = "error_messages.forget_password_email_empty";
      return false;
    }
    if (!Utility.isValidEmail(email)) {
      errorMessage = "error_messages.forget_password_email_not_valid";
      return false;
    }
    try {
      await FirebaseAuth.instance.sendPasswordResetEmail(email: email.trim());
      return true;
    } on FirebaseAuthException catch (error) {
      errorMessage = "TECHNICAL_ERROR";
      if (error.code == "user-not-found") {
        errorMessage = "error_messages.forget_password_user_not_found";
      }
    }
    return false;
  }

  Future<UserModel?> getUser(String userId) async {
    try {
      var userResult = await FirebaseFirestore.instance
          .collection("users")
          .doc(userId)
          .get();
      if (userResult.exists) {
        var userData = UserModel.fromJson(userResult.data()!);
        return userData;
      } else {
        errorMessage = "TECHNICAL_ERROR";
        return null;
      }
    } catch (error) {
      errorMessage = "TECHNICAL_ERROR";
    }
    return null;
  }

  Future<bool> updateUser(UserModel userModel) async {

     StorageServices storageServices = new StorageServices();
    try {
      if(userModel.imageFile != null)
      {
        var uploadResult = await storageServices.uploadFile("users", userModel.id!, userModel.imageFile!);
        if(uploadResult.isEmpty)
        {
          errorMessage = storageServices.errorMessage;
          return false;
        }
        userModel.imagePath = uploadResult;
      }

      if(userModel.passportFile != null)
      {
        var uploadResult = await storageServices.uploadFile("users", userModel.id! + "_passport", userModel.passportFile!);
        if(uploadResult.isEmpty)
        {
          errorMessage = storageServices.errorMessage;
          return false;
        }
        userModel.citizenImagePath = uploadResult;
        userModel.isVerificationUser = false;
        userModel.verificationStatus = 0;
      }

      await FirebaseFirestore.instance
          .collection("users")
          .doc(userModel.id)
          .update(userModel.toJson());
      return true;
    } catch (error) {
      errorMessage = "TECHNICAL_ERROR";
    }

    return false;
  }

  Future<User> getCurrentUser() async {
    return FirebaseAuth.instance.currentUser!;
  }

  Future<bool> logout() async {
    try {
      await FirebaseAuth.instance.signOut();
      return true;
    } catch (e) {
      return false;
    }
  }

  Future<bool> delete() async {
    try {
      await FirebaseAuth.instance.currentUser!.delete();
      return true;
    } catch (e) {
      return false;
    }
  }

  Future<String?> sendOtpSms(String phoneNumber) async {
    try {
      phoneNumber = phoneNumber.replaceAll("+", "");

      if (phoneNumber.isEmpty) {
        errorMessage = "error_messages.register_phoneNumber_empty";
        return null;
      }

      var rng = new Random();
      var code = rng.nextInt(9000) + 1000;

       var smsOtpContext = tr('register_screen.register_sms_subject',args: [code.toString()]); 

      await FirebaseFirestore.instance
      .collection("sms")
      .add({
        "mobilePhoneNumber": phoneNumber,
        "smsContent": smsOtpContext,
      });

      return code.toString();

    } catch (e) {
      errorMessage = "error_messages.send_otp_error";
      return null;
    }
  }

  Future<String?> otpControl(String requestId, String code) async {
    try {
      var url = Uri.parse(
          'https://api.nexmo.com/verify/check/json?&api_key=80eb0634&api_secret=4T1f037Mm93eoOYH&request_id=' +
              requestId +
              '&code=' +
              code);
      var response = await http.get(url);
      var body = jsonDecode(response.body);
      return body["status"].toString();
    } catch (e) {
      errorMessage = "error_messages.send_otp_error";
      return null;
    }
  }

  Future<String?> sendOtpEmail(String emailAddress) async {

    if (emailAddress.isEmpty) {
      errorMessage = "error_messages.register_email_empty";
      return null;
    }

    if (!Utility.isValidEmail(emailAddress)) {
      errorMessage = "error_messages.register_email_not_valid";
      return null;
    }

    try {
      var rng = new Random();
      var code = rng.nextInt(9000) + 1000;
      var subject = tr('register_screen.register_email_subject'); 
      var text = tr('register_screen.register_email_otp_message',args: [code.toString()]); 

      await FirebaseFirestore.instance
      .collection("mail")
      .add({
        "to": emailAddress,
        "message": {
         "subject": subject,
          "text": text,
          "html": text,
        },
      });

      return code.toString();
    } catch (e) {
      errorMessage = "error_messages.send_otp_error";
      return null;
    }
  }

  Future sendMessage(String messageTitle,String message, String messageId, bool isFirstMessage,KeyValueModel? request) async
  {
    DateTime now = DateTime.now();
    DateFormat formatter = DateFormat('yyyyMMddHms');

    try {

      MessagesModel messagesModel = new MessagesModel(
        isDeleted: 0,
        status: 0,
        title: messageTitle,
        request: request,
        lastMessage: message,
        userId: FirebaseAuth.instance.currentUser!.uid,
        updatedDate: now,
        id: messageId,
      );

      if(isFirstMessage)
      {
        messagesModel.createdDate = now;
      }

      if(isFirstMessage)
        await FirebaseFirestore.instance.collection("messages").doc(messagesModel.id).set(messagesModel.toJson());
      else
        await FirebaseFirestore.instance.collection("messages").doc(messagesModel.id).update(messagesModel.toJson());

      ChatsModel chatsModel = new ChatsModel(
        date: DateTime.now().millisecondsSinceEpoch,
        userId: FirebaseAuth.instance.currentUser!.uid,
        isDeleted: 0,
        status: 0,
        message: message,
        id: FirebaseAuth.instance.currentUser!.uid + "_" + DateTime.now().millisecondsSinceEpoch.toString()
      );

      await FirebaseFirestore.instance.collection("messages").doc(messagesModel.id).collection("chats").doc(chatsModel.id).set(chatsModel.toJson());

    } on FirebaseAuthException catch (error) {
      errorMessage = "TECHNICAL_ERROR";

    }

  }

}
