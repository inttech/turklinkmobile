import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:turklink/models/properties_model.dart';
import 'package:turklink/services/storage_services.dart';
import 'package:turklink/util/property_process_status.dart';
import 'package:turklink/util/property_status.dart';

class PropertiesServices{

    String errorMessage = "";

    Future<bool> addProperties(PropertiesModel propertiesModel) async
    {
      StorageServices storageServices = new StorageServices();
      try
      {
        DateTime now = DateTime.now();
        DateFormat formatter = DateFormat('yyyyMMddHms');

        propertiesModel.userId = FirebaseAuth.instance.currentUser!.uid;
        propertiesModel.status = PropertyStatus.pendingApproval;
        propertiesModel.isDeleted = 0;
        propertiesModel.createdDate = now;
        propertiesModel.updatedDate = now;
        propertiesModel.id =  propertiesModel.userId! + "_" + formatter.format(now);

        propertiesModel.earthquakeProcessStatus = PropertyProcessStatus.pendingInfoFromAdmin;
        propertiesModel.propertyTaxProcessStatus = PropertyProcessStatus.pendingInfoFromAdmin;
        propertiesModel.leaseProcessStatus = PropertyProcessStatus.pendingInfoFromAdmin;

        if( propertiesModel.imageFile != null)
        {
          var uploadResult = await storageServices.uploadFile("properties", propertiesModel.id!, propertiesModel.imageFile!);
          if(uploadResult.isEmpty)
          {
            errorMessage = storageServices.errorMessage;
            return false;
          }
          propertiesModel.imagePath = uploadResult;
        }
         if( propertiesModel.deedFile != null)
        {
          var uploadResult = await storageServices.uploadFile("deeds", propertiesModel.id!, propertiesModel.deedFile!);
          if(uploadResult.isEmpty)
          {
            errorMessage = storageServices.errorMessage;
            return false;
          }
          propertiesModel.deedPath = uploadResult;
        }
        if( propertiesModel.rentContractFile != null)
        {
          var uploadResult = await storageServices.uploadFile("rentcontracts", propertiesModel.id!, propertiesModel.rentContractFile!);
          if(uploadResult.isEmpty)
          {
            errorMessage = storageServices.errorMessage;
            return false;
          }
          propertiesModel.rentContractPath = uploadResult;
        }

        await FirebaseFirestore.instance.collection("properties").doc(propertiesModel.id).set(propertiesModel.toJson());
        await FirebaseFirestore.instance.collection("properties_first_v").doc(propertiesModel.id).set(propertiesModel.toJson());
        return true;
      }
      catch (error)
      {
        errorMessage = "TECHNICAL_ERROR";
      }

      return false;
    }

    Future<bool> updateProperties(PropertiesModel propertiesModel) async
    {
      StorageServices storageServices = new StorageServices();
      try
      {
        DateTime now = DateTime.now();
        DateFormat formatter = DateFormat('yyyyMMddHms');
        propertiesModel.updatedDate = now;

        if( propertiesModel.imageFile != null)
        {
          var uploadResult = await storageServices.uploadFile("properties", propertiesModel.userId! + "_" + formatter.format(now), propertiesModel.imageFile!);
          if(uploadResult.isEmpty)
          {
            errorMessage = storageServices.errorMessage;
            return false;
          }
          propertiesModel.imagePath = uploadResult;
        }
         if( propertiesModel.deedFile != null)
        {
          var uploadResult = await storageServices.uploadFile("deeds", propertiesModel.userId! + "_" + formatter.format(now), propertiesModel.deedFile!);
          if(uploadResult.isEmpty)
          {
            errorMessage = storageServices.errorMessage;
            return false;
          }
          propertiesModel.deedPath = uploadResult;
        }

        if( propertiesModel.rentContractFile != null)
        {
          var uploadResult = await storageServices.uploadFile("rentcontracts", propertiesModel.id!, propertiesModel.rentContractFile!);
          if(uploadResult.isEmpty)
          {
            errorMessage = storageServices.errorMessage;
            return false;
          }
          propertiesModel.rentContractPath = uploadResult;
        }

        await FirebaseFirestore.instance.collection("properties").doc(propertiesModel.id).update(propertiesModel.toJson());
        return true;
      }
      catch (error)
      {
        errorMessage = "TECHNICAL_ERROR";
      }

      return false;
    }

    Future<PropertiesModel?> getProperty(String propertyId) async
    {
      try
      {
          var propertyResult = await FirebaseFirestore.instance.collection("properties").doc(propertyId).get();
          if(propertyResult.exists)
          {
              var propertyData = PropertiesModel.fromJson(propertyResult.data()!);
              return propertyData;
          }
          else
          {
            errorMessage = "TECHNICAL_ERROR";
            return null;
          }
      }
      catch (error)
      {
          errorMessage = "TECHNICAL_ERROR";
      }
      return null;
    }

    Future<bool> deleteProperties(PropertiesModel propertiesModel) async
    {
      try
      {
        propertiesModel.isDeleted = 1;
        await FirebaseFirestore.instance.collection("properties").doc(propertiesModel.id!).update(propertiesModel.toJson());
        return true;
      }
      catch (error)
      {
        errorMessage = "TECHNICAL_ERROR";
      }

      return false;
    }


}