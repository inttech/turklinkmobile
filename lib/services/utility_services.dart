import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:cool_alert/cool_alert.dart';
import 'package:flutter/material.dart';
import 'package:turklink/models/contracts_model.dart';
import 'package:turklink/models/dollar_value_model.dart';
import 'package:turklink/models/key_value_model.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:turklink/models/notification_model.dart';
import 'package:turklink/models/request_model.dart';

class UtilityServices {
  String errorMessage = "";

  List<KeyValueModel> getPropertyType() {
    var result = [
      KeyValueModel(key: "FLAT", value: "property_type.FLAT".tr()),
      KeyValueModel(key: "VILLA", value: "property_type.VILLA".tr()),
      KeyValueModel(key: "OFFICE", value: "property_type.OFFICE".tr()),
      KeyValueModel(key: "SHOP", value: "property_type.SHOP".tr()),
      KeyValueModel(key: "LAND", value: "property_type.LAND".tr()),
    ];
    return result;
  }

  List<KeyValueModel> getPropertyRoom() {
    var result = [
      KeyValueModel(key: "1+0", value: "1+0"),
      KeyValueModel(key: "1+1", value: "1+1"),
      KeyValueModel(key: "1.5+1", value: "1.5+1"),
      KeyValueModel(key: "2+0", value: "2+0"),
      KeyValueModel(key: "2+1", value: "2+1"),
      KeyValueModel(key: "2.5+1", value: "2.5+1"),
      KeyValueModel(key: "2+2", value: "2+2"),
      KeyValueModel(key: "3+1", value: "3+1"),
      KeyValueModel(key: "3.5+1", value: "3.5+1"),
      KeyValueModel(key: "4+1", value: "4+1"),
      KeyValueModel(key: "5+1", value: "5+1"),
      KeyValueModel(key: "5+2", value: "5+2"),
    ];
    return result;
  }

  List<KeyValueModel> getYesNo() {
    var result = [
      KeyValueModel(key: "Y", value: "utility.YES".tr()),
      KeyValueModel(key: "N", value: "utility.NO".tr()),
    ];
    return result;
  }

  List<KeyValueModel> getLeaseTerm() {
    List<KeyValueModel> result = [];

    for (var i = 12; i < 120; i++) {
      result.add(KeyValueModel(key: i.toString(), value: i.toString() + " " + "utility.MONTH".tr()));
    }
    return result;
  }

  List<KeyValueModel> getLeaseAmount(int estimatedAmount) {
    double reelEstimated = estimatedAmount/100;
    double minEstimated = reelEstimated - (reelEstimated/10);
    double maxEstimated = reelEstimated + (reelEstimated/10);
    List<KeyValueModel> result = [];

    List<int> minEstimatedInt = [];

    for(var i= reelEstimated.toInt()-100; i > minEstimated.toInt(); i=i-100)
    {
      minEstimatedInt.add(i);
    }

    var sort = minEstimatedInt..sort();

    for (var i in sort) {
      result.add(KeyValueModel(key: i.toString(), value: i.toString() + " TL / " + "utility.MONTH".tr()));
    }

    for (var i = reelEstimated.toInt(); i <= maxEstimated.toInt(); i=i+100) {
      result.add(KeyValueModel(key: i.toString(), value: i.toString() + " TL / " + "utility.MONTH".tr()));
    }
    return result;
  }


  List<KeyValueModel> getRequestMainType() {
    var result = [
      KeyValueModel(key: "VALUATION_REPORT", value: "request_type.VALUATION_REPORT".tr()),
      KeyValueModel(key: "SALES_REQUEST", value: "request_type.SALES_REQUEST".tr()),
      KeyValueModel(key: "SERVICE_REQUEST", value: "request_type.SERVICE_REQUEST".tr()),
      KeyValueModel(key: "CLEANING", value: "request_type.CLEANING".tr()),
      KeyValueModel(key: "EVICTION_REQUEST", value: "request_type.EVICTION_REQUEST".tr()),
    ];
    return result;
  }

  List<KeyValueModel> getRequestSubype(String mainType) {
    List<KeyValueModel> result = [];
    if(mainType == "VALUATION_REPORT")
    {
      result.add(new KeyValueModel(key: "VALUATION_REPORT_*_VALUATION_REPORT_WITH_SPK", value: "request_type.VALUATION_REPORT_WITH_SPK".tr()));
      result.add(new KeyValueModel(key: "VALUATION_REPORT_*_VALUATION_REPORT_WITHOUT_SPK", value: "request_type.VALUATION_REPORT_WITHOUT_SPK".tr()));
    }
    if(mainType == "SERVICE_REQUEST")
    {
      result.add(new KeyValueModel(key: "SERVICE_REQUEST_*_SERVICE_REQUEST_PAINT", value: "request_type.SERVICE_REQUEST_PAINT".tr()));
      result.add(new KeyValueModel(key: "SERVICE_REQUEST_*_SERVICE_REQUEST_PLUMBING", value: "request_type.SERVICE_REQUEST_PLUMBING".tr()));
      result.add(new KeyValueModel(key: "SERVICE_REQUEST_*_SERVICE_REQUEST_ELECTRIC", value: "request_type.SERVICE_REQUEST_ELECTRIC".tr()));
      result.add(new KeyValueModel(key: "SERVICE_REQUEST_*_SERVICE_REQUEST_FURNISH", value: "request_type.SERVICE_REQUEST_FURNISH".tr()));
    }
    if(mainType == "EVICTION_REQUEST")
    {
      result.add(new KeyValueModel(key: "EVICTION_REQUEST_*_EVICTION_REQUEST_MODIFICATIONS", value: "request_type.EVICTION_REQUEST_MODIFICATIONS".tr()));
      result.add(new KeyValueModel(key: "EVICTION_REQUEST_*_EVICTION_REQUEST_POSSESSOR", value: "request_type.EVICTION_REQUEST_POSSESSOR".tr()));
      result.add(new KeyValueModel(key: "EVICTION_REQUEST_*_EVICTION_REQUEST_TENANT", value: "request_type.EVICTION_REQUEST_TENANT".tr()));

    }
    return result;
  }

  int getRequestAmount(String amountType,int? propertySize)
  {
    int amount = 0;
    if(amountType == "VALUATION_REPORT_*_VALUATION_REPORT_WITH_SPK")
      return 200000;
    if(amountType == "VALUATION_REPORT_*_VALUATION_REPORT_WITHOUT_SPK")
      return 20000;

    if(amountType == "CLEANING")
      return propertySize!*2000;

    return amount;
  }

  Future<double> getUsdValue() async
  {
    var dollarValue= await FirebaseFirestore.instance.collection("dollar_value").get();
    var dollarData = DollarValueModel.fromJson(dollarValue.docs.first.data());


    return dollarData.usdTry!;
  }

  Future<ContractsModel> getKvkkContract() async
  {
    var contracts= await FirebaseFirestore.instance.collection("kvkkContracts").where('isActive', isEqualTo: true).get();
    var contractData = ContractsModel.fromJson(contracts.docs.first.data());

    return contractData;
  }

  Future<ContractsModel> getConfContract() async
  {
    var contracts= await FirebaseFirestore.instance.collection("confContracts").where('isActive', isEqualTo: true).get();
    var contractData = ContractsModel.fromJson(contracts.docs.first.data());

    return contractData;
  }



  List<KeyValueModel> getMessageReason(bool isShowCurrent) {
    var result = [
      KeyValueModel(key: "TECH_PROBLEM", value: "utility.TECH_PROBLEM".tr()),
      KeyValueModel(key: "PROPERTY_ISSUE", value: "utility.PROPERTY_ISSUE".tr()),
      KeyValueModel(key: "ACCOUNTING", value: "utility.ACCOUNTING".tr()),
      KeyValueModel(key: "OTHER", value: "utility.OTHER".tr()),
    ];
    if(isShowCurrent)
      result.add(KeyValueModel(key: "CURRENT_PROBLEM", value: "utility.CURRENT_PROBLEM".tr()));

    return result;
  }

  List<KeyValueModel> convertRequestKeyValue(List<RequestModel>? requestss) {
    List<KeyValueModel> result = [];

    for (var item in requestss!) {
      result.add(KeyValueModel(key: item.id, value: ("request_type." + item.mainRequestType!.key!).tr()));
    }

    return result;
  }

  String localNotificationTitle(NotificaitonModel notificaitonModel,String locale)
  {
     if(locale == "ar")
      return notificaitonModel.title_ar!;
    if(locale == "tr")
      return notificaitonModel.title_tr!;
    if(locale == "en")
      return notificaitonModel.title_en!;

      return "-";
  }

  String localNotificationMessages(NotificaitonModel notificaitonModel,String locale)
  {
     if(locale == "ar")
      return notificaitonModel.message_ar!;
    if(locale == "tr")
      return notificaitonModel.message_tr!;
    if(locale == "en")
      return notificaitonModel.message_en!;

      return "-";
  }

}
