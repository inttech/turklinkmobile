import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:turklink/models/request_model.dart';
import 'package:turklink/services/storage_services.dart';
import 'package:turklink/util/request_status.dart';

class RequestServices{

    String errorMessage = "";

    Future<bool> addPRequest(RequestModel requestModel) async
    {
      StorageServices storageServices = new StorageServices();
      try
      {
        DateTime now = DateTime.now();
        DateFormat formatter = DateFormat('yyyyMMddHms');

        requestModel.userId = FirebaseAuth.instance.currentUser!.uid;
        requestModel.propertiesId = requestModel.propertiesModel!.id;

        if(requestModel.mainRequestType!.key == "VALUATION_REPORT" || requestModel.mainRequestType!.key == "CLEANING")
          requestModel.status = RequestStatus.waitingPayment;
        else if(requestModel.mainRequestType!.key == "SERVICE_REQUEST")
          requestModel.status = RequestStatus.pendingOffer;
        else
          requestModel.status = RequestStatus.onProgress;

        requestModel.isDeleted = 0;
        requestModel.createdDate = now;
        requestModel.updatedDate = now;
        requestModel.id =  requestModel.userId! + "_" + formatter.format(now);

        if( requestModel.imageFile1 != null)
        {
          var uploadResult = await storageServices.uploadFile("requests", requestModel.id! + "_1", requestModel.imageFile1!);
          if(uploadResult.isEmpty)
          {
            errorMessage = storageServices.errorMessage;
            return false;
          }
          requestModel.imagePath1 = uploadResult;
        }

        if( requestModel.imageFile2 != null)
        {
          var uploadResult = await storageServices.uploadFile("requests", requestModel.id! + "_2", requestModel.imageFile2!);
          if(uploadResult.isEmpty)
          {
            errorMessage = storageServices.errorMessage;
            return false;
          }
          requestModel.imagePath2 = uploadResult;
        }

        if( requestModel.imageFile3 != null)
        {
          var uploadResult = await storageServices.uploadFile("requests", requestModel.id! + "_3", requestModel.imageFile3!);
          if(uploadResult.isEmpty)
          {
            errorMessage = storageServices.errorMessage;
            return false;
          }
          requestModel.imagePath3 = uploadResult;
        }

        if( requestModel.imageFile4 != null)
        {
          var uploadResult = await storageServices.uploadFile("requests", requestModel.id! + "_4", requestModel.imageFile4!);
          if(uploadResult.isEmpty)
          {
            errorMessage = storageServices.errorMessage;
            return false;
          }
          requestModel.imagePath4 = uploadResult;
        }

        await FirebaseFirestore.instance.collection("requests").doc(requestModel.id).set(requestModel.toJson());
        return true;
      }
      catch (error)
      {
        errorMessage = "TECHNICAL_ERROR";
      }

      return false;
    }

    Future<bool> updateRequest(RequestModel requestModel) async
    {
      StorageServices storageServices = new StorageServices();
      try
      {
        DateTime now = DateTime.now();
        requestModel.updatedDate = now;


        if( requestModel.imageFile1 != null)
        {
          var uploadResult = await storageServices.uploadFile("requests", requestModel.id! + "_1", requestModel.imageFile1!);
          if(uploadResult.isEmpty)
          {
            errorMessage = storageServices.errorMessage;
            return false;
          }
          requestModel.imagePath1 = uploadResult;
        }

        if( requestModel.imageFile2 != null)
        {
          var uploadResult = await storageServices.uploadFile("requests", requestModel.id! + "_2", requestModel.imageFile2!);
          if(uploadResult.isEmpty)
          {
            errorMessage = storageServices.errorMessage;
            return false;
          }
          requestModel.imagePath2 = uploadResult;
        }

        if( requestModel.imageFile3 != null)
        {
          var uploadResult = await storageServices.uploadFile("requests", requestModel.id! + "_3", requestModel.imageFile3!);
          if(uploadResult.isEmpty)
          {
            errorMessage = storageServices.errorMessage;
            return false;
          }
          requestModel.imagePath3 = uploadResult;
        }

        if( requestModel.imageFile4 != null)
        {
          var uploadResult = await storageServices.uploadFile("requests", requestModel.id! + "_4", requestModel.imageFile4!);
          if(uploadResult.isEmpty)
          {
            errorMessage = storageServices.errorMessage;
            return false;
          }
          requestModel.imagePath4 = uploadResult;
        }

        await FirebaseFirestore.instance.collection("requests").doc(requestModel.id).update(requestModel.toJson());
        return true;
      }
      catch (error)
      {
        errorMessage = "TECHNICAL_ERROR";
      }

      return false;
    }

    Future<RequestModel?> getRequest(String requestId) async
    {
      try
      {
          var requestResult = await FirebaseFirestore.instance.collection("requests").doc(requestId).get();
          if(requestResult.exists)
          {
              var requestData = RequestModel.fromJson(requestResult.data()!);
              return requestData;
          }
          else
          {
            errorMessage = "TECHNICAL_ERROR";
            return null;
          }
      }
      catch (error)
      {
          errorMessage = "TECHNICAL_ERROR";
      }
      return null;
    }

    Future<bool> deleteRequest(RequestModel requestModel) async
    {
      try
      {
        requestModel.isDeleted = 1;
        await FirebaseFirestore.instance.collection("requests").doc(requestModel.id!).update(requestModel.toJson());
        return true;
      }
      catch (error)
      {
        errorMessage = "TECHNICAL_ERROR";
      }

      return false;
    }

   Future<List<RequestModel>?> getRequestsByUser(String userId) async {
    try {
      var result = await FirebaseFirestore.instance.collection("requests")
                    .where('userId', isEqualTo: userId)
                    .where("isDeleted", isEqualTo: 0)
                    .where("status",whereIn: [RequestStatus.pendingOffer,RequestStatus.waitingPayment,RequestStatus.onProgress])
                    .get();
      var allData = result.docs.map((doc) => doc.data()).toList();
      var modelList = RequestModel.listFromJson(allData);
      return modelList;
    } catch (error) {
      errorMessage = "TECHNICAL_ERROR";
    }

    return null;
  }


}