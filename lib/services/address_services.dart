import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:turklink/models/key_value_model.dart';

class AddressServices {
  String errorMessage = "";

  Future<List<KeyValueModel>?> getCities() async {
    try {
      var result = await FirebaseFirestore.instance.collection("city").where("id",whereIn: ["34","41","54"]).get();
      var allData = result.docs.map((doc) => doc.data()).toList();
      var modelList = KeyValueModel.listFromJsonCity(allData);
      return modelList;
    } catch (error) {
      errorMessage = "TECHNICAL_ERROR";
    }

    return null;
  }

  Future<List<KeyValueModel>?> getDistrict(String cityId) async {
    try {
      var result = await FirebaseFirestore.instance.collection("district").where("ilId",isEqualTo: cityId).get();
      var allData = result.docs.map((doc) => doc.data()).toList();
      var modelList = KeyValueModel.listFromJsonDistrict(allData);
      return modelList;
    } catch (error) {
      errorMessage = "TECHNICAL_ERROR";
    }

    return null;
  }

  Future<List<KeyValueModel>?> getPlace(String dicrictId) async {
    try {
      var result = await FirebaseFirestore.instance.collection("place").where("ilceId",isEqualTo: dicrictId).get();
      var allData = result.docs.map((doc) => doc.data()).toList();
      var modelList = KeyValueModel.listFromJsonPlace(allData);
      return modelList;
    } catch (error) {
      errorMessage = "TECHNICAL_ERROR";
    }

    return null;
  }

  Future<List<KeyValueModel>?> getNeigborhood(String placeId) async {
    try {
      var result = await FirebaseFirestore.instance.collection("neighborhood").where("semtBucakBeldeId",isEqualTo: placeId).get();
      var allData = result.docs.map((doc) => doc.data()).toList();
      var modelList = KeyValueModel.listFromJsonNeighborhood(allData);
      return modelList;
    } catch (error) {
      errorMessage = "TECHNICAL_ERROR";
    }

    return null;
  }
}
