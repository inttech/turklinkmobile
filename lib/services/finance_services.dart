import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:turklink/models/creditcards_payment_model.dart';
import 'package:turklink/models/finances_model.dart';
import 'package:turklink/models/request_model.dart';
import 'package:turklink/models/take_money_model.dart';
import 'package:turklink/models/transaction_history_model.dart';
import 'package:turklink/services/properties_services.dart';
import 'package:turklink/services/request_services.dart';
import 'package:turklink/services/storage_services.dart';
import 'package:turklink/services/user_services.dart';
import 'package:turklink/util/finance_type.dart';
import 'package:turklink/util/property_process_status.dart';
import 'package:turklink/util/request_status.dart';
import 'package:turklink/util/transaction_status.dart';

class FinanceServices{

    String errorMessage = "";
    String returnData = "";

    Future<bool> doPayment(String propertyId,int amount,int financeType,String paymentMethod,String? requestId ) async
    {
      UserServices userServices = new UserServices();
      try
      {
        DateTime now = DateTime.now();
        DateFormat formatter = DateFormat('yyyyMMddHms');

        var userModel = await userServices.getUser(FirebaseAuth.instance.currentUser!.uid);
        if(userModel == null)
        {
          errorMessage = userServices.errorMessage;
          return false;
        }

        if(paymentMethod == "CREDIT_CARD")
        {
           CreditCardsPaymentModel creditCardsPaymentModel = new CreditCardsPaymentModel(
            amount:  amount,
            financeDescription: "FINANCE_DESC_" + financeType.toString(),
            financeType: financeType,
            id: FirebaseAuth.instance.currentUser!.uid + formatter.format(now),
            operationDate: now,
            propertyId: propertyId,
            requestId: requestId,
            userId: FirebaseAuth.instance.currentUser!.uid,
            phone: userModel.phone!,
            status: 0,
            userEmail: userModel.email,
            userName: userModel.name,
            financeDescriptionText: ("FINANCE_DESC_" + financeType.toString()).tr()
           );

           await FirebaseFirestore.instance.collection("creditcards_payment").doc(creditCardsPaymentModel.id).set(creditCardsPaymentModel.toJson());
           returnData = creditCardsPaymentModel.id!;
           return true;
        }

        if(paymentMethod == "WALLET")
        {
          var hasPendingRecord = await this.hasTakeMoneyRecordUser(FirebaseAuth.instance.currentUser!.uid);
          if(hasPendingRecord)
          {
            errorMessage = "error_messages.pending_record_take_money";
            return false;
          }
        }


        var currentBalance = userModel.tryBalance!;
        if(paymentMethod == "WALLET" && amount > currentBalance)
        {
          errorMessage = "error_messages.not_enough_money";
          return false;
        }


        if(financeType == FinanceType.propertyTax || financeType == FinanceType.earthquakeInsurance)
        {
          PropertiesServices propertiesServices = new PropertiesServices();
          var propertyModel = await propertiesServices.getProperty(propertyId);
          if(propertyModel == null)
          {
              errorMessage = propertiesServices.errorMessage;
              return false;
          }

          if(financeType == FinanceType.propertyTax)
          {
            propertyModel.propertyTaxProcessStatus = PropertyProcessStatus.pendingProcessFromAdmin;
          }

          if(financeType == FinanceType.earthquakeInsurance)
          {
            propertyModel.earthquakeProcessStatus = PropertyProcessStatus.pendingProcessFromAdmin;
          }

          var propertyResult = await propertiesServices.updateProperties(propertyModel);
          if(!propertyResult)
          {
              errorMessage = propertiesServices.errorMessage;
              return false;
          }
        }

        if(financeType == FinanceType.cleaning || financeType == FinanceType.report || financeType == FinanceType.serviceReqeust)
        {
          RequestServices requestServices = new RequestServices();
          var requestModel = await requestServices.getRequest(requestId!);
          if(requestModel == null)
          {
              errorMessage = requestServices.errorMessage;
              return false;
          }

          requestModel.status = RequestStatus.onProgress;

          var requestResult = await requestServices.updateRequest(requestModel);
          if(!requestResult)
          {
              errorMessage = requestServices.errorMessage;
              return false;
          }
        }

        if(paymentMethod == "WALLET")
        {
          userModel.tryBalance = currentBalance - amount;
          var usrUpdate = await userServices.updateUser(userModel);
          if(!usrUpdate)
          {
            errorMessage = userServices.errorMessage;
            return false;
          }
        }

        FinancesModel financesModel = new FinancesModel(
          amount:  amount,
          direction: "OUT",
          financeDescription: "FINANCE_DESC_" + financeType.toString(),
          financeType: financeType,
          id: FirebaseAuth.instance.currentUser!.uid + propertyId + formatter.format(now),
          operationDate: now,
          paymentMethod: paymentMethod,
          propertyId: propertyId,
          requestId: requestId,
          userId: FirebaseAuth.instance.currentUser!.uid
        );

        await FirebaseFirestore.instance.collection("finances").doc(financesModel.id).set(financesModel.toJson());

        TransactionHistoryModel transactionHistoryModel = new TransactionHistoryModel(
          amount: amount,
          financeId: financesModel.id,
          financeType: financeType,
          operationDate: now,
           paymentMethod: paymentMethod,
          propertyId: propertyId,
          requestId: requestId,
          userId: FirebaseAuth.instance.currentUser!.uid,
          txStatus: TransactionStatus.inProgress,
          id: FirebaseAuth.instance.currentUser!.uid + "_" + DateTime.now().millisecondsSinceEpoch.toString()
        );

        await FirebaseFirestore.instance.collection("transactions").doc(transactionHistoryModel.id).set(transactionHistoryModel.toJson());

        return true;
      }
      catch (error)
      {
        errorMessage = "TECHNICAL_ERROR";
      }

      return false;
    }

    Future<bool> doTakeMoney(String userId) async
    {
      UserServices userServices = new UserServices();
      try
      {
        DateTime now = DateTime.now();

        var userModel = await userServices.getUser(FirebaseAuth.instance.currentUser!.uid);
        if(userModel == null)
        {
          errorMessage = userServices.errorMessage;
          return false;
        }
        var currentBalance = userModel.tryBalance!;
        if(currentBalance <= 0)
        {
          errorMessage = "error_messages.not_enough_money";
          return false;
        }

        var hasPendingRecord = await this.hasTakeMoneyRecordUser(userId);
        if(hasPendingRecord)
        {
          errorMessage = "error_messages.pending_record_take_money";
          return false;
        }

        TakeMoneyModel takeMoneyModel = new TakeMoneyModel(
          amount: currentBalance,
          status: 0,
          isDeleted: 0,
          createdDate: now,
          iban: userModel.iban,
          userId: FirebaseAuth.instance.currentUser!.uid,
          id: FirebaseAuth.instance.currentUser!.uid + "_" + DateTime.now().millisecondsSinceEpoch.toString()
        );

        await FirebaseFirestore.instance.collection("take_money").doc(takeMoneyModel.id).set(takeMoneyModel.toJson());

        return true;
      }
      catch (error)
      {
        errorMessage = "TECHNICAL_ERROR";
      }

      return false;
    }


    Future<bool> hasTakeMoneyRecordUser(String userId) async
    {
      try
      {
          var result = await FirebaseFirestore.instance.collection("take_money").where("userId",isEqualTo: userId).where("status",isEqualTo: 0).get();
          if(result.docs.length > 0)
          {
              return true;
          }
          else
          {
            return false;
          }
      }
      catch (error)
      {
          return false;
      }
    }




}