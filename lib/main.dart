import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter_downloader/flutter_downloader.dart';
import 'package:turklink/models/push_notification_model.dart';
import 'package:turklink/services/user_services.dart';
import 'package:turklink/turklink_router.dart';
import 'package:turklink/views/pages/main/main_screen.dart';
import 'package:turklink/views/pages/splash_screen.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:firebase_core/firebase_core.dart';

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await EasyLocalization.ensureInitialized();
  await Firebase.initializeApp();
  await FlutterDownloader.initialize(
    debug: true
  );

  
  var user = await FirebaseAuth.instance.currentUser;
  String? userLang;
  if(user != null)
  {
    UserServices userServices = new UserServices();
    var userModel = await userServices.getUser(FirebaseAuth.instance.currentUser!.uid);
    if(userModel != null)
    {
      userModel.lastLoginDate = DateTime.now();
      userServices.updateUser(userModel);
      userLang = userModel.langCode;
    }

    if(userModel == null)
      user = null;
  }

  runApp(
    EasyLocalization(
        supportedLocales: [
          Locale('tr', 'TR'),
          Locale('en', 'US')
        ],
        path: 'assets/translations',
        fallbackLocale: Locale('tr', 'TR'),
        child: MyApp(
          hasCurrentUser: user != null,
          userLangCode: userLang,
        )),
  );
}

Future<void> _firebaseMessagingBackgroundHandler(RemoteMessage message) async {
  print(message);

}


class MyApp extends StatelessWidget {

  final bool hasCurrentUser;
  final String? userLangCode;
  PushNotificationModel? pushNotificationModel;

  MyApp({required this.hasCurrentUser,this.userLangCode});

  void requestAndRegisterNotification() async
  {
    var messaging = FirebaseMessaging.instance;
    FirebaseMessaging.onBackgroundMessage(_firebaseMessagingBackgroundHandler);

    NotificationSettings settings = await messaging.requestPermission(
      alert: true,
      announcement: false,
      badge: true,
      carPlay: false,
      criticalAlert: false,
      provisional: false,
      sound: true,
    );

    if(settings.authorizationStatus == AuthorizationStatus.authorized)
    {
      String? token = await messaging.getToken();
      print("token" + token!);

      FirebaseMessaging.onMessage.listen((RemoteMessage message) {
        PushNotificationModel notificationModel = new PushNotificationModel(
          title: message.notification?.title,
          body: message.notification?.body
        );
        print(notificationModel);
      });
    }

  }

  @override
  Widget build(BuildContext context) {

    Locale localLang = context.locale;
    if(userLangCode != null)
    {
      if(userLangCode == "tr")
        localLang = Locale("tr","TR");
      else if(userLangCode == "en")
        localLang = Locale("en","US");
      else 
        localLang = Locale("en","US");
    }

    context.setLocale(localLang);

    requestAndRegisterNotification();

    final textTheme = Theme.of(context).textTheme;
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      localizationsDelegates: context.localizationDelegates,
      supportedLocales: context.supportedLocales,
      locale: context.locale,
      title: 'Turk.Link',
      routes: TurklinkRouter.getRoutes(),
      home: hasCurrentUser ? MainScreen() : SplashScreen(),
      theme: ThemeData(
        primaryColor: Colors.white,
        textTheme: GoogleFonts.poppinsTextTheme(textTheme).copyWith(
          bodyText1: GoogleFonts.poppins(textStyle: textTheme.bodyText1),
        ),
      ),
    );
  }
}
