import 'package:flutter/material.dart';
import 'package:turklink/util/turklink_colors.dart';

class TurklinkTextField extends StatelessWidget {
  final String labelText;
  final bool? obscureText;
  final Function? onTap;
  final String? errorText;
  final TextEditingController controller;
  final TextInputType? textInputType;
  final Color? borderColor;
  final int? maxLines;
  const TurklinkTextField(
      {required this.labelText,
      this.obscureText,
      this.errorText,
      required this.controller,
      this.textInputType,this.onTap,this.borderColor,this.maxLines});

  @override
  Widget build(BuildContext context) {
    return TextField(
      keyboardType:
          this.textInputType != null ? this.textInputType : TextInputType.text,
      obscureText: this.obscureText != null ? this.obscureText! : false,
      controller: this.controller,
      onTap: (){
        if(this.onTap != null)
          this.onTap!();
      },
      maxLines: this.maxLines,
      minLines: this.maxLines,
      readOnly: this.onTap != null,
      style: TextStyle(height: 1),
      decoration: InputDecoration(
          errorText: this.errorText,
          errorStyle: TextStyle(color: Colors.red.withOpacity(.7)),
          focusedErrorBorder: OutlineInputBorder(
              gapPadding: 0,
              borderSide: BorderSide(
                color: Colors.red.withOpacity(.7),
              )),
          errorBorder: OutlineInputBorder(
              gapPadding: 0,
              borderSide: BorderSide(
                color: Colors.red.withOpacity(.7),
              )),
          filled: true,
          fillColor: Colors.white,
          labelText: this.labelText,
          enabledBorder: OutlineInputBorder(
            borderRadius: BorderRadius.circular(12),
            borderSide: BorderSide(
              color: this.borderColor != null ? this.borderColor! : Colors.white,
              width: 0.0,
            ),
          ),
          disabledBorder: OutlineInputBorder(
            borderRadius: BorderRadius.circular(12),
            borderSide: BorderSide(
              color: this.borderColor != null ? this.borderColor! : Colors.white,
              width: this.borderColor != null ? 0.5 : 0.0,
            ),
          ),
          focusedBorder: OutlineInputBorder(
            borderRadius: BorderRadius.circular(12),
            borderSide: BorderSide(
              color: TurklinkColors.mainColor,
              width: 2.0,
            ),
          ),
          labelStyle: TextStyle(color: Colors.black54)),
    );
  }
}
