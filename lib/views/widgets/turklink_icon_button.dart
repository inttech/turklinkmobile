import 'package:flutter/material.dart';

class TurklinkIconButton extends StatelessWidget {
  final String buttonText;
  final Color backgroundColor;
  final Color borderColor;
  final Color textColor;
  final Function onPressed;
  final IconData iconData;
  final double? fontSize;
  TurklinkIconButton({required this.buttonText,required this.backgroundColor,required this.borderColor,required this.textColor,required this.iconData,this.fontSize,required this.onPressed});

  @override
  Widget build(BuildContext context) {
    return ElevatedButton(
        child: Row(
          children: [
            Icon(this.iconData,size: this.fontSize != null ? this.fontSize! + 8 : 22,),
            SizedBox(width: 5,),
            Text(this.buttonText, style: TextStyle(fontSize: this.fontSize != null ? this.fontSize : 14))
          ],
        ),
        style: ButtonStyle(
            foregroundColor: MaterialStateProperty.all<Color>(this.textColor),
            backgroundColor: MaterialStateProperty.all<Color>(this.backgroundColor),
            shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(10),
                    side: BorderSide(color: this.borderColor)))),
        onPressed: () => this.onPressed());
  }
}
