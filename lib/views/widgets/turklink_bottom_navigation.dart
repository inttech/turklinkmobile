import 'dart:io';

import 'package:flutter/material.dart';
import 'package:turklink/util/turklink_colors.dart';
import 'package:easy_localization/easy_localization.dart';

class TurklinkBottomNavigaiton extends StatelessWidget {

  final Function onTap;
  final int? currentIndex;
  const TurklinkBottomNavigaiton({required this.onTap,this.currentIndex}) ;

  @override
  Widget build(BuildContext context) {
    return Theme(
      data: Theme.of(context).copyWith(
          canvasColor: Colors.white,
          primaryColor: TurklinkColors.mainColor,
          textTheme: Theme.of(context).textTheme.copyWith(
              caption: TextStyle(color: TurklinkColors.grayBackground))),
      child: SafeArea(
        child: Container(
          height:  Platform.isIOS ? 60: 60,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.end,
            crossAxisAlignment: CrossAxisAlignment.end,
            children: [
              Container(
                width: double.infinity,
                height: 4,
                alignment: Alignment.center,
                decoration: BoxDecoration(
                  gradient: LinearGradient(
                    begin: Alignment.centerLeft,
                    end: Alignment.centerRight,
                    colors: [
                      TurklinkColors.mainColor2,
                      TurklinkColors.mainColor,
                    ],
                  ),
                ),
              ),
              BottomNavigationBar(
                selectedFontSize: 10,
                unselectedFontSize: 10,
                backgroundColor: Colors.white,
                type: BottomNavigationBarType.fixed,
                selectedItemColor: TurklinkColors.mainColor,
                elevation: 0,
                currentIndex: this.currentIndex != null ? this.currentIndex! : 0,
                onTap: (value){
                  this.onTap(value);
                },
                items: [
                  BottomNavigationBarItem(
                    icon: Icon(Icons.home_outlined),
                    label: "main_screen.home_menu".tr(),
                  ),
                  BottomNavigationBarItem(
                    icon: Icon(Icons.dynamic_feed_outlined),
                    label: "main_screen.my_properties_menu".tr(),
                  ),
                  BottomNavigationBarItem(
                    icon: Icon(Icons.credit_card_outlined),
                    label: "main_screen.finances_menu".tr(),
                  ),
                  BottomNavigationBarItem(
                    icon: Icon(Icons.settings),
                    label: "main_screen.settings_menu".tr(),
                  ),
                  BottomNavigationBarItem(
                    icon: Icon(Icons.chat_outlined),
                    label: "main_screen.service_center_menu".tr(),
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}
