import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:intl_phone_field/phone_number.dart';
import 'package:turklink/util/turklink_colors.dart';
import 'package:intl_phone_field/intl_phone_field.dart';

class TurklinkPhoneTextFiled extends StatelessWidget {

  final TextEditingController controller;
  final Function(PhoneNumber) phoneNumberValueChange;
  final String? errorText;
  final String? initialCountryCode;

  TurklinkPhoneTextFiled({required this.controller,required this.phoneNumberValueChange,this.errorText,this.initialCountryCode});

  @override
  Widget build(BuildContext context) {
    return IntlPhoneField(
      searchText: "register_screen.search_title".tr(),
      autovalidateMode: AutovalidateMode.disabled,
      controller: controller,
      disableLengthCheck: true,
      initialCountryCode: this.initialCountryCode == null ? "TR" : this.initialCountryCode,
      onChanged: (phone) {
        phoneNumberValueChange(phone);
      },
      keyboardType:
          TextInputType.numberWithOptions(signed: true, decimal: true),
      decoration: InputDecoration(
        errorText: this.errorText,
          errorStyle: TextStyle(color: Colors.red.withOpacity(.7)),
          focusedErrorBorder: OutlineInputBorder(
              gapPadding: 0,
              borderSide: BorderSide(
                color: Colors.red.withOpacity(.7),
              )),
          errorBorder: OutlineInputBorder(
              gapPadding: 0,
              borderSide: BorderSide(
                color: Colors.red.withOpacity(.7),
              )),
          filled: true,
          fillColor: Colors.white,
          labelText: "register_screen.phone_number".tr(),
          enabledBorder: OutlineInputBorder(
            borderRadius: BorderRadius.circular(12),
            borderSide: BorderSide(
              color: Colors.white,
              width: 0.0,
            ),
          ),
          focusedBorder: OutlineInputBorder(
            borderRadius: BorderRadius.circular(12),
            borderSide: BorderSide(
              color: TurklinkColors.mainColor,
              width: 2.0,
            ),
          ),
          labelStyle: TextStyle(color: Colors.black54)),
    );
  }
}
