import 'package:flutter/material.dart';

class TurklinkEmptyScreen extends StatelessWidget {

  final String message;
  TurklinkEmptyScreen({required this.message});

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Icon(Icons.info_outline,color: Colors.grey,size: 60,),
          SizedBox(height: 10,),
          Text(this.message,style: TextStyle(color: Colors.grey,fontSize: 18),textAlign: TextAlign.center,)
        ],
      ),
    );
  }
}