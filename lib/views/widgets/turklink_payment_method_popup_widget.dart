import 'package:another_flushbar/flushbar_helper.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:rounded_loading_button/rounded_loading_button.dart';
import 'package:turklink/models/finances_model.dart';
import 'package:turklink/services/finance_services.dart';
import 'package:turklink/util/turklink_colors.dart';
import 'package:turklink/util/utility.dart';
import 'package:turklink/views/pages/main/finances/paytr_creditcard_screen.dart';
import 'package:turklink/views/widgets/turklink_button.dart';

class TurklinkPaymentMethodPopupWidget extends StatefulWidget {
  final BuildContext parentContext;
  final int? turkLinkBalance;
  final int? financeType;
  final int?  amount;
  final String? propertyId;
  final String? requestId;
  TurklinkPaymentMethodPopupWidget(
      {required this.parentContext,this.turkLinkBalance,this.financeType,this.amount,this.propertyId,this.requestId});

  @override
  State<TurklinkPaymentMethodPopupWidget> createState() => _TurklinkPaymentMethodPopupWidgetState();
}

class _TurklinkPaymentMethodPopupWidgetState extends State<TurklinkPaymentMethodPopupWidget> {

  var sendController = new RoundedLoadingButtonController();
  String? paymentMethod;
  FinanceServices financeServices = new FinanceServices();

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(12)),
      titlePadding: EdgeInsets.symmetric(vertical: 20, horizontal: 20),
      contentPadding: EdgeInsets.symmetric(vertical: 10, horizontal: 20),
      title: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Text(
            "utility.payment_method".tr(),
            style: TextStyle(fontSize: 15),
          ),
          InkWell(
            onTap: () {
              Navigator.of(widget.parentContext).pop();
            },
            child: Icon(
              Icons.cancel,
              color: Colors.red,
              size: 28,
            ),
          )
        ],
      ),
      content: Wrap(
        children: [
          Column(
            children: [
              ListTile(
                onTap: (){
                  paymentMethod = "WALLET";
                  setState(() {

                  });
                },
                contentPadding: EdgeInsets.zero,
                leading: Icon(Icons.account_balance_wallet),
                title: Text("utility.turlink_wallet".tr(),style: TextStyle(fontSize: 12,fontWeight: FontWeight.bold),),
                subtitle: widget.turkLinkBalance != null ? Text(Utility.convertCurrency(widget.turkLinkBalance!) + " TL",style: TextStyle(color: TurklinkColors.mainColor,fontWeight: FontWeight.bold),) : null,
                trailing: paymentMethod != null && paymentMethod == "WALLET" ? Icon(Icons.check,color: Colors.green,) : Container(width: 5,height: 5,),
              ),
              Divider(),
              ListTile(
                 onTap: (){
                  paymentMethod = "CREDIT_CARD";
                  setState(() {

                  });
                },
                 contentPadding: EdgeInsets.zero,
                leading: Icon(Icons.credit_card),
                title: Text("utility.credit_card".tr(),style: TextStyle(fontSize: 12,fontWeight: FontWeight.bold),),
                trailing: paymentMethod != null && paymentMethod == "CREDIT_CARD" ? Icon(Icons.check,color: Colors.green,) : Container(width: 5,height: 5,),
              ),
            ],
          )
        ],
      ),
      actionsAlignment: MainAxisAlignment.start,
      actionsPadding: EdgeInsets.only(left: 10, right: 10, bottom: 10),
      actions: [
        SizedBox(
          height: 52,
          width: MediaQuery.of(context).size.width,
          child: TurklinkButton(
            width: MediaQuery.of(context).size.width,
            backgroundColor: TurklinkColors.mainColor,
            buttonText: "request_screen.pay_button".tr(),
            borderColor: TurklinkColors.mainColor2,
            textColor: Colors.white,
            btnController: sendController,
            animatedOnTap: true,
            onPressed: () async {
              if(paymentMethod == null)
              {
                sendController.reset();
                FlushbarHelper.createError(
                  title: "popup_message.popup_error_title".tr(),
                  message: "finances_screen.not_select_payment_method".tr()
                ).show(widget.parentContext);

                return;
              }

              var result = await financeServices.doPayment(widget.propertyId!,widget.amount!,widget.financeType!,paymentMethod!,widget.requestId);

              if(!result)
              {
                sendController.reset();
                FlushbarHelper.createError(
                  title: "popup_message.popup_error_title".tr(),
                  message: (financeServices.errorMessage).tr()
                ).show(widget.parentContext);
              }
              else
              {
                if(paymentMethod == "WALLET")
                {
                  sendController.reset();
                  Navigator.of(widget.parentContext).pop();
                  await FlushbarHelper.createSuccess(
                    title: "popup_message.popup_success_title".tr(),
                    message: "popup_message.popup_success_message".tr(),
                  ).show(widget.parentContext);
                }
                else
                {
                  sendController.reset();
                  await Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) => PayTrCreditCardScreen(creditCardPaymentId: financeServices.returnData),
                      ),
                  );
                  Navigator.of(widget.parentContext).pop();
                }
              }
            },
          ),
        ),
      ],
    );
  }
}