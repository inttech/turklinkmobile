import 'package:flutter/material.dart';
import 'package:turklink/models/key_value_model.dart';
import 'package:turklink/util/turklink_colors.dart';

class TurklinkDropdownField extends StatelessWidget {
  final Function onChanged;
  final Function? onTap;
  final String labelText;
  final List<KeyValueModel>? list;
  final KeyValueModel? value;
  final Color? borderColor;
  final String? diffrentColorKey;
  TurklinkDropdownField({required this.onChanged,required this.labelText,this.list,this.value,this.borderColor,this.diffrentColorKey,this.onTap});

  @override
  Widget build(BuildContext context) {
    return DropdownButtonFormField<String>(
      items: this.list != null ?  this.list!
          .map((KeyValueModel item) {
        return DropdownMenuItem<String>(
          value: item.key,
          child: (this.diffrentColorKey != null && item.key == this.diffrentColorKey! ) ? Text(item.value!,style: TextStyle(fontWeight: FontWeight.bold, color: Color.fromARGB(255, 45, 105, 47)),) : Text(item.value!),
        );
      }).toList() : null,
      onChanged: (item) {
        var itemObject = this.list?.where((element) => element.key == item).first;
        this.onChanged(itemObject);
      },
      onTap: (){
        if(this.onTap != null)
          this.onTap!();
      },
      value: this.value != null ? this.value!.key : null,
      isExpanded: true,
      decoration: InputDecoration(
        filled: true,
        contentPadding: EdgeInsets.symmetric(vertical: 16,horizontal: 12),
        fillColor: Colors.white,
        labelText: this.labelText,
        enabledBorder: OutlineInputBorder(
          borderRadius: BorderRadius.circular(12),
          borderSide: BorderSide(
            color: this.borderColor != null ? this.borderColor! : Colors.white,
            width: 0.0,
          ),
        ),
        focusedBorder: OutlineInputBorder(
          borderRadius: BorderRadius.circular(12),
          borderSide: BorderSide(
            color: TurklinkColors.mainColor,
            width: 2.0,
          ),
        ),
        labelStyle: TextStyle(color: Colors.black54),
      ),
    );
  }
}
