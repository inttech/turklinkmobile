import 'package:flutter/material.dart';
import 'package:rounded_loading_button/rounded_loading_button.dart';

class TurklinkButton extends StatelessWidget {
  final String buttonText;
  final Color backgroundColor;
  final Color borderColor;
  final Color textColor;
  final Function onPressed;
  final double width;
  final double? fontSize;
  final RoundedLoadingButtonController btnController;
  final bool? animatedOnTap;
  TurklinkButton({required this.buttonText,required this.backgroundColor,required this.borderColor,required this.textColor,required this.onPressed,required this.btnController,required this.width,this.fontSize,this.animatedOnTap});

  @override
  Widget build(BuildContext context) {
    return RoundedLoadingButton(
        height: 52,
        width: width,
        child: Text(this.buttonText, style: TextStyle(fontSize: this.fontSize != null ? this.fontSize : 14,color: this.textColor)),
        color: this.backgroundColor,
        borderRadius: 12,
        animateOnTap: this.animatedOnTap != null ? this.animatedOnTap! : true,
        controller: this.btnController,
        onPressed: () async{
          await this.onPressed();
        });
  }
}
