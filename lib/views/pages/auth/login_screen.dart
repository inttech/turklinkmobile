import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:rounded_loading_button/rounded_loading_button.dart';
import 'package:turklink/services/user_services.dart';
import 'package:turklink/services/utility_services.dart';
import 'package:turklink/turklink_router.dart';
import 'package:turklink/util/turklink_colors.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:turklink/views/widgets/turklink_button.dart';
import 'package:turklink/views/widgets/turklink_textfield.dart';

class LoginScreen extends StatefulWidget {
  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  @override
  void initState() {
    super.initState();
  }

  var eMailController = TextEditingController();
  var passwordController = TextEditingController();
  var btnController = new RoundedLoadingButtonController();
  var registerBtnController = new RoundedLoadingButtonController();
  var userServices = new UserServices();
  String? emailErrorMessage;
  String? passwordErrorMessage;

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Container(
        width: MediaQuery.of(context).size.width,
        height: MediaQuery.of(context).size.height-150,
        child: Padding(
          padding: EdgeInsets.only(left: 20, right: 20, bottom: 15),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.end,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(
                  padding: EdgeInsets.only(left: 10, right: 10),
                  child: Text(
                    "login_screen.login_top_text",
                    textAlign: TextAlign.center,
                  ).tr()),
              SizedBox(
                height: 20,
              ),
              TurklinkTextField(
                labelText: "login_screen.email_input".tr(),
                controller: eMailController,
                errorText: emailErrorMessage,
                maxLines: 1,
                textInputType: TextInputType.emailAddress,
              ),
              SizedBox(
                height: 15,
              ),
              TurklinkTextField(
                labelText: "login_screen.password_input".tr(),
                obscureText: true,
                errorText: passwordErrorMessage,
                maxLines: 1,
                controller: passwordController,
              ),
              SizedBox(
                height: 5,
              ),
              GestureDetector(
                child: Text(
                  "login_screen.forget_password",
                  style: TextStyle(
                      color: TurklinkColors.mainColor,
                      fontSize: 12,
                      fontWeight: FontWeight.w600),
                ).tr(),
                onTap: () {
                  Navigator.pushNamed(context, TurklinkRouter.ForgetPassword);
                },
              ),
              SizedBox(
                height: 15,
              ),
              SizedBox(
                height: 52,
                width: MediaQuery.of(context).size.width,
                child: TurklinkButton(
                  width: MediaQuery.of(context).size.width,
                  backgroundColor: TurklinkColors.mainColor,
                  buttonText: "login_screen.entry_button".tr(),
                  borderColor: TurklinkColors.mainColor,
                  textColor: Colors.white,
                  btnController: btnController,
                  animatedOnTap: true,
                  onPressed: () async {
                    setState(() {
                      emailErrorMessage = null;
                      passwordErrorMessage = null;
                    });
                    User? model = await userServices.login(
                        eMailController.text, passwordController.text);
                    btnController.reset();
                    if (model != null) {
                      Navigator.pushReplacementNamed(context, TurklinkRouter.Main);
                    } else {
                       if(userServices.errorMessage == "error_messages.login_email_empty" || userServices.errorMessage == "error_messages.login_user_not_found" || userServices.errorMessage == "error_messages.login_user_disabled" )
                       {
                         setState(() {
                           emailErrorMessage = userServices.errorMessage.tr();
                         });
                       }
                       if(userServices.errorMessage == "error_messages.login_password_empty"  || userServices.errorMessage == "error_messages.login_user_wrong_password")
                       {
                         setState(() {
                           passwordErrorMessage = userServices.errorMessage.tr();
                         });
                       }
                       if(userServices.errorMessage == "error_messages.login_email_not_valid")
                       {
                         setState(() {
                           emailErrorMessage = userServices.errorMessage.tr();
                         });
                       }
                    }
                  },
                ),
              ),
              SizedBox(
                height: 15,
              ),
              Center(
                child: Text(
                      "login_screen.has_not_account".tr(),
                      style: TextStyle(fontWeight: FontWeight.bold),
                ),
              ),
              SizedBox(
                height: 15,
              ),
              SizedBox(
                height: 52,
                width: MediaQuery.of(context).size.width,
                child: TurklinkButton(
                  width: MediaQuery.of(context).size.width,
                  backgroundColor: TurklinkColors.mainColor2,
                  buttonText: "login_screen.register".tr(),
                  borderColor: TurklinkColors.mainColor2,
                  textColor: Colors.white,
                  btnController: registerBtnController,
                  animatedOnTap: false,
                  onPressed: () async {
                    Navigator.pushNamed(
                          context, TurklinkRouter.Register);
                  },
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
/*todo */