import 'package:another_flushbar/flushbar_helper.dart';
import 'package:flutter/material.dart';
import 'package:flutter_statusbarcolor_ns/flutter_statusbarcolor_ns.dart';
import 'package:rounded_loading_button/rounded_loading_button.dart';
import 'package:turklink/services/user_services.dart';
import 'package:turklink/util/turklink_colors.dart';
import 'package:turklink/util/utility.dart';
import 'package:turklink/views/widgets/turklink_button.dart';
import 'package:turklink/views/widgets/turklink_textfield.dart';
import 'package:easy_localization/easy_localization.dart';

class ForgetPasswordScreen extends StatefulWidget {

  @override
  _ForgetPasswordScreenState createState() => _ForgetPasswordScreenState();
}

class _ForgetPasswordScreenState extends State<ForgetPasswordScreen> {
  @override
  void initState() {
    changeStatusBar();
    super.initState();
  }

  changeStatusBar() async {
    await FlutterStatusbarcolor.setStatusBarColor(
        TurklinkColors.grayBackground);
  }

  var btnController = new RoundedLoadingButtonController();
  var emailController = TextEditingController();
  var userServices = new UserServices();
  String? emailErrorMessage;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: TurklinkColors.grayBackground,
      appBar: AppBar(
        backgroundColor: TurklinkColors.grayBackground,
        elevation: 0,
        title: Text(
          "forget_password_screen.forget_password_title",
          style: TextStyle(
              color: Colors.black, fontSize: 17, fontWeight: FontWeight.bold),
        ).tr(),
        centerTitle: true,
        automaticallyImplyLeading: true,
        iconTheme: IconThemeData(color: Colors.black),
      ),
      body: Padding(
        padding: EdgeInsets.all(20),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Image.asset(
              "assets/images/logo.png",
              width: 200,
            ),
            SizedBox(
              height: 75,
            ),
            Container(
                padding: EdgeInsets.only(left: 10, right: 10),
                child: Text(
                  "forget_password_screen.forget_password_top_text",
                  textAlign: TextAlign.center,
                ).tr()),
            SizedBox(
              height: 35,
            ),
            TurklinkTextField(
              labelText: "forget_password_screen.email_input".tr(),
              textInputType: TextInputType.emailAddress,
              controller: emailController,
              errorText: emailErrorMessage,
            ),
            SizedBox(
              height: 25,
            ),
            SizedBox(
              height: 52,
              width: MediaQuery.of(context).size.width,
              child: TurklinkButton(
                width: MediaQuery.of(context).size.width,
                btnController: btnController,
                backgroundColor: TurklinkColors.mainColor,
                buttonText: "forget_password_screen.continue_button".tr(),
                borderColor: TurklinkColors.mainColor,
                textColor: Colors.white,
                animatedOnTap: true,
                onPressed: () async{
                  setState(() {
                    emailErrorMessage = null;
                  });
                  var result = await userServices.resetPassword(emailController.text);
                  btnController.reset();
                  if(!result)
                  {
                    setState(() {
                       emailErrorMessage = userServices.errorMessage.tr();
                     });
                  }
                  else
                  {
                    await FlushbarHelper.createSuccess(
                      message: "forget_password_screen.send_email_content".tr(),
                      title: "forget_password_screen.send_email_title".tr()
                    ).show(context);;
                  }
                },
              ),
            ),
          ],
        ),
      ),
    );
  }
}
