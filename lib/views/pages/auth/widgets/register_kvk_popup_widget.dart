import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:turklink/services/utility_services.dart';
import 'package:flutter_widget_from_html/flutter_widget_from_html.dart';

class RegisterKvkkPopupWidget extends StatefulWidget {
  final BuildContext parentContext;
  final String contractType;
  const RegisterKvkkPopupWidget({required this.parentContext,required this.contractType});

  @override
  State<RegisterKvkkPopupWidget> createState() => _RegisterKvkkPopupWidgetState();
}

class _RegisterKvkkPopupWidgetState extends State<RegisterKvkkPopupWidget> {

  UtilityServices utilityServices = new UtilityServices();
  String contractDetail = "";

  @override
  void initState() {
    getContractDetail();
    super.initState();
  }

  getContractDetail() async
  {
    if(widget.contractType == "KVKK")
    {
      var dlv = await utilityServices.getKvkkContract();
      setState(() {
        contractDetail = dlv.contractContent!;
      });
    }
    if(widget.contractType == "PRIVACY")
    {
      var dlv = await utilityServices.getConfContract();
      setState(() {
        contractDetail = dlv.contractContent!;
      });
    }
  }

  @override
  Widget build(BuildContext context) {

    return AlertDialog(
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(12)),
      titlePadding: EdgeInsets.symmetric(vertical: 20, horizontal: 20),
      contentPadding: EdgeInsets.symmetric(vertical: 10, horizontal: 20),
      title: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Text(
            widget.contractType == "KVKK" ? "register_screen.register_kvkk_header".tr() : "register_screen.register_conf_header".tr(),
            style: TextStyle(fontSize: 15),
          ),
          InkWell(
            onTap: () {
              Navigator.of(widget.parentContext).pop();
            },
            child: Icon(
              Icons.cancel,
              color: Colors.red,
              size: 28,
            ),
          )
        ],
      ),
      content: SingleChildScrollView(
        child: Container(
          child: contractDetail == "" ? Container() : HtmlWidget(contractDetail),
        ),
      ),
    );
  }
}