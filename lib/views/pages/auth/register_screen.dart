import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_otp_text_field/flutter_otp_text_field.dart';
import 'package:flutter_statusbarcolor_ns/flutter_statusbarcolor_ns.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:pinput/pinput.dart';
import 'package:rounded_loading_button/rounded_loading_button.dart';
import 'package:timer_count_down/timer_count_down.dart';
import 'package:turklink/models/user_model.dart';
import 'package:turklink/services/user_services.dart';
import 'package:turklink/services/utility_services.dart';
import 'package:turklink/util/turklink_colors.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:turklink/util/utility.dart';
import 'package:turklink/views/widgets/turklink_button.dart';
import 'package:turklink/views/widgets/turklink_phone_textfield.dart';
import 'package:turklink/views/widgets/turklink_textfield.dart';

import '../../../turklink_router.dart';

class RegisterScreen extends StatefulWidget {
  @override
  _RegisterScreenState createState() => _RegisterScreenState();
}

class _RegisterScreenState extends State<RegisterScreen> {
  @override
  void initState() {
    changeStatusBar();
    super.initState();
  }

  bool emailVerifiactionVisible = false;
  bool phoneVerifiactionVisible = false;

  changeStatusBar() async {
    await FlutterStatusbarcolor.setStatusBarColor(
        TurklinkColors.grayBackground);
  }

  var phoneChangeValue = "";

  var fullNameController = TextEditingController();
  var eMailController = TextEditingController();
  var passwordController = TextEditingController();
  var rePasswordController = TextEditingController();
  var phoneController = TextEditingController();

  var btnController = new RoundedLoadingButtonController();
  var emailBtnController = new RoundedLoadingButtonController();
  var phoneBtnController = new RoundedLoadingButtonController();

  final controller = TextEditingController();
  var emailVerificationController = TextEditingController();
  final focusNodeEmail = FocusNode();
  final focusNode = FocusNode();


  UserServices userServices = new UserServices();
  String? nameErrorMessage;
  String? emailErrorMessage;
  String? passwordErrorMessage;
  String? rePasswordErrorMessage;
  String? phoneErrorMessage;
  String phoneRequestId = "";
  String phoneVerificationCode = "";
  String emailRequestId = "";
  String emailVerificationCode = "";
  bool kvkkText = false;
  bool privacyText = false;
  String? initialCountryCode = "TR";

  @override
  Widget build(BuildContext context) {
    final defaultPinTheme = PinTheme(
      width: 78,
      height: 64,
      decoration:
           BoxDecoration(color: Colors.white,borderRadius: BorderRadius.circular(12)),
    );

    return Scaffold(
      backgroundColor: TurklinkColors.grayBackground,
      appBar: AppBar(
        backgroundColor: TurklinkColors.grayBackground,
        elevation: 0,
        title: Text(
          "register_screen.register_title",
          style: TextStyle(
              color: Colors.black, fontSize: 17, fontWeight: FontWeight.bold),
        ).tr(),
        centerTitle: true,
        automaticallyImplyLeading: true,
        iconTheme: IconThemeData(color: Colors.black),
      ),
      body: SafeArea(
        child: SingleChildScrollView(
          child: Padding(
            padding: EdgeInsets.all(20),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.end,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                SizedBox(
                  height: 15,
                ),
                Container(
                    padding: EdgeInsets.only(left: 10, right: 10),
                    child: Text(
                      "register_screen.register_top_text",
                      textAlign: TextAlign.center,
                    ).tr()),
                SizedBox(
                  height: 35,
                ),
                TurklinkTextField(
                  labelText: "register_screen.name_surname_input".tr(),
                  controller: fullNameController,
                  errorText: nameErrorMessage,
                ),
                SizedBox(
                  height: 25,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Expanded(
                      child: TurklinkTextField(
                        labelText: "register_screen.email_input".tr(),
                        controller: eMailController,
                        errorText: emailErrorMessage,
                      ),
                    ),
                    SizedBox(
                      width: 10,
                    ),
                    SizedBox(
                      width: 60,
                      child: !emailVerifiactionVisible
                          ? TurklinkButton(
                              width: 60,
                              backgroundColor: TurklinkColors.mainColor,
                              buttonText: "register_screen.approve_button".tr(),
                              fontSize: 10,
                              borderColor: TurklinkColors.mainColor,
                              textColor: Colors.white,
                              btnController: emailBtnController,
                              onPressed: () async {
                                var result = await userServices
                                    .sendOtpEmail(eMailController.text);
                                if (result != null) {
                                  emailBtnController.reset();
                                  setState(() {
                                    emailErrorMessage = "";
                                    emailVerifiactionVisible = true;
                                    emailRequestId = result;
                                  });
                                } else {
                                  emailBtnController.reset();
                                  setState(() {
                                    emailErrorMessage =
                                        userServices.errorMessage.tr();
                                  });
                                }
                              },
                            )
                          : Center(
                              child: Countdown(
                                seconds: 300,
                                build: (BuildContext context, double time) {
                                  var minutes =
                                      '${(Duration(seconds: time.toInt()))}'
                                          .split('.')[0]
                                          .padLeft(8, '0')
                                          .split(":");
                                  return Text(
                                    minutes[1] + ":" + minutes[2],
                                    style: TextStyle(
                                        fontSize: 16,
                                        color: Colors.red.shade700),
                                  );
                                },
                                interval: Duration(seconds: 1),
                                onFinished: () {
                                  setState(() {
                                    emailVerifiactionVisible = false;
                                  });
                                },
                              ),
                            ),
                    ),
                  ],
                ),
                Visibility(
                  visible: emailVerifiactionVisible,
                  child: Column(
                    children: [
                      SizedBox(
                        height: 25,
                      ),
                      Pinput(
                        length: 4,
                        controller: emailVerificationController,
                        focusNode: focusNodeEmail,
                        defaultPinTheme: defaultPinTheme,
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        showCursor: true,
                        focusedPinTheme: defaultPinTheme.copyWith(
                          width: 78,
                          decoration: BoxDecoration(color: Colors.white,borderRadius: BorderRadius.circular(12)),
                        ),
                        onCompleted: ((value) {
                          emailVerificationCode = value;
                        }),
                      ),
                    ],
                  ),
                ),
                SizedBox(
                  height: 25,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Expanded(
                      child: TurklinkPhoneTextFiled(
                        controller: phoneController,
                        errorText: phoneErrorMessage,
                        initialCountryCode: initialCountryCode,
                        phoneNumberValueChange: (phone) {
                          phoneChangeValue = phone.completeNumber;
                          initialCountryCode = phone.countryCode;
                        },
                      ),
                    ),
                    SizedBox(
                      width: 10,
                    ),
                    SizedBox(
                      width: 60,
                      child: !phoneVerifiactionVisible
                          ? TurklinkButton(
                              width: 60,
                              backgroundColor: TurklinkColors.mainColor,
                              buttonText: "register_screen.approve_button".tr(),
                              fontSize: 10,
                              borderColor: TurklinkColors.mainColor,
                              textColor: Colors.white,
                              btnController: phoneBtnController,
                              onPressed: () async {
                                var result = await userServices
                                    .sendOtpSms(phoneChangeValue);
                                phoneBtnController.reset();
                                if (result != null) {
                                  setState(() {
                                    phoneErrorMessage = "";
                                    phoneVerifiactionVisible = true;
                                    phoneRequestId = result;
                                  });
                                } else {
                                  phoneBtnController.reset();
                                  setState(() {
                                    phoneErrorMessage =
                                        userServices.errorMessage.tr();
                                  });
                                }
                              },
                            )
                          : Center(
                              child: Countdown(
                                seconds: 300,
                                build: (BuildContext context, double time) {
                                  var minutes =
                                      '${(Duration(seconds: time.toInt()))}'
                                          .split('.')[0]
                                          .padLeft(8, '0')
                                          .split(":");
                                  return Text(
                                    minutes[1] + ":" + minutes[2],
                                    style: TextStyle(
                                        fontSize: 16,
                                        color: Colors.red.shade700),
                                  );
                                },
                                interval: Duration(seconds: 1),
                                onFinished: () {
                                  setState(() {
                                    phoneVerifiactionVisible = false;
                                  });
                                },
                              ),
                            ),
                    ),
                  ],
                ),
                Visibility(
                  visible: phoneVerifiactionVisible,
                  child: Column(
                    children: [
                      SizedBox(
                        height: 25,
                      ),
                      Pinput(
                        length: 4,
                        controller: controller,
                        focusNode: focusNode,
                        defaultPinTheme: defaultPinTheme,
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        showCursor: true,
                        focusedPinTheme: defaultPinTheme.copyWith(
                          width: 78,
                          decoration: BoxDecoration(color: Colors.white,borderRadius: BorderRadius.circular(12)),
                        ),
                        onCompleted: ((value) {
                          phoneVerificationCode = value;
                        }),
                      ),
                    ],
                  ),
                ),
                SizedBox(
                  height: 25,
                ),
                TurklinkTextField(
                  labelText: "register_screen.password_input".tr(),
                  obscureText: true,
                  controller: passwordController,
                  maxLines: 1,
                  errorText: passwordErrorMessage,
                ),
                SizedBox(
                  height: 25,
                ),
                TurklinkTextField(
                  labelText: "register_screen.repassword_input".tr(),
                  obscureText: true,
                  controller: rePasswordController,
                  maxLines: 1,
                  errorText: rePasswordErrorMessage,
                ),
                SizedBox(
                  height: 25,
                ),
                ListTile(
                  contentPadding: EdgeInsets.all(0),
                  subtitle: kvkkText
                      ? Text("register_screen.kvkk_text").tr()
                      : Text(
                          "register_screen.kvkk_text",
                          style: TextStyle(color: Colors.red.withOpacity(.7)),
                        ).tr(),
                  trailing: CupertinoSwitch(
                    value: kvkkText,
                    activeColor: TurklinkColors.mainColor,
                    onChanged: (value) {
                      if (value) Utility.showContractPopup(context, "KVKK");
                      setState(() {
                        kvkkText = value;
                      });
                    },
                  ),
                ),
                SizedBox(
                  height: 15,
                ),
                ListTile(
                  contentPadding: EdgeInsets.all(0),
                  subtitle: privacyText
                      ? Text("register_screen.privacy_text").tr()
                      : Text(
                          "register_screen.privacy_text",
                          style: TextStyle(color: Colors.red.withOpacity(.7)),
                        ).tr(),
                  trailing: CupertinoSwitch(
                    value: privacyText,
                    activeColor: TurklinkColors.mainColor,
                    
                    onChanged: (value) {
                      if (value) Utility.showContractPopup(context, "PRIVACY");
                      setState(() {
                        privacyText = value;
                      });
                    },
                  ),
                ),
                SizedBox(
                  height: 15,
                ),
                Container(
                  height: 52,
                  width: double.infinity,
                  child: TurklinkButton(
                    width: MediaQuery.of(context).size.width,
                    backgroundColor: TurklinkColors.mainColor,
                    buttonText: "entry_screen.register".tr(),
                    borderColor: TurklinkColors.mainColor,
                    textColor: Colors.white,
                    animatedOnTap: true,
                    btnController: btnController,
                    onPressed: () async {
                      

                      setState(() {
                        nameErrorMessage = null;
                        emailErrorMessage = null;
                        passwordErrorMessage = null;
                        phoneErrorMessage = null;
                        rePasswordErrorMessage = null;
                      });

                      UserModel? model = await userServices.signup(
                          fullNameController.text,
                          eMailController.text,
                          passwordController.text,
                          rePasswordController.text,
                          phoneChangeValue,
                          emailVerificationCode,
                          emailRequestId,
                          phoneVerificationCode,
                          phoneRequestId,
                          initialCountryCode!,kvkkText,privacyText);

                      btnController.reset();
                      if (model != null) {
                        Navigator.pushReplacementNamed(
                            context, TurklinkRouter.Main);
                      } else {
                        if (userServices.errorMessageList.contains(
                            "error_messages.register_name_empty")) {
                          setState(() {
                            nameErrorMessage = "error_messages.register_name_empty".tr();
                          });
                        }
                        if (userServices.errorMessageList.contains(
                                "error_messages.register_email_empty")) {
                          setState(() {
                            emailErrorMessage = "error_messages.register_email_empty".tr();
                          });
                        }
                        if (
                            userServices.errorMessageList.contains(
                                "error_messages.register_email_not_valid")) {
                          setState(() {
                            emailErrorMessage = "error_messages.register_email_not_valid".tr();
                          });
                        }
                        if (userServices.errorMessageList.contains(
                                "error_messages.register_email_already_use")) {
                          setState(() {
                            emailErrorMessage = "error_messages.register_email_already_use".tr();
                          });
                        }
                        if (userServices.errorMessageList.contains(
                                "error_messages.register_password_empty")) {
                          setState(() {
                            passwordErrorMessage =
                                "error_messages.register_password_empty".tr();
                          });
                        }
                        if (userServices.errorMessageList.contains(
                                "error_messages.register_weak_password")) {
                          setState(() {
                            passwordErrorMessage =
                                "error_messages.register_weak_password".tr();
                          });
                        }
                        if (userServices.errorMessageList.contains(
                            "error_messages.register_repassword_empty")) {
                          setState(() {
                            rePasswordErrorMessage =
                                "error_messages.register_repassword_empty".tr();
                          });
                        }
                        if (userServices.errorMessageList.contains(
                            "error_messages.register_password_repassword_not_valid")) {
                          setState(() {
                            passwordErrorMessage =
                                "error_messages.register_password_repassword_not_valid".tr();
                            rePasswordErrorMessage =
                                "error_messages.register_password_repassword_not_valid".tr();
                          });
                        }
                        if (userServices.errorMessageList.contains(
                                "error_messages.register_phoneNumber_empty")) {
                          setState(() {
                            phoneErrorMessage = "error_messages.register_phoneNumber_empty".tr();
                          });
                        }
                        if (userServices.errorMessageList.contains(
                                "error_messages.register_phoneVerification_empty")) {
                          setState(() {
                            phoneErrorMessage = "error_messages.register_phoneVerification_empty".tr();
                          });
                        }
                        if (userServices.errorMessageList.contains(
                                "error_messages.register_phoneVerification_not_valid")) {
                          setState(() {
                            phoneErrorMessage = "error_messages.register_phoneVerification_not_valid".tr();
                          });
                        }
                        if (userServices.errorMessageList.contains(
                                "error_messages.register_emailVerification_empty"))
                                {
                          setState(() {
                            emailErrorMessage = "error_messages.register_emailVerification_empty".tr();
                          });
                                }
                        if (userServices.errorMessageList.contains(
                                "error_messages.register_emailVerification_not_valid")) {
                          setState(() {
                            emailErrorMessage = "error_messages.register_emailVerification_not_valid".tr();
                          });
                        }
                      }
                    },
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
