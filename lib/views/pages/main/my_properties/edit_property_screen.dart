import 'package:another_flushbar/flushbar_helper.dart';
import 'package:flutter/material.dart';
import 'package:rounded_loading_button/rounded_loading_button.dart';
import 'package:turklink/models/properties_model.dart';
import 'package:turklink/services/properties_services.dart';
import 'package:turklink/util/turklink_colors.dart';
import 'package:turklink/views/pages/main/my_properties/widgets/property_address_widget.dart';
import 'package:turklink/views/pages/main/my_properties/widgets/property_detail_widget.dart';
import 'package:turklink/views/pages/main/my_properties/widgets/property_image_widget.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:turklink/views/widgets/turklink_button.dart';
import 'package:turklink/views/widgets/turklink_textfield.dart';

class EditPropertyScreen extends StatefulWidget {
  final PropertiesModel propertiesModel;
  EditPropertyScreen(this.propertiesModel);

  @override
  State<EditPropertyScreen> createState() => _EditPropertyScreenState();
}

var btnController = new RoundedLoadingButtonController();
var propertiesService = new PropertiesServices();
var propertyTitleController = TextEditingController();

var propertyAddressWidget = PropertyAddressWidget();
var propertyDetailWidget = PropertyDetailWidget();
var propertyImageWidget = PropertyImageWidget("add_property_screen.property_image_title".tr(),false);
var propertyDeedWidget = PropertyImageWidget("add_property_screen.property_deed_title".tr(),false);
String? titleErrorMessage;

class _EditPropertyScreenState extends State<EditPropertyScreen> {

  @override
  void initState() {
    propertyTitleController.text = widget.propertiesModel.title!;

    propertyImageWidget.imageFile = widget.propertiesModel.imageFile;
    propertyImageWidget.imagePath = widget.propertiesModel.imagePath;

    propertyDeedWidget.imageFile = widget.propertiesModel.deedFile;
    propertyDeedWidget.imagePath = widget.propertiesModel.deedPath;


    propertyAddressWidget.isEdit = true;
    propertyAddressWidget.cityValue = widget.propertiesModel.city;
    propertyAddressWidget.districtValue = widget.propertiesModel.district;
    propertyAddressWidget.placeValue = widget.propertiesModel.place;
    if(widget.propertiesModel.neighborhood != null)
      propertyAddressWidget.neighborhoodController.text = widget.propertiesModel.neighborhood!;
    if(widget.propertiesModel.street != null)
      propertyAddressWidget.streetController.text = widget.propertiesModel.street!;
    if(widget.propertiesModel.addressNumber != null)
      propertyAddressWidget.numberController.text = widget.propertiesModel.addressNumber!;
    if(widget.propertiesModel.doorNumber != null)
      propertyAddressWidget.doorNumberController.text = widget.propertiesModel.doorNumber!;

    propertyDetailWidget.furnishedValue = widget.propertiesModel.propertyFurnished;
    propertyDetailWidget.hirerValue = widget.propertiesModel.propertyHirer;
    propertyDetailWidget.isPersonelUseValue = widget.propertiesModel.propertyIsPersonelUse;
    if(widget.propertiesModel.propertySize != null)
       propertyDetailWidget.propertySizeController.text = widget.propertiesModel.propertySize!;
    if(widget.propertiesModel.propertyRentAmount != null)
       propertyDetailWidget.propertyRentAmountController.text = (widget.propertiesModel.propertyRentAmount!/100).round().toString();
    if(widget.propertiesModel.propertyRentFinishDate != null)
       propertyDetailWidget.propertyRentFinishDateController.text = widget.propertiesModel.propertyRentFinishDate!;
    propertyDetailWidget.propertyTypeValue = widget.propertiesModel.propertyType;
    propertyDetailWidget.roomValue = widget.propertiesModel.propertyRoom;

    propertyDetailWidget.propertyRentContractWidget = PropertyImageWidget("Kira Sözleşmesi",false);
    propertyDetailWidget.propertyRentContractWidget!.imageFile = widget.propertiesModel.rentContractFile;
    propertyDetailWidget.propertyRentContractWidget!.imagePath = widget.propertiesModel.rentContractPath;

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: TurklinkColors.grayBackground,
      appBar: AppBar(
        automaticallyImplyLeading: true,
        backgroundColor: TurklinkColors.mainColor,
        elevation: 0,
        centerTitle: true,
        title: Text(
          "add_property_screen.add_property_title".tr(),
          style: TextStyle(color: Colors.white, fontWeight: FontWeight.w400),
        ),
      ),
      body: SingleChildScrollView(
        child: Padding(
          padding: EdgeInsets.only(left: 20, right: 20),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              SizedBox(
                height: 20,
              ),
              Row(
                children: [
                  Expanded(
                    child: propertyImageWidget,
                  ),
                  SizedBox(
                    width: 20,
                  ),
                  Expanded(
                    child: propertyDeedWidget,
                  ),
                ],
              ),
              SizedBox(
                height: 20,
              ),
              TurklinkTextField(
                labelText: "add_property_screen.property_title".tr(),
                obscureText: false,
                controller: propertyTitleController,
                errorText: titleErrorMessage,
              ),
              SizedBox(
                height: 20,
              ),
              propertyAddressWidget,
              SizedBox(
                height: 20,
              ),
              propertyDetailWidget,
              SizedBox(
                height: 25,
              ),
              Container(
                height: 52,
                width: double.infinity,
                child: TurklinkButton(
                  width: MediaQuery.of(context).size.width,
                  backgroundColor: TurklinkColors.mainColor,
                  buttonText: "add_property_screen.save_button".tr(),
                  borderColor: TurklinkColors.mainColor,
                  textColor: Colors.white,
                  btnController: btnController,
                  onPressed: () async {
                    if(propertyTitleController.text.isEmpty)
                    {
                      titleErrorMessage = "add_property_screen.title_error_message".tr();
                      btnController.reset();
                      setState(() {});
                      return;
                    }
                      widget.propertiesModel.imageFile =  propertyImageWidget.imageFile;
                      widget.propertiesModel.deedFile = propertyDeedWidget.imageFile;
                      widget.propertiesModel.rentContractFile = propertyDetailWidget.propertyRentContractWidget != null ? propertyDetailWidget.propertyRentContractWidget!.imageFile : null;
                      widget.propertiesModel.title = propertyTitleController.text;
                      widget.propertiesModel.city = propertyAddressWidget.cityValue;
                      widget.propertiesModel.district = propertyAddressWidget.districtValue;
                      widget.propertiesModel.place = propertyAddressWidget.placeValue;
                      widget.propertiesModel.neighborhood = propertyAddressWidget.neighborhoodController.text;
                      widget.propertiesModel.street = propertyAddressWidget.streetController.text;
                      widget.propertiesModel.addressNumber = propertyAddressWidget.numberController.text;
                      widget.propertiesModel.propertyType = propertyDetailWidget.propertyTypeValue;
                      widget.propertiesModel.propertyRoom = propertyDetailWidget.roomValue;
                      widget.propertiesModel.propertySize = propertyDetailWidget.propertySizeController.text;
                      widget.propertiesModel.propertyHirer = propertyDetailWidget.hirerValue;
                      widget.propertiesModel.propertyIsPersonelUse = propertyDetailWidget.isPersonelUseValue;
                      widget.propertiesModel.propertyFurnished = propertyDetailWidget.furnishedValue;
                      widget.propertiesModel.propertyRentAmount = propertyDetailWidget.propertyRentAmountController.text != "" ? int.parse(propertyDetailWidget.propertyRentAmountController.text)*100 : 0;
                      widget.propertiesModel.propertyRentFinishDate = propertyDetailWidget.propertyRentFinishDateController.text;
                    var result = await propertiesService.updateProperties(widget.propertiesModel);
                    btnController.reset();
                    if(!result)
                    {
                      FlushbarHelper.createError(
                        title: "popup_message.popup_error_title".tr(),
                        message: propertiesService.errorMessage
                      ).show(context);
                    }
                    else
                    {
                       Navigator.of(context).pop();
                      FlushbarHelper.createSuccess(
                        title: "popup_message.popup_success_title".tr(),
                        message: "popup_message.popup_success_message".tr(),
                      ).show(context);
                    }


                  },
                ),
              ),
              SizedBox(
                height: 15,
              ),
              Container(
                height: 52,
                width: double.infinity,
                child: TurklinkButton(
                  width: MediaQuery.of(context).size.width,
                  backgroundColor: Colors.red,
                  buttonText: "add_property_screen.delete_button".tr(),
                  borderColor: Colors.red,
                  textColor: Colors.white,
                  btnController: btnController,
                  onPressed: () async {

                    var result = await propertiesService.deleteProperties(widget.propertiesModel);
                    btnController.reset();
                    if(!result)
                    {
                      FlushbarHelper.createError(
                        title: "popup_message.popup_error_title".tr(),
                        message: propertiesService.errorMessage
                      ).show(context);
                    }
                    else
                    {
                      Navigator.of(context).pop();
                      FlushbarHelper.createSuccess(
                        title: "popup_message.popup_success_title".tr(),
                        message: "popup_message.popup_success_message".tr(),
                      ).show(context);
                    }


                  },
                ),
              ),
              SizedBox(
                height: 15,
              ),
            ],
          ),
        ),
      ),
    );
  }

}