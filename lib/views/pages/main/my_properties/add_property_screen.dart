import 'package:another_flushbar/flushbar_helper.dart';
import 'package:flutter/material.dart';
import 'package:rounded_loading_button/rounded_loading_button.dart';
import 'package:turklink/models/properties_model.dart';
import 'package:turklink/services/properties_services.dart';
import 'package:turklink/util/turklink_colors.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:turklink/views/pages/main/my_properties/widgets/property_address_widget.dart';
import 'package:turklink/views/pages/main/my_properties/widgets/property_detail_widget.dart';
import 'package:turklink/views/pages/main/my_properties/widgets/property_image_widget.dart';
import 'package:turklink/views/widgets/turklink_button.dart';
import 'package:turklink/views/widgets/turklink_textfield.dart';

class AddPropertyScreen extends StatefulWidget {
  @override
  _AddPropertyScreenState createState() => _AddPropertyScreenState();
}

var btnController = new RoundedLoadingButtonController();
var propertiesService = new PropertiesServices();
var propertyTitleController = TextEditingController();

var propertyAddressWidget = PropertyAddressWidget();
var propertyDetailWidget = PropertyDetailWidget();
var propertyImageWidget = PropertyImageWidget("add_property_screen.property_image_title".tr(),false);
var propertyDeedWidget = PropertyImageWidget("add_property_screen.property_deed_title".tr(),false);
String? titleErrorMessage;

class _AddPropertyScreenState extends State<AddPropertyScreen> {
  @override
  void initState() {
    propertyTitleController.text = "";
    propertyImageWidget.imageFile = null;
    propertyDeedWidget.imageFile = null;
    propertyAddressWidget.isEdit = false;
    propertyAddressWidget.cityValue = null;
    propertyAddressWidget.districtValue = null;
    propertyAddressWidget.neighborhoodController.text = "";
    propertyAddressWidget.placeValue = null;
    propertyAddressWidget.streetController.text = "";
    propertyAddressWidget.numberController.text = "";
    propertyAddressWidget.doorNumberController.text = "";
    propertyDetailWidget.furnishedValue = null;
    propertyDetailWidget.hirerValue = null;
    propertyDetailWidget.isPersonelUseValue = null;
    propertyDetailWidget.propertySizeController.text = "";
    propertyDetailWidget.propertyRentAmountController.text = "";
    propertyDetailWidget.propertyRentFinishDateController.text = "";
    propertyDetailWidget.propertyRentFinishDateColor = Colors.black87;
    propertyDetailWidget.propertyRentAmountColor = Colors.black87;
    propertyDetailWidget.roomValue = null;
    propertyDetailWidget.propertyTypeValue = null;
    titleErrorMessage = null;
    propertyImageWidget = PropertyImageWidget("add_property_screen.property_image_title".tr(),false);
    propertyDeedWidget = PropertyImageWidget("add_property_screen.property_deed_title".tr(),false);
    propertyDetailWidget.propertyRentContractWidget = PropertyImageWidget("Kira Sözleşmesi",false);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: TurklinkColors.grayBackground,
      appBar: AppBar(
        automaticallyImplyLeading: true,
        backgroundColor: TurklinkColors.mainColor,
        elevation: 0,
        centerTitle: true,
        title: Text(
          "add_property_screen.add_property_title".tr(),
          style: TextStyle(color: Colors.white, fontWeight: FontWeight.w400),
        ),
      ),
      body: SingleChildScrollView(
        child: Padding(
          padding: EdgeInsets.only(left: 20, right: 20),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              SizedBox(
                height: 20,
              ),
              Row(
                children: [
                  Expanded(
                    child: propertyImageWidget,
                  ),
                  SizedBox(
                    width: 20,
                  ),
                  Expanded(
                    child: propertyDeedWidget,
                  ),
                ],
              ),
              SizedBox(
                height: 20,
              ),
              TurklinkTextField(
                labelText: "add_property_screen.property_title".tr(),
                obscureText: false,
                controller: propertyTitleController,
                errorText: titleErrorMessage,
              ),
              SizedBox(
                height: 20,
              ),
              propertyAddressWidget,
              SizedBox(
                height: 20,
              ),
              propertyDetailWidget,
              SizedBox(
                height: 25,
              ),
              Container(
                height: 52,
                width: double.infinity,
                child: TurklinkButton(
                  width: MediaQuery.of(context).size.width,
                  backgroundColor: TurklinkColors.mainColor,
                  buttonText: "add_property_screen.save_button".tr(),
                  borderColor: TurklinkColors.mainColor,
                  textColor: Colors.white,
                  btnController: btnController,
                  onPressed: () async {
                    bool isError = false;
                    if(propertyDeedWidget.imageFile == null)
                    {
                      propertyDeedWidget = PropertyImageWidget("add_property_screen.property_deed_title".tr(),true);
                      isError = true;
                    }

                    if(propertyTitleController.text.isEmpty)
                    {
                      titleErrorMessage = "add_property_screen.title_error_message".tr();
                      isError = true;
                    }
                   
                    if(isError)
                    {
                      btnController.reset();
                      setState(() {
                        
                      });
                      isError = false;
                      return;
                    }

                    var propertiesModel = new PropertiesModel(
                      imageFile: propertyImageWidget.imageFile,
                      deedFile: propertyDeedWidget.imageFile,
                      rentContractFile: propertyDetailWidget.propertyRentContractWidget != null ? propertyDetailWidget.propertyRentContractWidget!.imageFile : null,
                      title: propertyTitleController.text,
                      city: propertyAddressWidget.cityValue,
                      district: propertyAddressWidget.districtValue,
                      place: propertyAddressWidget.placeValue,
                      neighborhood: propertyAddressWidget.neighborhoodController.text,
                      street: propertyAddressWidget.streetController.text,
                      addressNumber: propertyAddressWidget.numberController.text,
                      doorNumber: propertyAddressWidget.doorNumberController.text,
                      propertyType: propertyDetailWidget.propertyTypeValue,
                      propertyRoom: propertyDetailWidget.roomValue,
                      propertySize: propertyDetailWidget.propertySizeController.text,
                      propertyHirer: propertyDetailWidget.hirerValue,
                      propertyIsPersonelUse: propertyDetailWidget.isPersonelUseValue,
                      propertyFurnished: propertyDetailWidget.furnishedValue,
                      propertyRentAmount: propertyDetailWidget.propertyRentAmountController.text != "" ? int.parse(propertyDetailWidget.propertyRentAmountController.text)*100 : 0,
                      propertyRentFinishDate: propertyDetailWidget.propertyRentFinishDateController.text
                    );
                    var result = await propertiesService.addProperties(propertiesModel);
                    btnController.reset();
                    if(!result)
                    {
                      FlushbarHelper.createError(
                        title: "popup_message.popup_error_title".tr(),
                        message: propertiesService.errorMessage
                      ).show(context);
                    }
                    else
                    {
                      Navigator.of(context).pop();
                      FlushbarHelper.createSuccess(
                        title: "popup_message.popup_success_title".tr(),
                        message: "popup_message.popup_success_message".tr(),
                      ).show(context);
                    }


                  },
                ),
              ),
              SizedBox(
                height: 15,
              ),
            ],
          ),
        ),
      ),
    );
  }
}
