import 'package:flutter/material.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter_datetime_picker/flutter_datetime_picker.dart';
import 'package:turklink/models/key_value_model.dart';
import 'package:turklink/services/utility_services.dart';
import 'package:turklink/views/pages/main/my_properties/widgets/property_image_widget.dart';
import 'package:turklink/views/widgets/turklink_dropdown_field.dart';
import 'package:turklink/views/widgets/turklink_textfield.dart';

class PropertyDetailWidget extends StatefulWidget {

  KeyValueModel? propertyTypeValue;
  KeyValueModel? roomValue;
  KeyValueModel? furnishedValue;
  KeyValueModel? hirerValue;
  KeyValueModel? isPersonelUseValue;
  TextEditingController propertySizeController = TextEditingController();
  TextEditingController propertyRentAmountController = TextEditingController();
  TextEditingController propertyRentFinishDateController = TextEditingController();
  Color propertyRentAmountColor = Colors.black87;
  Color propertyRentFinishDateColor = Colors.black87;
  PropertyImageWidget? propertyRentContractWidget;

  PropertyDetailWidget();

  @override
  State<PropertyDetailWidget> createState() =>
      _PropertyDetailWidgetState();
}

class _PropertyDetailWidgetState extends State<PropertyDetailWidget> {
  List<KeyValueModel>? emptyList = [KeyValueModel(key: "", value: "")];
  List<KeyValueModel>? propertyTypeList;
  List<KeyValueModel>? roomList;
  List<KeyValueModel>? yesNoList;

  UtilityServices utilityServices = new UtilityServices();


  @override
  void initState() {
    getPropertyTypes();
    getPropertyRooms();
    getYesNo();
    
    super.initState();
  }

  getPropertyTypes() {
    propertyTypeList = utilityServices.getPropertyType();
    setState(() {});
  }

  getPropertyRooms() {
    roomList = utilityServices.getPropertyRoom();
    setState(() {});
  }

  getYesNo() {
    yesNoList = utilityServices.getYesNo();
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text("add_property_screen.property_detail_title".tr()),
        SizedBox(
          height: 10,
        ),
        Container(
          child: TurklinkDropdownField(
            labelText: "add_property_screen.property_type".tr(),
            list: propertyTypeList != null ? propertyTypeList : emptyList,
            value: propertyTypeList != null ? widget.propertyTypeValue : null,
            onChanged: (item) {
              widget.propertyTypeValue = item;
              setState(() {});
            },
          ),
        ),
        SizedBox(
          height: 10,
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            if (widget.propertyTypeValue == null || widget.propertyTypeValue!.key! == "FLAT" ||
                widget.propertyTypeValue!.key! == "VILLA" ||
                widget.propertyTypeValue!.key! == "OFFICE")
              Expanded(
                child: TurklinkDropdownField(
                  labelText: "add_property_screen.property_room".tr(),
                  list: roomList != null ? roomList : emptyList,
                  value: roomList != null ? widget.roomValue : null,
                  onChanged: (item) {
                    widget.roomValue = item;
                    setState(() {});
                  },
                ),
              ),

              if (widget.propertyTypeValue == null || widget.propertyTypeValue!.key! == "FLAT" ||
                widget.propertyTypeValue!.key! == "VILLA" ||
                widget.propertyTypeValue!.key! == "OFFICE")
                SizedBox(width: 20,),

              Expanded(
                child: TurklinkTextField(
                labelText:
                    "add_property_screen.property_size".tr(),
                obscureText: false,
                textInputType: TextInputType.number,
                controller: widget.propertySizeController,
                ),
              ),

          ],
        ),
        if (widget.propertyTypeValue == null || widget.propertyTypeValue!.key! == "FLAT" ||
                widget.propertyTypeValue!.key! == "VILLA" ||
                widget.propertyTypeValue!.key! == "OFFICE")
                Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    SizedBox(height: 10,),
                    Container(
                      child: TurklinkDropdownField(
                        labelText: "add_property_screen.property_furnished".tr(),
                        list: yesNoList != null ? yesNoList : emptyList,
                        value: yesNoList != null ? widget.furnishedValue : null,
                        onChanged: (item) {
                          widget.furnishedValue = item;
                          setState(() {});
                        },
                      ),
                    ),
                  ],
                ),
                if (widget.propertyTypeValue == null || widget.propertyTypeValue!.key! == "FLAT" ||
                widget.propertyTypeValue!.key! == "VILLA" ||
                widget.propertyTypeValue!.key! == "OFFICE" || 
                widget.propertyTypeValue!.key! == "SHOP")
                Column(
                   mainAxisAlignment: MainAxisAlignment.start,
                   crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    SizedBox(height: 10,),
                    Container(
                      child: TurklinkDropdownField(
                        labelText: "add_property_screen.property_hirer".tr(),
                        list: yesNoList != null ? yesNoList : emptyList,
                        value: yesNoList != null ? widget.hirerValue : null,
                        onChanged: (item) {
                          widget.hirerValue = item;
                          setState(() {});
                        },
                      ),
                    ),
                    SizedBox(height: 10,),
                    Container(
                      child: TurklinkDropdownField(
                        labelText: "Gayrimenkulünüz şahsi kullanımda mı?",
                        list: yesNoList != null ? yesNoList : emptyList,
                        value: yesNoList != null ? widget.isPersonelUseValue : null,
                        onChanged: (item) {
                          widget.isPersonelUseValue = item;
                          setState(() {});
                        },
                      ),
                    ),
                  ],
                ),
                if ((widget.propertyTypeValue != null && (widget.propertyTypeValue!.key! == "FLAT" ||
                widget.propertyTypeValue!.key! == "VILLA" ||
                widget.propertyTypeValue!.key! == "OFFICE" || 
                widget.propertyTypeValue!.key! == "SHOP") &&  widget.hirerValue != null && widget.hirerValue!.key == "Y"))
                Column(
                   mainAxisAlignment: MainAxisAlignment.start,
                   crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    SizedBox(height: 10,),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Expanded(
                          child: TurklinkTextField(
                          labelText:
                              "Kira Tutarı",
                          obscureText: false,
                          textInputType: TextInputType.number,
                          borderColor: widget.propertyRentAmountColor,
                          controller: widget.propertyRentAmountController,
                          ),
                        ),
                        SizedBox(width: 20,),
                        Expanded(
                          child: SizedBox(
                            height: 52,
                            width: double.infinity,
                            child: Container(
                              width: double.infinity,
                              height: double.infinity,
                              child: TurklinkTextField(
                                labelText: "Sözleşme Bitiş Tarihi",
                                obscureText: false,
                                borderColor: widget.propertyRentFinishDateColor,
                                onTap: () {
                                  DatePicker.showDatePicker(context,
                                      showTitleActions: true,
                                      minTime:
                                          DateTime.now().add(new Duration(days: 1)),
                                      onConfirm: (date) {
                                    DateFormat formatter = DateFormat('yyyy-MM-dd');
                                    widget.propertyRentFinishDateController.text = formatter.format(date);
                                    setState(() {});
                                  },
                                      currentTime: DateTime.now(),
                                      locale: LocaleType.tr);
                                },
                                textInputType: TextInputType.number,
                                controller: widget.propertyRentFinishDateController,
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                    SizedBox(height: 10,),
                    widget.propertyRentContractWidget!,
                  ],
                )
      ],
    );
  }
}
