import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/container.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:turklink/models/properties_model.dart';

class MyPropertyRentAmountsPopupWidget extends StatefulWidget {
  final PropertiesModel propertiesModel;
  final BuildContext parentContext;
  MyPropertyRentAmountsPopupWidget({ required this.propertiesModel,required this.parentContext});

  @override
  State<MyPropertyRentAmountsPopupWidget> createState() => _MyPropertyRentAmountsPopupWidgetState();
}

class _MyPropertyRentAmountsPopupWidgetState extends State<MyPropertyRentAmountsPopupWidget> {
  @override
  Widget build(BuildContext context) {

     DateFormat formatter = DateFormat('MMM yyyy');

    return AlertDialog(
          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(12)),
          titlePadding: EdgeInsets.symmetric(vertical: 20,horizontal: 20),
          contentPadding: EdgeInsets.symmetric(vertical: 10,horizontal: 20),
          title: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text("Ödemeler",style: TextStyle(fontSize: 15),),
              InkWell(onTap: (){
                Navigator.of(widget.parentContext).pop();
              }, child: Icon(Icons.cancel,color: Colors.red,size: 28,),)
            ],
          ),
          content: Container(
            height: MediaQuery.of(context).size.height/2,
            width: MediaQuery.of(context).size.width,
            child: StreamBuilder(
                stream: FirebaseFirestore.instance
                    .collection("properties")
                    .doc(widget.propertiesModel.id).collection("rentAmounts")
                    .orderBy("rentAmountDate", descending: false)
                    .snapshots(),
                builder: (BuildContext context,
                    AsyncSnapshot<QuerySnapshot> snapshot) {
                  if (snapshot.connectionState == ConnectionState.active) {
                    if (snapshot.hasData && snapshot.data!.docs.length > 0) {
                      return ListView.builder(
                            itemCount: snapshot.data!.docs.length,
                            itemBuilder: (context, index) {
                              Map<String, dynamic> data =
                                  snapshot.data!.docs[index].data()!
                                      as Map<String, dynamic>;
                              return ListTile(
                                title: Text(formatter
                                    .format(data["rentAmountDate"].toDate())),
                                trailing: Text((widget.propertiesModel.leaseAmount! / 100).toString() + " TL"),
                              );
                            });
                    } else {
                      return Container();
                    }
                  } else {
                    return Center(
                      child: CircularProgressIndicator(),
                    );
                  }
                }),
          ),
        );
  }
}