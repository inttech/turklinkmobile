import 'package:flutter/material.dart';
import 'package:turklink/models/properties_model.dart';
import 'package:easy_localization/easy_localization.dart';

class MyPropertyDetailsAddressWidget extends StatelessWidget {
  final PropertiesModel propertiesModel;
  MyPropertyDetailsAddressWidget(this.propertiesModel);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.only(top: 10, bottom: 10),
      child: Row(
        children: [
          Expanded(
            child: Wrap(
              children: [
                Text(
                  (propertiesModel.neighborhood != null
                          ? propertiesModel.neighborhood!
                          : "") +
                      (propertiesModel.street != null
                          ? " " + propertiesModel.street!
                          : "") +
                      (propertiesModel.addressNumber != null
                          ? " " + propertiesModel.addressNumber!
                          : "") +
                      (propertiesModel.district != null
                          ? " " + propertiesModel.district!.value!
                          : "") +
                      (propertiesModel.place != null
                          ? " " + propertiesModel.place!.value!
                          : "") +
                      (propertiesModel.city != null
                          ? " " + propertiesModel.city!.value!
                          : ""),
                  style: TextStyle(
                      fontSize: 11,
                      fontWeight: FontWeight.bold,
                      color: Colors.grey[600]),
                )
              ],
            ),
          ),
          Expanded(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.end,
              children: [
                Text(
                  "my_properties_screen.uavt_code".tr() +
                      " " +
                      (propertiesModel.uavtCode != null
                          ? propertiesModel.uavtCode!
                          : ""),
                  style: TextStyle(
                      fontSize: 11,
                      fontWeight: FontWeight.bold,
                      color: Colors.grey[600]),
                ),
                Text(
                  (propertiesModel.propertyRoom != null
                          ? propertiesModel.propertyRoom!.value!
                          : "") +
                      " / " +
                      (propertiesModel.propertySize != null
                          ? propertiesModel.propertySize! + "m²"
                          : ""),
                  style: TextStyle(
                      fontSize: 11,
                      fontWeight: FontWeight.bold,
                      color: Colors.grey[600]),
                )
              ],
            ),
          ),
        ],
      ),
    );
  }
}
