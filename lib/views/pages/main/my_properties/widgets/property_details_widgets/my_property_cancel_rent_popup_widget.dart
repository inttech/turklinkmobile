import 'package:another_flushbar/flushbar_helper.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:rounded_loading_button/rounded_loading_button.dart';
import 'package:turklink/models/properties_model.dart';
import 'package:turklink/services/properties_services.dart';
import 'package:turklink/util/property_process_status.dart';
import 'package:turklink/util/turklink_colors.dart';
import 'package:turklink/views/widgets/turklink_button.dart';

class MyPropertyCancelRentPopupWidget extends StatefulWidget {
  final PropertiesModel propertiesModel;
  final BuildContext parentContext;
  final Function callBack;
  MyPropertyCancelRentPopupWidget({ required this.propertiesModel,required this.parentContext,required this.callBack});

  @override
  State<MyPropertyCancelRentPopupWidget> createState() => _MyPropertyCancelRentPopupWidgetState();
}

class _MyPropertyCancelRentPopupWidgetState extends State<MyPropertyCancelRentPopupWidget> {

  var sendController = new RoundedLoadingButtonController();
  PropertiesServices propertiesServices = new PropertiesServices();

  @override
  Widget build(BuildContext context) {

    return AlertDialog(
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(12)),
      titlePadding: EdgeInsets.symmetric(vertical: 20, horizontal: 20),
      contentPadding: EdgeInsets.symmetric(vertical: 10, horizontal: 20),
      title: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Text(
            "my_properties_screen.cancel_rent_property_title".tr(),
            style: TextStyle(fontSize: 15),
          ),
          InkWell(
            onTap: () {
              Navigator.of(widget.parentContext).pop();
            },
            child: Icon(
              Icons.cancel,
              color: Colors.red,
              size: 28,
            ),
          )
        ],
      ),
      content: Wrap(
        children: [
          Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Center(child: Text("my_properties_screen.cancel_rent_property_text".tr(),textAlign: TextAlign.center,)),
            ],
          )
        ],
      ),
      actionsAlignment: MainAxisAlignment.start,
      actionsPadding: EdgeInsets.only(left: 10, right: 10, bottom: 10),
      actions: [
        SizedBox(
          height: 52,
          width: MediaQuery.of(context).size.width,
          child: TurklinkButton(
            width: MediaQuery.of(context).size.width,
            backgroundColor: TurklinkColors.mainColor,
            buttonText: "request_screen.approve_button".tr(),
            borderColor: TurklinkColors.mainColor2,
            textColor: Colors.white,
            btnController: sendController,
            animatedOnTap: true,
            onPressed: () async {



            PropertiesModel propertiesModelReq = widget.propertiesModel;
            propertiesModelReq.leaseStatus = null;
            propertiesModelReq.leaseProcessStatus = PropertyProcessStatus.pendingAproveFromUser;

            var result = await propertiesServices.updateProperties(propertiesModelReq);
            sendController.reset();
            if (!result) {
              FlushbarHelper.createError(
                      title: "popup_message.popup_error_title".tr(),
                      message: propertiesServices.errorMessage)
                  .show(widget.parentContext);
            } else {
              Navigator.of(widget.parentContext).pop();
              await FlushbarHelper.createSuccess(
                title: "popup_message.popup_success_title".tr(),
                message: "popup_message.popup_success_message".tr(),
              ).show(widget.parentContext);
              widget.callBack();
            }
            },
          ),
        ),
      ],
    );

  }
}