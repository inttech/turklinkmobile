import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:turklink/models/finances_model.dart';
import 'package:turklink/models/properties_model.dart';
import 'package:turklink/util/turklink_colors.dart';
import 'package:turklink/util/utility.dart';
import 'package:turklink/views/pages/main/finances/widgets/finances_detail_card_item_widget.dart';
import 'package:turklink/views/widgets/turklink_empty_screen.dart';

class MyPropertyDetailsFinancesWidget extends StatefulWidget {
  final PropertiesModel propertiesModel;
  MyPropertyDetailsFinancesWidget(this.propertiesModel);

  @override
  State<MyPropertyDetailsFinancesWidget> createState() =>
      _MyPropertyDetailsFinancesWidgetState();
}

class _MyPropertyDetailsFinancesWidgetState
    extends State<MyPropertyDetailsFinancesWidget> {

  @override
  Widget build(BuildContext context) {
    return Container(
      width: MediaQuery.of(context).size.width,
      padding: EdgeInsets.symmetric(vertical: 5, horizontal: 15),
      margin: EdgeInsets.only(top: 10),
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(15),
      ),
      child: Theme(
        data: Theme.of(context).copyWith(dividerColor: Colors.transparent,primaryColor: TurklinkColors.mainColor),
        child: ExpansionTile(
          trailing: Icon(Icons.arrow_drop_down,color: Colors.grey,),
          tilePadding: EdgeInsets.zero,
          expandedCrossAxisAlignment: CrossAxisAlignment.end,
          onExpansionChanged: (change){

          },
          childrenPadding: EdgeInsets.zero,
          iconColor: Colors.grey,
          collapsedIconColor: Colors.grey,
          title: Text(("my_properties_screen.property_finances").tr(),style: TextStyle(color: TurklinkColors.mainColor,fontSize: 12,
                        fontWeight: FontWeight.bold),),
          expandedAlignment: Alignment.topLeft,
          children: [
            Container(
              width: double.infinity,
              height: 250,
              child: StreamBuilder(
                  stream: FirebaseFirestore.instance
                      .collection("finances")
                      .where('userId',
                          isEqualTo: FirebaseAuth.instance.currentUser!.uid)
                      .where('propertyId',
                          isEqualTo: widget.propertiesModel.id!)
                          .orderBy("operationDate", descending: true)
                      .snapshots(),
                  builder: (BuildContext  context, AsyncSnapshot<QuerySnapshot> snapshot) {
                    if (snapshot.connectionState == ConnectionState.active) {
                      if (snapshot.hasData && snapshot.data!.docs.length > 0) {
                        return ListView.builder(
                            itemCount: snapshot.data!.docs.length,
                            itemBuilder: (context, index) {
                               Map<String, dynamic> data = snapshot.data!.docs[index].data()! as Map<String, dynamic>;
                              var financesModel = FinancesModel.fromJson(data);

                              DateFormat formatter = DateFormat('dd MMM yyyy');
                              return FinancesDetailCardItemWidget(
                                date: formatter.format(financesModel.operationDate!),
                                title: ("finances_screen." + financesModel.financeDescription!).tr(),
                                amount: (financesModel.direction == "IN" ? "+" : "-") + " " + Utility.convertCurrency(financesModel.amount!) + " TL" ,
                                amountColor: financesModel.direction == "IN" ? TurklinkColors.mainColor2 : Colors.orange,
                              );
                            });
                      } else {
                        return TurklinkEmptyScreen(
                          message: "finances_screen.has_no_property".tr(),
                        );
                      }
                    } else {
                      return Center(
                        child: CircularProgressIndicator(),
                      );
                    }
                  }),
            ),
          ],
        ),
      ),
    );
  }
}
