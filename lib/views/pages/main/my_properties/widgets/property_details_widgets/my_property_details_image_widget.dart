import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';

class MyPropertiesDetailsImageWidget extends StatelessWidget {
  final String imagePath;
  MyPropertiesDetailsImageWidget(this.imagePath);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      height: MediaQuery.of(context).size.width / 2,
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(15), color: Colors.white),
      child: ClipRRect(
        borderRadius: BorderRadius.circular(15),
        child: CachedNetworkImage(
          imageUrl: this.imagePath,
          fit: BoxFit.cover,
          placeholder: (context, url) => Center(
            child: Container(
              width: 20,
              height: 20,
              child: CircularProgressIndicator(
                strokeWidth: 1,
                value: 1,
              ),
            ),
          ),
          errorWidget: (context, url, error) => Icon(Icons.error),
        ),
      ),
    );
  }
}
