import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:turklink/models/properties_model.dart';
import 'package:turklink/util/turklink_colors.dart';
import 'package:turklink/views/pages/main/my_properties/request_screen.dart';

class MyPropertyDetailsRequestWidget extends StatefulWidget {
  final PropertiesModel propertiesModel;
  MyPropertyDetailsRequestWidget(this.propertiesModel);

  @override
  State<MyPropertyDetailsRequestWidget> createState() => _MyPropertyDetailsRequestWidgetState();
}

class _MyPropertyDetailsRequestWidgetState extends State<MyPropertyDetailsRequestWidget> {
  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        GestureDetector(
          onTap: ()
          {
            Navigator.push(
              context,
              MaterialPageRoute(
                builder: (context) => RequestScreen(widget.propertiesModel),
              ),
            );
          },
          child: Container(
            width: MediaQuery.of(context).size.width,
            padding: EdgeInsets.symmetric(vertical: 25, horizontal: 15),
            margin: EdgeInsets.only(top: 10),
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.circular(15),
            ),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Text(
                    ("my_properties_screen.property_request").tr(),
                    style: TextStyle(
                        color: TurklinkColors.mainColor,
                        fontSize: 12,
                        fontWeight: FontWeight.bold),
                  ),
                  Icon(Icons.arrow_drop_down,color: Colors.grey,)
              ],
            ),
          ),
        ),
      ],
    );
  }
}