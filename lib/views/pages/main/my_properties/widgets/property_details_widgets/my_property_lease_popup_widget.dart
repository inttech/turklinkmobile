import 'package:another_flushbar/flushbar_helper.dart';
import 'package:flutter/material.dart';
import 'package:rounded_loading_button/rounded_loading_button.dart';
import 'package:turklink/models/key_value_model.dart';
import 'package:turklink/models/properties_model.dart';
import 'package:turklink/services/properties_services.dart';
import 'package:turklink/services/utility_services.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:turklink/util/lease_status.dart';
import 'package:turklink/util/property_process_status.dart';
import 'package:turklink/util/turklink_colors.dart';
import 'package:turklink/util/utility.dart';
import 'package:turklink/views/widgets/turklink_button.dart';
import 'package:turklink/views/widgets/turklink_dropdown_field.dart';

class MyPropertyLeasePopupWidget extends StatefulWidget {
  final PropertiesModel propertiesModel;
  final BuildContext parentContext;
  MyPropertyLeasePopupWidget({ required this.propertiesModel,required this.parentContext});

  @override
  State<MyPropertyLeasePopupWidget> createState() => _MyPropertyLeasePopupWidgetState();
}

class _MyPropertyLeasePopupWidgetState extends State<MyPropertyLeasePopupWidget> {

  UtilityServices utilityServices = new UtilityServices();
  PropertiesServices propertiesServices = new PropertiesServices();
  var leaseRequestController = new RoundedLoadingButtonController();
  KeyValueModel? selectedLeaseTerm;
  KeyValueModel? selectedLeaseAmount;

  Color leaseTermColor = Colors.black87;
  Color leaseAmountColor = Colors.black87;
  DateTime now = DateTime.now();
  List<KeyValueModel>? listLeaseAmount;
  KeyValueModel? selectedAmount;
  @override
  void initState() {
    listLeaseAmount = utilityServices.getLeaseAmount(widget.propertiesModel.estimatedRentIncome!);
    String key = (widget.propertiesModel.estimatedRentIncome!/100).toInt().toString();
    selectedAmount = listLeaseAmount!.where((element) => element.key == key).first;
    selectedLeaseAmount = selectedAmount;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {


    return AlertDialog(
          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(12)),
          titlePadding: EdgeInsets.symmetric(vertical: 20,horizontal: 20),
          contentPadding: EdgeInsets.symmetric(vertical: 10,horizontal: 20),
          title: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text("my_properties_screen.rental_request".tr(),style: TextStyle(fontSize: 15),),
              InkWell(onTap: (){
                Navigator.of(widget.parentContext).pop();
              }, child: Icon(Icons.cancel,color: Colors.red,size: 28,),)
            ],
          ),
          content: Wrap(
            children: [Column(
              children: [
                 SizedBox(height: 10,),
                 SizedBox(
                   height: 52,
                   width: double.infinity,
                   child: TurklinkDropdownField(
                     labelText: "my_properties_screen.lease_amount".tr(),
                     list: listLeaseAmount,
                     borderColor: leaseAmountColor,
                     diffrentColorKey: Utility.convertCurrency(widget.propertiesModel.estimatedRentIncome!),
                     value: selectedAmount,
                     onChanged: (item) {
                       selectedLeaseAmount = item;
                     },
                   ),
                 ),
                 SizedBox(height: 10,),
                 Wrap(
                   children: [
                     Text("my_properties_screen.rental_request_message".tr(args: [(widget.propertiesModel.managementFee!/100).toString()]),style: TextStyle(fontSize: 10),)
                   ],
                 )
              ],
            )],
          ),
          actionsAlignment: MainAxisAlignment.start,
          actionsPadding: EdgeInsets.only(left:10,right: 10,bottom: 10),
          actions: <Widget>[
            SizedBox(
              height: 52,
              width: double.infinity,
              child: TurklinkButton(
                width: MediaQuery.of(context).size.width,
                backgroundColor: TurklinkColors.mainColor,
                buttonText: "my_properties_screen.rental_request_button".tr(),
                borderColor: TurklinkColors.mainColor,
                textColor: Colors.white,
                btnController: leaseRequestController,
                animatedOnTap: true,
                onPressed: () async {
                    if(selectedLeaseAmount == null)
                    {
                       leaseRequestController.reset();
                       setState(() {
                         leaseAmountColor = Colors.red;
                       });
                       return;
                    }
                    widget.propertiesModel.leaseProcessStatus = PropertyProcessStatus.pendingProcessFromAdmin;
                    widget.propertiesModel.leaseStatus = LeaseStatus.pendingListing;
                    widget.propertiesModel.leaseAmount = int.parse(selectedLeaseAmount!.key!)*100;
                    widget.propertiesModel.leaseTerm = 12;
                    var result = await propertiesServices.updateProperties(widget.propertiesModel);
                    leaseRequestController.reset();
                    if(!result)
                    {
                      FlushbarHelper.createError(
                        title: "popup_message.popup_error_title".tr(),
                        message: propertiesServices.errorMessage
                      ).show(widget.parentContext);
                    }
                    else
                    {
                      Navigator.of(widget.parentContext).pop();
                      Navigator.of(widget.parentContext).pop();
                      await FlushbarHelper.createSuccess(
                        title: "popup_message.popup_success_title".tr(),
                        message: "popup_message.popup_success_message".tr(),
                      ).show(widget.parentContext);
                    }
                },
              ),
            )
          ],
        );
  }
}