import 'dart:async';
import 'dart:io';
import 'dart:isolate';
import 'dart:ui';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter_downloader/flutter_downloader.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:path_provider/path_provider.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:turklink/models/properties_model.dart';
import 'package:turklink/models/user_model.dart';
import 'package:turklink/services/properties_services.dart';
import 'package:turklink/util/finance_type.dart';
import 'package:turklink/util/property_process_status.dart';
import 'package:turklink/util/transaction_status.dart';
import 'package:turklink/util/turklink_colors.dart';
import 'package:turklink/util/utility.dart';
import 'package:turklink/views/pages/main/my_properties/widgets/add_property_widgets/add_property_ribbon_widget.dart';

class MyPropertyDetailsEarthquakeInsuranceWidget extends StatefulWidget {
  final PropertiesModel propertiesModel;
  MyPropertyDetailsEarthquakeInsuranceWidget(this.propertiesModel);

  @override
  State<MyPropertyDetailsEarthquakeInsuranceWidget> createState() => _MyPropertyDetailsEarthquakeInsuranceWidgetState();
}

class _MyPropertyDetailsEarthquakeInsuranceWidgetState extends State<MyPropertyDetailsEarthquakeInsuranceWidget> {

  ReceivePort _port = ReceivePort();

  @override
  void initState() {
    super.initState();

    IsolateNameServer.registerPortWithName(
        _port.sendPort, 'downloader_send_port');
    _port.listen((dynamic data) async{
      String id = data[0];
      DownloadTaskStatus status = data[1];
      int progress = data[2];
      if (status == DownloadTaskStatus.complete && Platform.isIOS) {
         await Future.delayed(const Duration(seconds: 1));
        unawaited(FlutterDownloader.open(taskId: id));
      }
      setState(() {});
    });

    FlutterDownloader.registerCallback(downloadCallback);
  }

  @override
  void dispose() {
    IsolateNameServer.removePortNameMapping('downloader_send_port');
    super.dispose();
  }

  static void downloadCallback(
      String id, DownloadTaskStatus status, int progress) {
    final SendPort send =
        IsolateNameServer.lookupPortByName('downloader_send_port')!;
    send.send([id, status, progress]);
  }

  @override
  Widget build(BuildContext context) {

    Locale local = context.locale;

    return Stack(
      children: [
        Container(
          width: MediaQuery.of(context).size.width,
          padding: EdgeInsets.symmetric(vertical: 25, horizontal: 15),
          margin: EdgeInsets.only(top: 10),
          decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.circular(15),
          ),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              if (widget.propertiesModel.earthquakeProcessStatus == PropertyProcessStatus.pendingInfoFromAdmin || widget.propertiesModel.earthquakeProcessStatus == PropertyProcessStatus.complate)
                Text(
                  ("my_properties_screen.earthquarke_title").tr(),
                  style: TextStyle(
                      color: TurklinkColors.mainColor,
                      fontSize: 12,
                      fontWeight: FontWeight.bold),
                )
              else
                Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      ("my_properties_screen.earthquarke_title").tr(),
                      style: TextStyle(
                          color: TurklinkColors.mainColor,
                          fontSize: 12,
                          fontWeight: FontWeight.bold),
                    ),
                    if (widget.propertiesModel.earthquakeInsuranceAmount != null)
                      Text(Utility.convertCurrency(widget.propertiesModel.earthquakeInsuranceAmount!) +  " TL",
                        style: TextStyle(fontSize: 10, color: Colors.black54),
                    ),
                  ],
                ),
              if (widget.propertiesModel.earthquakeInsurancePath != null && widget.propertiesModel.earthquakeProcessStatus == PropertyProcessStatus.complate)
                Row(
                        children: [
                          Icon(
                            Icons.file_download_outlined,
                            color: Colors.grey,
                          ),
                          SizedBox(
                            width: 5,
                          ),
                          InkWell(
                            onTap: () async {
                              download(widget.propertiesModel.earthquakeInsurancePath!);
                            },
                            child: Text(
                              "my_properties_screen.download_contract".tr(),
                              style: TextStyle(
                                  color: Colors.grey[500],
                                  fontSize: 12,
                                  fontWeight: FontWeight.bold),
                            ),
                          )
                        ],
                      ),
              if (widget.propertiesModel.earthquakeProcessStatus == PropertyProcessStatus.pendingAproveFromUser)
                MaterialButton(
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(12)),
                  onPressed: () async {
                    var userModel = await FirebaseFirestore.instance.collection("users").doc(FirebaseAuth.instance.currentUser!.uid).get();
                    var userData  = UserModel.fromJson(userModel.data()!);
                    await Utility.showPaymentMethodPopup(context,userData.tryBalance,FinanceType.earthquakeInsurance,widget.propertiesModel.earthquakeInsuranceAmount!,propertyId: widget.propertiesModel.id,);
                    PropertiesServices propertiesServices = new PropertiesServices();
                    var property = await propertiesServices.getProperty(widget.propertiesModel.id!);
                    widget.propertiesModel.earthquakeProcessStatus = property!.earthquakeProcessStatus;
                    setState(() {

                    });
                  },
                  color: TurklinkColors.mainColor2,
                  child: Text(
                    "my_properties_screen.pay_property".tr(),
                    style: TextStyle(color: Colors.white),
                  ),
                ),
            ],
          ),
        ),
        if (widget.propertiesModel.earthquakeProcessStatus == PropertyProcessStatus.pendingAproveFromUser)
          Positioned(
            right: local.languageCode != "ar" ? 10 : null,
            left: local.languageCode == "ar" ? 10 : null,
            top: 8,
            child: AddPropertyRibbonWidget(
              color: Colors.orange,
              notification: Icon(
                FontAwesomeIcons.exclamation,
                color: Colors.white,
                size: 14,
              ),
            ),
          ),
        if (widget.propertiesModel.earthquakeProcessStatus == PropertyProcessStatus.complate)
          Positioned(
            right: local.languageCode != "ar" ? 10 : null,
            left: local.languageCode == "ar" ? 10 : null,
            top: 8,
            child: AddPropertyRibbonWidget(
              color: TurklinkColors.mainColor2,
              notification: Icon(
                Icons.check,
                color: Colors.white,
                size: 14,
              ),
            ),
          ),
          if (widget.propertiesModel.earthquakeProcessStatus == PropertyProcessStatus.pendingInfoFromAdmin || widget.propertiesModel.earthquakeProcessStatus == PropertyProcessStatus.pendingProcessFromAdmin)
          Positioned(
            right: local.languageCode != "ar" ? 10 : null,
            left: local.languageCode == "ar" ? 10 : null,
            top: 15,
            child: Text(
              ("my_properties_screen.pendingAdmin_" +  widget.propertiesModel.earthquakeProcessStatus.toString()).tr(),
              style: TextStyle(color: Colors.orange),
            ),
          ),
      ],
    );
  }

  void download(String url) async {
    final status = await Permission.storage.request();

    if (status.isGranted) {
      final externalDir = await getDownloadPath();

      final id = await FlutterDownloader.enqueue(
          url: url,
          savedDir: externalDir!,
          showNotification: true,
          openFileFromNotification: true,
          saveInPublicStorage: true);
    }
  }


  Future<String?> getDownloadPath() async {
    Directory? directory;
    try {
      directory = await getApplicationDocumentsDirectory();
    } catch (err, stack) {
      print("Cannot get download folder path");
    }
    return directory?.path;
  }

}