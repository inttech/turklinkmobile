import 'dart:async';
import 'dart:io';
import 'dart:isolate';
import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:path_provider/path_provider.dart';
import 'package:rounded_loading_button/rounded_loading_button.dart';
import 'package:turklink/models/properties_model.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:turklink/util/lease_status.dart';
import 'package:turklink/util/property_process_status.dart';
import 'package:turklink/util/turklink_colors.dart';
import 'package:turklink/util/utility.dart';
import 'package:flutter_downloader/flutter_downloader.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:turklink/views/pages/main/my_properties/widgets/add_property_widgets/add_property_ribbon_widget.dart';
import 'package:turklink/views/widgets/turklink_button.dart';
import 'package:url_launcher/url_launcher.dart';

class MyPropertyRentalDetailsWidget extends StatefulWidget {
  final PropertiesModel propertiesModel;
  MyPropertyRentalDetailsWidget(this.propertiesModel);

  @override
  State<MyPropertyRentalDetailsWidget> createState() =>
      _MyPropertyRentalDetailsWidgetState();
}

class _MyPropertyRentalDetailsWidgetState
    extends State<MyPropertyRentalDetailsWidget> {
  ReceivePort _port = ReceivePort();
  var rentBtnController = new RoundedLoadingButtonController();

  @override
  void initState() {
    super.initState();

    IsolateNameServer.registerPortWithName(
        _port.sendPort, 'downloader_send_port');
    _port.listen((dynamic data) async {
      String id = data[0];
      DownloadTaskStatus status = data[1];
      int progress = data[2];
      if (status == DownloadTaskStatus.complete && Platform.isIOS) {
        await Future.delayed(const Duration(seconds: 1));
        unawaited(FlutterDownloader.open(taskId: id));
      }
      setState(() {});
    });

    FlutterDownloader.registerCallback(downloadCallback);
  }

  @override
  void dispose() {
    IsolateNameServer.removePortNameMapping('downloader_send_port');
    super.dispose();
  }

  static void downloadCallback(
      String id, DownloadTaskStatus status, int progress) {
    final SendPort send =
        IsolateNameServer.lookupPortByName('downloader_send_port')!;
    send.send([id, status, progress]);
  }

  @override
  Widget build(BuildContext context) {
    DateFormat formatter = DateFormat('MMM yyyy');
    Locale local = context.locale;

    return Stack(
      children: [
        Container(
          width: MediaQuery.of(context).size.width,
          padding: EdgeInsets.symmetric(vertical: 20, horizontal: 15),
          margin: EdgeInsets.only(top: 10),
          decoration: BoxDecoration(
              color: Colors.white, borderRadius: BorderRadius.circular(15)),
          child: (widget.propertiesModel.propertyHirer != null &&
                  widget.propertiesModel.propertyHirer!.key! == "Y")
              ? Text(
                  "my_properties_screen.rented_out_by_property_owner".tr(),
                  style: TextStyle(
                      color: TurklinkColors.mainColor,
                      fontSize: 12,
                      fontWeight: FontWeight.bold),
                )
              : Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    if ((widget.propertiesModel.contractPath != null &&
                            widget.propertiesModel.contractPath! != "") ||
                        widget.propertiesModel.leaseStatus != null &&
                            widget.propertiesModel.leaseStatus ==
                                LeaseStatus.leased)
                      Expanded(
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text("Kiralandı",
                              style: TextStyle(
                                  color: TurklinkColors.mainColor,
                                  fontSize: 12,
                                  fontWeight: FontWeight.bold),
                            ),
                            Text(
                              formatter
                                    .format(widget.propertiesModel.leaseDate!) + "  " + (widget.propertiesModel.leaseAmount! / 100).toString() + " TL",
                              style: TextStyle(
                                  color: Colors.grey,
                                  fontSize: 10,
                                  fontWeight: FontWeight.bold),
                            ),
                          ],
                        ),
                      ),
                    if (widget.propertiesModel.contractPath != null &&
                        widget.propertiesModel.contractPath! != "")
                      Expanded(
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.end,
                          crossAxisAlignment: CrossAxisAlignment.end,
                          children: [
                            MaterialButton(
                              shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(12)),
                              onPressed: () async{
                                Utility.showRentPropertyAmountsPopup(
                                  context, widget.propertiesModel);
                              },
                              color: Colors.blue,
                              child: Text(
                                "Ödemeler",
                                style: TextStyle(color: Colors.white,fontSize: 13),
                              ),),
                              MaterialButton(
                              shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(12)),
                              onPressed: () async{
                                if (await canLaunch(widget.propertiesModel.contractPath!))
                                    await launch(widget.propertiesModel.contractPath!);
                              },
                              color: Colors.grey,
                              child: Text(
                                "Sözleşme Görüntüle",
                                style: TextStyle(color: Colors.white,fontSize: 13),
                              ),),
                          ],
                        ),
                      ),
                    if (widget.propertiesModel.leaseProcessStatus ==
                            PropertyProcessStatus.pendingInfoFromAdmin ||
                        widget.propertiesModel.leaseProcessStatus ==
                            PropertyProcessStatus.pendingAproveFromUser)
                      Text("my_properties_screen.property_empty".tr(),
                          style: TextStyle(
                              color: TurklinkColors.mainColor,
                              fontSize: 12,
                              fontWeight: FontWeight.bold)),
                    if (widget.propertiesModel.leaseProcessStatus ==
                            PropertyProcessStatus.pendingProcessFromAdmin ||
                        (widget.propertiesModel.leaseProcessStatus ==
                                PropertyProcessStatus.complate &&
                            widget.propertiesModel.leaseStatus !=
                                LeaseStatus.leased))
                      Text("my_properties_screen.long_term_rental_text".tr(),
                          style: TextStyle(
                              color: TurklinkColors.mainColor,
                              fontSize: 12,
                              fontWeight: FontWeight.bold)),
                    if (widget.propertiesModel.leaseProcessStatus ==
                        PropertyProcessStatus.pendingAproveFromUser)
                      MaterialButton(
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(12)),
                        onPressed: () {
                          Utility.showLeasePopup(
                              context, widget.propertiesModel);
                        },
                        color: TurklinkColors.mainColor2,
                        child: Text(
                          "my_properties_screen.rent_my_property".tr(),
                          style: TextStyle(color: Colors.white),
                        ),
                      ),
                    Column(
                      children: [
                        if (widget.propertiesModel.leaseStatus != null &&
                            widget.propertiesModel.leaseStatus ==
                                LeaseStatus.onListing &&
                            widget.propertiesModel.leaseProcessStatus ==
                                PropertyProcessStatus.complate)
                          MaterialButton(
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(12)),
                            onPressed: () async{
                              if (await canLaunch(widget.propertiesModel.propertyLinkSahibinden!))
                                  await launch(widget.propertiesModel.propertyLinkSahibinden!);
                            },
                            color: Colors.blue,
                            child: Text(
                              "İlanı Gör",
                              style: TextStyle(color: Colors.white),
                            ),
                          ),
                        if (widget.propertiesModel.leaseStatus != null &&
                            widget.propertiesModel.leaseStatus !=
                                LeaseStatus.leased)
                          MaterialButton(
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(12)),
                            onPressed: () {
                              Utility.showCancelRentPropertyPopup(
                                  context, widget.propertiesModel, () {
                                setState(() {});
                              });
                            },
                            color: Colors.red,
                            child: Text(
                              "my_properties_screen.cancel_rent_my_property"
                                  .tr(),
                              style: TextStyle(color: Colors.white),
                            ),
                          ),
                      ],
                    )
                  ],
                ),
        ),
        if (widget.propertiesModel.leaseStatus != null &&
            widget.propertiesModel.leaseStatus == LeaseStatus.pendingListing)
          Positioned(
            right: local.languageCode != "ar" ? 10 : null,
            left: local.languageCode == "ar" ? 10 : null,
            top: 0,
            child: Container(
              padding: EdgeInsets.only(top: 5, bottom: 5, left: 15, right: 15),
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(8),
                  color: Color.fromARGB(255, 240, 144, 137)),
              child: Text(
                "İlan hazırlanıyor",
                style: TextStyle(
                    color: Colors.black,
                    fontSize: 10,
                    fontWeight: FontWeight.bold),
              ),
            ),
          ),
        if (widget.propertiesModel.leaseStatus != null &&
            widget.propertiesModel.leaseStatus == LeaseStatus.onListing)
          Positioned(
            right: local.languageCode != "ar" ? 10 : null,
            left: local.languageCode == "ar" ? 10 : null,
            top: 0,
            child: Container(
              padding: EdgeInsets.only(top: 5, bottom: 5, left: 15, right: 15),
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(8),
                  color: Color.fromARGB(255, 240, 173, 96)),
              child: Text(
                "my_properties_screen.on_listing".tr(),
                style: TextStyle(
                    color: Colors.black,
                    fontSize: 10,
                    fontWeight: FontWeight.bold),
              ),
            ),
          ),
        if ((widget.propertiesModel.contractPath != null &&
            widget.propertiesModel.contractPath! != "" &&
            widget.propertiesModel.leaseStatus != null &&
            widget.propertiesModel.leaseStatus == LeaseStatus.leased))
          Positioned(
            right: local.languageCode != "ar" ? 10 : null,
            left: local.languageCode == "ar" ? 10 : null,
            top: 8,
            child: AddPropertyRibbonWidget(
              color: TurklinkColors.mainColor2,
              notification: Icon(
                Icons.check,
                color: Colors.white,
                size: 14,
              ),
            ),
          ),
        if (widget.propertiesModel.leaseProcessStatus ==
            PropertyProcessStatus.pendingInfoFromAdmin)
          Positioned(
            right: local.languageCode != "ar" ? 10 : null,
            left: local.languageCode == "ar" ? 10 : null,
            top: 15,
            child: Text(
              "Fiyat araştırması yapılıyor",
              style: TextStyle(color: Colors.orange),
            ),
          ),
      ],
    );
  }

  void download(String url) async {
    final status = await Permission.storage.request();

    if (status.isGranted) {
      final externalDir = await getDownloadPath();

      final id = await FlutterDownloader.enqueue(
          url: url,
          savedDir: externalDir!,
          showNotification: true,
          openFileFromNotification: true,
          saveInPublicStorage: true);
    }
  }

  Future<String?> getDownloadPath() async {
    Directory? directory;
    try {
      directory = await getApplicationDocumentsDirectory();
    } catch (err, stack) {
      print("Cannot get download folder path");
    }
    return directory?.path;
  }
}
