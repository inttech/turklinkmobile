import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:turklink/models/properties_model.dart';
import 'package:turklink/models/user_model.dart';
import 'package:turklink/services/properties_services.dart';
import 'package:turklink/util/finance_type.dart';
import 'package:turklink/util/property_process_status.dart';
import 'package:turklink/util/transaction_status.dart';
import 'package:turklink/util/turklink_colors.dart';
import 'package:turklink/util/utility.dart';
import 'package:turklink/views/pages/main/my_properties/widgets/add_property_widgets/add_property_ribbon_widget.dart';

class MyPrpertyDetailsPropertyTaxWidget extends StatefulWidget {
  final PropertiesModel propertiesModel;
  MyPrpertyDetailsPropertyTaxWidget(this.propertiesModel);

  @override
  State<MyPrpertyDetailsPropertyTaxWidget> createState() =>
      _MyPrpertyDetailsPropertyTaxWidgetState();
}

class _MyPrpertyDetailsPropertyTaxWidgetState
    extends State<MyPrpertyDetailsPropertyTaxWidget> {
  @override
  Widget build(BuildContext context) {

    Locale local = context.locale;

    return Stack(
      children: [
        Container(
          width: MediaQuery.of(context).size.width,
          padding: EdgeInsets.symmetric(vertical: 25, horizontal: 15),
          margin: EdgeInsets.only(top: 10),
          decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.circular(15),
          ),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              if (widget.propertiesModel.propertyTaxProcessStatus == PropertyProcessStatus.pendingInfoFromAdmin || widget.propertiesModel.propertyTaxProcessStatus == PropertyProcessStatus.complate)
              Text(
                ("my_properties_screen.property_tax_title").tr(),
                style: TextStyle(
                    color: TurklinkColors.mainColor,
                    fontSize: 12,
                    fontWeight: FontWeight.bold),
              ),
              if (widget.propertiesModel.propertyTaxProcessStatus == PropertyProcessStatus.pendingAproveFromUser || widget.propertiesModel.propertyTaxProcessStatus == PropertyProcessStatus.pendingProcessFromAdmin)
                Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      ("my_properties_screen.property_tax_title").tr(),
                      style: TextStyle(
                          color: TurklinkColors.mainColor,
                          fontSize: 12,
                          fontWeight: FontWeight.bold),
                    ),
                    Text(
                      widget.propertiesModel.propertyTaxYear!.toString() +
                          " " +
                          ("my_properties_screen.property_tax_installment_" +
                                  (widget.propertiesModel.propertyTaxInstallment!
                                      .toString()))
                              .tr() +
                          " " +
                          Utility.convertCurrency(widget.propertiesModel.propertyTaxAmount!)
                              .toString() +
                          " TL",
                      style: TextStyle(fontSize: 10, color: Colors.black54),
                    ),
                  ],
                ),
              if (widget.propertiesModel.propertyTaxInstallment != null && widget.propertiesModel.propertyTaxProcessStatus == PropertyProcessStatus.complate)
                Text(widget.propertiesModel.propertyTaxYear!.toString() +
                    " " +
                    ("my_properties_screen.property_tax_installment_" +
                            widget.propertiesModel.propertyTaxInstallment!
                                .toString())
                        .tr()),
              if (widget.propertiesModel.propertyTaxProcessStatus == PropertyProcessStatus.pendingAproveFromUser)
                MaterialButton (
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(12)),
                  onPressed: () async{
                    var userModel = await FirebaseFirestore.instance.collection("users").doc(FirebaseAuth.instance.currentUser!.uid).get();
                    var userData  = UserModel.fromJson(userModel.data()!);
                    await Utility.showPaymentMethodPopup(context,userData.tryBalance,FinanceType.propertyTax,widget.propertiesModel.propertyTaxAmount!,propertyId: widget.propertiesModel.id,);
                    PropertiesServices propertiesServices = new PropertiesServices();
                    var property = await propertiesServices.getProperty(widget.propertiesModel.id!);
                    widget.propertiesModel.propertyTaxProcessStatus = property!.propertyTaxProcessStatus;
                    setState(() {

                    });
                  },
                  color: TurklinkColors.mainColor2,
                  child: Text(
                    "my_properties_screen.pay_property".tr(),
                    style: TextStyle(color: Colors.white),
                  ),
                ),
            ],
          ),
        ),
        if (widget.propertiesModel.propertyTaxProcessStatus == PropertyProcessStatus.pendingAproveFromUser)
          Positioned(
            right: local.languageCode != "ar" ? 10 : null,
            left: local.languageCode == "ar" ? 10 : null,
            top: 8,
            child: AddPropertyRibbonWidget(
              color: Colors.orange,
              notification: Icon(
                FontAwesomeIcons.exclamation,
                color: Colors.white,
                size: 14,
              ),
            ),
          ),
        if (widget.propertiesModel.propertyTaxProcessStatus == PropertyProcessStatus.complate)
          Positioned(
            right: local.languageCode != "ar" ? 10 : null,
            left: local.languageCode == "ar" ? 10 : null,
            top: 15,
            child: Text(
              "my_properties_screen.paid_property".tr(),
              style: TextStyle(color: TurklinkColors.mainColor2),
            ),
          ),
          if (widget.propertiesModel.propertyTaxProcessStatus == PropertyProcessStatus.pendingInfoFromAdmin || widget.propertiesModel.propertyTaxProcessStatus == PropertyProcessStatus.pendingProcessFromAdmin)
          Positioned(
            right: local.languageCode != "ar" ? 10 : null,
            left: local.languageCode == "ar" ? 10 : null,
            top: 15,
            child: Text(
              ("my_properties_screen.pendingAdmin_" +  widget.propertiesModel.propertyTaxProcessStatus.toString()).tr(),
              style: TextStyle(color: Colors.orange),
            ),
          ),
      ],
    );
  }
}
