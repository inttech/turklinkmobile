import 'package:flutter/material.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:turklink/models/key_value_model.dart';
import 'package:turklink/services/address_services.dart';
import 'package:turklink/views/widgets/turklink_dropdown_field.dart';
import 'package:turklink/views/widgets/turklink_textfield.dart';

class PropertyAddressWidget extends StatefulWidget {

  KeyValueModel? cityValue;
  KeyValueModel? districtValue;
  KeyValueModel? placeValue;
  TextEditingController neighborhoodController = TextEditingController();
  TextEditingController streetController = TextEditingController();
  TextEditingController numberController = TextEditingController();
  TextEditingController doorNumberController = TextEditingController();

  bool? isEdit;

  PropertyAddressWidget();

  @override
  State<PropertyAddressWidget> createState() =>
      _PropertyAddressWidgetState();
}

class _PropertyAddressWidgetState extends State<PropertyAddressWidget> {
  List<KeyValueModel>? emptyList = [KeyValueModel(key: "", value: "")];
  List<KeyValueModel>? cityList;
  List<KeyValueModel>? districtList;
  List<KeyValueModel>? placeList;
  List<KeyValueModel>? neighborhoodList;
  AddressServices addressServices = new AddressServices();

  @override
  void initState() {
    getCities();
    super.initState();
  }

  getCities() async {
    cityList = await addressServices.getCities();
    if(widget.isEdit != null && widget.isEdit!){
      if(widget.cityValue != null)
        await getDisricts(widget.cityValue!.key!);
      if(widget.districtValue != null)
        await getPlaces(widget.districtValue!.key!);
     // if(widget.placeValue != null)
       // await getNeigborhoods(widget.placeValue!.key!);
    }

    setState(() {});
  }

  getDisricts(String cityId) async {
    if(widget.isEdit == null || widget.isEdit! == false){
      widget.districtValue = null;
      widget.placeValue = null;
      widget.neighborhoodController.text= "";
    }
    districtList = await addressServices.getDistrict(cityId);
    setState(() {});
  }

  getPlaces(String disrictId) async {
    if(widget.isEdit == null || widget.isEdit! == false){
      widget.placeValue = null;
      widget.neighborhoodController.text= "";
    }
    placeList = await addressServices.getPlace(disrictId);
    setState(() {});
  }

  getNeigborhoods(String placeId) async {
    if(widget.isEdit == null || widget.isEdit! == false)
      widget.neighborhoodController.text= "";

    neighborhoodList = await addressServices.getNeigborhood(placeId);
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text("add_property_screen.address_title".tr()),
        SizedBox(
          height: 10,
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Expanded(
              child: TurklinkDropdownField(
                labelText: "add_property_screen.city".tr(),
                list: cityList != null ? cityList : emptyList,
                value:  cityList != null ? widget.cityValue : null,
                onChanged: (item) {
                  widget.cityValue = item;
                  getDisricts(item.key);
                },
              ),
            ),
            SizedBox(
              width: 15,
            ),
            Expanded(
              child: TurklinkDropdownField(
                labelText: "add_property_screen.district".tr(),
                list: districtList != null ? districtList : emptyList,
                value: districtList != null ? widget.districtValue : null,
                onChanged: (item) {
                  widget.districtValue = item;
                  getPlaces(item.key);
                },
              ),
            ),
          ],
        ),
        SizedBox(
          height: 10,
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Expanded(
              child: TurklinkDropdownField(
                labelText: "add_property_screen.place".tr(),
                list: placeList != null ? placeList : emptyList,
                value: placeList != null ? widget.placeValue : null,
                onChanged: (item) {
                  widget.placeValue = item;
                  //getNeigborhoods(item.key);
                },
              ),
            ),
            SizedBox(
              width: 15,
            ),
            Expanded(
              child: TurklinkTextField(
                labelText:
                    "add_property_screen.neighbourhood".tr(),
                obscureText: false,
                textInputType: TextInputType.text,
                controller: widget.neighborhoodController,
              ),
            ),
          ],
        ),
        SizedBox(
          height: 10,
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Expanded(
              child: TurklinkTextField(
                labelText:
                    "add_property_screen.street".tr(),
                obscureText: false,
                textInputType: TextInputType.text,
                controller: widget.streetController,
              ),
            ),
            SizedBox(
              width: 15,
            ),
           Expanded(
              child: TurklinkTextField(
                labelText:
                    "add_property_screen.number".tr(),
                obscureText: false,
                textInputType: TextInputType.text,
                controller: widget.numberController,
              ),
            ),
          ],
        ),
        SizedBox(
          height: 10,
        ),
        Container(
          width: double.infinity,
          child: Row(
            children: [
              Expanded(
                child: TurklinkTextField(
                      labelText:
                          "Kapı Numarası",
                      obscureText: false,
                      textInputType: TextInputType.text,
                      controller: widget.doorNumberController,
                    ),
              ),
              SizedBox(
              width: 15,
            ),
              Expanded(child: Container())
            ],
          ),
          
        )
      ],
    );
  }
}
