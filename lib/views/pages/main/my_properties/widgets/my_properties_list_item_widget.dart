import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:turklink/models/properties_model.dart';
import 'package:turklink/util/property_status.dart';
import 'package:turklink/util/turklink_colors.dart';
import 'package:turklink/util/utility.dart';
import 'package:turklink/views/pages/main/my_properties/edit_property_screen.dart';
import 'package:turklink/views/pages/main/my_properties/my_property_details_screen.dart';
import 'package:turklink/views/pages/main/my_properties/widgets/add_property_widgets/add_property_ribbon_widget.dart';
import 'package:easy_localization/easy_localization.dart';

class MyPropertiesListItemWidget extends StatelessWidget {
  final PropertiesModel propertiesModel;

  MyPropertiesListItemWidget({required this.propertiesModel});

  @override
  Widget build(BuildContext context) {

    Locale local = context.locale;

    return InkWell(
      onTap: (){
         Navigator.push(
          context,
          MaterialPageRoute(
            builder: (context) => propertiesModel.status == PropertyStatus.pendingApproval ? EditPropertyScreen(propertiesModel) : MyPropertyDetailsScreen(propertiesModel),
          ),
        );
      },
      child: Stack(
        children: [
          Padding(
            padding: const EdgeInsets.only(top:8.0),
            child: Container(
              width: MediaQuery.of(context).size.width,
              padding: EdgeInsets.all(15),
              decoration: BoxDecoration(
                  color: Colors.white, borderRadius: BorderRadius.circular(15)),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Container(
                    width: 85,
                    height: 85,
                    margin:  local.languageCode != "ar" ? EdgeInsets.only(right: 15) : EdgeInsets.only(left: 15),
                    decoration:
                        BoxDecoration(borderRadius: BorderRadius.circular(15)),
                    child: this.propertiesModel.imagePath != null
                        ? ClipRRect(
                            borderRadius: BorderRadius.circular(15),
                            child: CachedNetworkImage(
                              imageUrl: this.propertiesModel.imagePath!,
                              fit: BoxFit.cover,
                              placeholder: (context, url) => Padding(
                                padding: const EdgeInsets.all(15.0),
                                child: CircularProgressIndicator(
                                  strokeWidth: 1,
                                  value: 1,
                                ),
                              ),
                              errorWidget: (context, url, error) =>
                                  Icon(Icons.error),
                            ),
                          )
                        : Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Image(
                              image: AssetImage(
                                "assets/images/home.png",
                              ),
                            ),
                        ),
                  ),
                  Container(
                    height: 85,
                    padding: EdgeInsets.only(top: 5, bottom: 5),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          this.propertiesModel.title!,
                          style: TextStyle(
                              fontWeight: FontWeight.bold),
                        ),
                        Text(
                          Utility.getPropertyItemSubTitle(propertiesModel),
                          style: TextStyle(
                              color: TurklinkColors.mainColor,
                              fontWeight: FontWeight.bold),
                        ),
                        SizedBox(
                          height: 10,
                        ),
                        Text(
                          Utility.getPropertyItemLeasedContent(propertiesModel),
                          style: TextStyle(
                              fontSize: 10, color: Colors.black.withOpacity(.5),fontWeight: FontWeight.w900),
                        )
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ),
          if(propertiesModel.status == PropertyStatus.pendingApproval)
          Positioned(
            right: local.languageCode != "ar" ? 10 : null,
            left: local.languageCode == "ar" ? 10 : null,
            top: 0,
            child: Container(
            padding: EdgeInsets.only(top: 5,bottom: 5,left: 15,right: 15),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(8),
                color: Color.fromARGB(255, 240, 144, 137)
              ),
              child: Text("my_properties_screen.pending_activation".tr(),style: TextStyle(color: Colors.black,fontSize: 10,fontWeight: FontWeight.bold),),
            ),
          ),
          Visibility(
            child: AddPropertyRibbonWidget(
              color: Colors.green,
              notification: Icon(
                Icons.check,
                color: Colors.white,
                size: 14,
              ),
            ),
            visible: false,
          ),
        ],
      ),
    );
  }
}
