import 'package:flutter/material.dart';

class RequestEmptyWidget extends StatefulWidget {
  final String message;
  const RequestEmptyWidget(this.message);

  @override
  State<RequestEmptyWidget> createState() => _RequestEmptyWidgetState();
}

class _RequestEmptyWidgetState extends State<RequestEmptyWidget> {
  @override
  Widget build(BuildContext context) {
    return Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Icon(Icons.info_outline,color: Colors.grey,size: 60,),
          SizedBox(height: 10,),
          Text(widget.message,style: TextStyle(color: Colors.grey,fontSize: 18),textAlign: TextAlign.center,)
        ],
      ),
    );
  }
}