import 'package:another_flushbar/flushbar_helper.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:rounded_loading_button/rounded_loading_button.dart';
import 'package:turklink/models/key_value_model.dart';
import 'package:turklink/models/properties_model.dart';
import 'package:turklink/models/request_model.dart';
import 'package:turklink/services/request_services.dart';
import 'package:turklink/services/utility_services.dart';
import 'package:turklink/util/turklink_colors.dart';
import 'package:turklink/util/utility.dart';
import 'package:turklink/views/pages/main/my_properties/widgets/request_widgets/new_request_popup_image_widget.dart';
import 'package:turklink/views/widgets/turklink_button.dart';
import 'package:turklink/views/widgets/turklink_dropdown_field.dart';
import 'package:turklink/views/widgets/turklink_textfield.dart';
import 'package:flutter_datetime_picker/flutter_datetime_picker.dart';

class NewRequestPopupWidget extends StatefulWidget {
  final PropertiesModel propertiesModel;
  final BuildContext parentContext;
  NewRequestPopupWidget(
      {required this.propertiesModel, required this.parentContext});

  @override
  State<NewRequestPopupWidget> createState() => _NewRequestPopupWidgetState();
}

class _NewRequestPopupWidgetState extends State<NewRequestPopupWidget> {
  UtilityServices utilityServices = new UtilityServices();
  RequestServices requestServices = new RequestServices();
  var requestController = new RoundedLoadingButtonController();
  Color requestMainTypeColor = Colors.black87;
  Color requestSubTypeColor = Colors.black87;
  Color cleaningColor = Colors.black87;
  Color saleRequestColor = Colors.black87;
  KeyValueModel requestMainType = KeyValueModel(
      key: "VALUATION_REPORT", value: "request_type.VALUATION_REPORT".tr());
  KeyValueModel requestSubType = KeyValueModel();

  TextEditingController saleAmountController = TextEditingController();
  TextEditingController cleaningDateController = TextEditingController();
  TextEditingController serviceRequestDescriptionController =TextEditingController();
  var imageFile1 = NewRequestPopupImageWidget();
  var imageFile2 = NewRequestPopupImageWidget();
  var imageFile3 = NewRequestPopupImageWidget();
  var imageFile4 = NewRequestPopupImageWidget();
  bool isShowPayPage = false;

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(12)),
      titlePadding: EdgeInsets.symmetric(vertical: 20, horizontal: 20),
      contentPadding: EdgeInsets.symmetric(vertical: 10, horizontal: 20),
      title: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Text(
            "request_screen.new_request_title".tr(),
            style: TextStyle(fontSize: 15),
          ),
          InkWell(
            onTap: () {
              Navigator.of(widget.parentContext).pop();
            },
            child: Icon(
              Icons.cancel,
              color: Colors.red,
              size: 28,
            ),
          )
        ],
      ),
      content: Wrap(
        children: [
          Column(
            children: [
              SizedBox(
                height: 52,
                width: double.infinity,
                child: TurklinkDropdownField(
                  labelText: "request_screen.request_main_type".tr(),
                  list: utilityServices.getRequestMainType(),
                  borderColor: requestMainTypeColor,
                  value: KeyValueModel(
                      key: "VALUATION_REPORT",
                      value: "request_type.VALUATION_REPORT".tr()),
                  onChanged: (item) {
                    requestMainType = item;
                    imageFile1.imageFile = null;
                    imageFile2.imageFile = null;
                    imageFile3.imageFile = null;
                    imageFile4.imageFile = null;
                    saleAmountController.text = "";
                    serviceRequestDescriptionController.text = "";
                    setState(() {});
                  },
                ),
              ),
              if (requestMainType.key! == "VALUATION_REPORT" ||
                  requestMainType.key! == "SERVICE_REQUEST" ||
                  requestMainType.key! == "EVICTION_REQUEST")
                Container(
                  margin: EdgeInsets.only(top: 15),
                  child: SizedBox(
                    height: 52,
                    width: double.infinity,
                    child: TurklinkDropdownField(
                      labelText: "request_screen.request_sub_type".tr(),
                      list: utilityServices
                          .getRequestSubype(requestMainType.key!),
                      borderColor: requestSubTypeColor,
                      onChanged: (item) {
                        requestSubType = item;
                        setState(() {

                        });
                      },
                    ),
                  ),
                ),
              if (requestMainType.key! == "SALES_REQUEST")
                Container(
                  margin: EdgeInsets.only(top: 15),
                  child: SizedBox(
                    height: 52,
                    width: double.infinity,
                    child: Container(
                      width: double.infinity,
                      height: double.infinity,
                      child: TurklinkTextField(
                        borderColor: saleRequestColor,
                        labelText: "request_screen.sale_amount".tr(),
                        obscureText: false,
                        textInputType: TextInputType.number,
                        controller: saleAmountController,
                      ),
                    ),
                  ),
                ),
              if (requestMainType.key! == "CLEANING")
                Container(
                  margin: EdgeInsets.only(top: 15),
                  child: SizedBox(
                    height: 52,
                    width: double.infinity,
                    child: Container(
                      width: double.infinity,
                      height: double.infinity,
                      child: TurklinkTextField(
                        labelText: "request_screen.cleaning_day".tr(),
                        obscureText: false,
                        borderColor: cleaningColor,
                        onTap: () {
                          DatePicker.showDatePicker(context,
                              showTitleActions: true,
                              minTime:
                                  DateTime.now().add(new Duration(days: 1)),
                              onConfirm: (date) {
                            DateFormat formatter = DateFormat('yyyy-MM-dd');
                            cleaningDateController.text = formatter.format(date);
                            setState(() {});
                          },
                              currentTime: DateTime.now(),
                              locale: LocaleType.tr);
                        },
                        textInputType: TextInputType.number,
                        controller: cleaningDateController,
                      ),
                    ),
                  ),
                ),
              if (requestMainType.key! == "SERVICE_REQUEST")
                Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Container(
                      margin: EdgeInsets.only(top: 15),
                      child: SizedBox(
                        height: 100,
                        width: double.infinity,
                        child: Container(
                          width: double.infinity,
                          height: double.infinity,
                          child: TurklinkTextField(
                            controller: serviceRequestDescriptionController,
                            labelText:
                                "request_screen.service_request_description"
                                    .tr(),
                            obscureText: false,
                            borderColor: Colors.black87,
                            maxLines: 4,
                          ),
                        ),
                      ),
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        Expanded(
                          child: imageFile1,
                        ),
                        SizedBox(
                          width: 5,
                        ),
                        Expanded(
                          child: imageFile2,
                        ),
                        SizedBox(
                          width: 5,
                        ),
                        Expanded(
                          child: imageFile3,
                        ),
                        SizedBox(
                          width: 5,
                        ),
                        Expanded(
                          child: imageFile4,
                        ),
                      ],
                    ),
                  ],
                ),
                 if ((requestMainType.key! == "VALUATION_REPORT" && requestSubType.key != null) || requestMainType.key! == "CLEANING")
                 Container(
                   margin: EdgeInsets.only(top: 15),
                   child: Row(
                     mainAxisAlignment: MainAxisAlignment.spaceBetween,
                     crossAxisAlignment: CrossAxisAlignment.center,
                     children: [
                       Text(("request_screen." + requestMainType.key! + "_amount_text").tr(),style: TextStyle(color: Colors.red,fontSize: 15,fontWeight: FontWeight.bold),),
                        if ((requestMainType.key! == "VALUATION_REPORT" && requestSubType.key != null))
                         Text(Utility.convertCurrency(utilityServices.getRequestAmount( requestSubType.key!, null)) + " TL",style: TextStyle(color: Colors.red,fontSize: 15,fontWeight: FontWeight.bold),),
                        if (requestMainType.key! == "CLEANING")
                         Text(Utility.convertCurrency(utilityServices.getRequestAmount("CLEANING", int.parse(widget.propertiesModel.propertySize!))) + " TL",style: TextStyle(color: Colors.red,fontSize: 15,fontWeight: FontWeight.bold),),
                     ],
                   ),
                 ),
                if (requestMainType.key! == "VALUATION_REPORT" ||
                  requestMainType.key! == "CLEANING" ||
                  requestMainType.key! == "EVICTION_REQUEST" ||
                   requestMainType.key! == "SALES_REQUEST")
                  Container(
                     margin: EdgeInsets.only(top: 15),
                     child: Wrap(
                       children: [
                         Text(("request_screen." + requestMainType.key! + "_sub_text").tr(),style: TextStyle(color: Colors.blue.shade800,fontSize: 14,fontWeight: FontWeight.w800),)
                       ],
                     ),
                  ),
            ],
          ),
        ],
      ),
      actionsAlignment: MainAxisAlignment.start,
      actionsPadding: EdgeInsets.only(left: 10, right: 10, bottom: 10),
      actions: <Widget>[
        SizedBox(
          height: 52,
          width: MediaQuery.of(context).size.width,
          child: TurklinkButton(
            width: MediaQuery.of(context).size.width,
            backgroundColor: TurklinkColors.mainColor,
            buttonText: "request_screen.approve_button".tr(),
            borderColor: TurklinkColors.mainColor,
            textColor: Colors.white,
            btnController: requestController,
            animatedOnTap: true,
            onPressed: () async {

              if(requestMainType.key! == "VALUATION_REPORT" || requestMainType.key! == "EVICTION_REQUEST" || requestMainType.key! == "SERVICE_REQUEST")
              {
                  if(requestSubType.key == null)
                  {
                     requestController.reset();
                     requestSubTypeColor = Colors.red;
                     setState(() {

                     });
                     return;
                  }
              }
              if(requestMainType.key! == "SALES_REQUEST")
              {
                  if(saleAmountController.text == "")
                  {
                    requestController.reset();
                    saleRequestColor = Colors.red;
                    setState(() {

                    });
                    return;
                  }
              }

              if(requestMainType.key! == "CLEANING")
              {
                 if(cleaningDateController.text == "")
                 {
                   requestController.reset();
                   cleaningColor = Colors.red;
                   setState(() {

                   });
                   return;
                 }
              }

                 KeyValueModel subRequestType = new KeyValueModel();
                 if(requestMainType.key! == "CLEANING")
                 {
                   subRequestType.key = "CLEANING_DAY";
                   subRequestType.value = cleaningDateController.text;
                 }
                 else if(requestMainType.key! == "SALES_REQUEST")
                 {
                   subRequestType.key = "SALES_AMOUNT";
                   subRequestType.value = (int.parse(saleAmountController.text)*100).toString();
                 }
                 else
                 {
                    subRequestType = requestSubType;
                 }

                 RequestModel requestModel = new RequestModel(
                   propertiesModel: widget.propertiesModel,
                   mainRequestType: requestMainType,
                   subRequestType: subRequestType,
                   description: serviceRequestDescriptionController.text,
                   imageFile1: imageFile1.imageFile,
                   imageFile2: imageFile2.imageFile,
                   imageFile3: imageFile3.imageFile,
                   imageFile4: imageFile4.imageFile,
                 );
                 if(requestMainType.key! == "CLEANING")
                  requestModel.requestAmount = utilityServices.getRequestAmount("CLEANING", int.parse(widget.propertiesModel.propertySize!));
                 if(requestMainType.key! == "VALUATION_REPORT")
                  requestModel.requestAmount = utilityServices.getRequestAmount( requestSubType.key!, null);

                 var result = await requestServices.addPRequest(requestModel);
                 requestController.reset();
                 if(!result)
                 {
                  FlushbarHelper.createError(
                    title: "popup_message.popup_error_title".tr(),
                    message: requestServices.errorMessage
                  ).show(widget.parentContext);
                 }
                 else
                 {
                  Navigator.of(widget.parentContext).pop();
                  await FlushbarHelper.createSuccess(
                    title: "popup_message.popup_success_title".tr(),
                    message: "popup_message.popup_success_message".tr(),
                  ).show(widget.parentContext);
                 }


            },
          ),
        )
      ],
    );
  }
}
