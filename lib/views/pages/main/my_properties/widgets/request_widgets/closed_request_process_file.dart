import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';

class ClosedRequestProcessFile extends StatefulWidget {

  String? fileUrl;

  @override
  State<ClosedRequestProcessFile> createState() => _ClosedRequestProcessFileState();
}

class _ClosedRequestProcessFileState extends State<ClosedRequestProcessFile> {
  @override
  Widget build(BuildContext context) {
    return Container(
      height: 80,
      child: Stack(
        children: [
          Padding(
            padding: const EdgeInsets.only(top: 10),
            child: Container(
              height: 60,
              width: double.infinity,
              decoration: BoxDecoration(
                border: Border.all(width: .5, color: Colors.black54),
                borderRadius: BorderRadius.circular(15),
              ),
              child: Padding(
                    padding: const EdgeInsets.all(5.0),
                    child: GestureDetector(
                      onTap: () async{
                        if (widget.fileUrl != null && await canLaunch(widget.fileUrl!)){
                          await launch(widget.fileUrl!);
                        }
                      },
                      child: Center(
                        child: Icon(
                          Icons.file_download
                        ),
                      ),
                    ),
                  )
            ),
          ),
        ],
      ),
    );
  }
}