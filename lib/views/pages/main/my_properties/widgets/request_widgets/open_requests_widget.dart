import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:rounded_loading_button/rounded_loading_button.dart';
import 'package:turklink/models/properties_model.dart';
import 'package:turklink/models/request_model.dart';
import 'package:turklink/util/request_status.dart';
import 'package:turklink/util/turklink_colors.dart';
import 'package:turklink/util/utility.dart';
import 'package:turklink/views/pages/main/my_properties/widgets/request_widgets/request_empty_widget.dart';
import 'package:turklink/views/pages/main/my_properties/widgets/request_widgets/request_list_item_widget.dart';
import 'package:turklink/views/widgets/turklink_button.dart';

class OpenRequestsWidget extends StatefulWidget {
  final PropertiesModel propertiesModel;
  OpenRequestsWidget(this.propertiesModel);

  @override
  State<OpenRequestsWidget> createState() => _OpenRequestsWidgetState();
}

class _OpenRequestsWidgetState extends State<OpenRequestsWidget> {
  var btnController = new RoundedLoadingButtonController();

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.only(left: 15,right: 15,top: 15),
      child: Column(
        children: [
          SizedBox(
            height: 52,
            width: MediaQuery.of(context).size.width,
            child: TurklinkButton(
              width: MediaQuery.of(context).size.width,
              backgroundColor: TurklinkColors.mainColor,
              buttonText: "request_screen.new_request_button".tr(),
              borderColor: TurklinkColors.mainColor,
              textColor: Colors.white,
              btnController: btnController,
              animatedOnTap: false,
              onPressed: () {
                Utility.showRequestPopup(context, widget.propertiesModel);
              },
            ),
          ),
          Expanded(
            child: StreamBuilder(
                stream: FirebaseFirestore.instance
                    .collection("requests")
                    .where('propertiesId', isEqualTo: widget.propertiesModel.id)
                    .where("isDeleted", isEqualTo: 0)
                    .where("status",whereIn: [RequestStatus.pendingOffer,RequestStatus.waitingPayment,RequestStatus.onProgress])
                    .orderBy("updatedDate", descending: true)
                    .snapshots(),
                builder: (BuildContext context,
                    AsyncSnapshot<QuerySnapshot> snapshot) {
                  if (snapshot.connectionState == ConnectionState.active) {
                    if (snapshot.hasData && snapshot.data!.docs.length > 0) {
                      return  ListView.builder(
                              itemCount: snapshot.data!.docs.length,
                              itemBuilder: (context, index) {
                                Map<String, dynamic> data =
                                    snapshot.data!.docs[index].data()!
                                        as Map<String, dynamic>;
                                var requestModel =
                                    RequestModel.fromJson(data);
                                return Container(
                                  margin: EdgeInsets.only(top:7,bottom: 7),
                                  child: GestureDetector(
                                    onTap: () {
                                      Utility.showRequestEditPopup(context, requestModel);
                                    },
                                    child: RequestListItemWidget(
                                      requestModel: requestModel,
                                    ),
                                  ),
                                );
                              },
                            );
                    } else {
                      return RequestEmptyWidget("request_screen.empty_list".tr());
                    }
                  } else {
                    return Center(
                      child: CircularProgressIndicator(),
                    );
                  }
                }),
          ),
        ],
      ),
    );
  }
}
