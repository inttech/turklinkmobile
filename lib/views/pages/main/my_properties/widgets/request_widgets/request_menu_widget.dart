import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:turklink/util/turklink_colors.dart';

class RequestMenuWidget extends StatefulWidget {

  Function onTap;

  RequestMenuWidget({required this.onTap});

  @override
  State<RequestMenuWidget> createState() => _RequestMenuWidgetState();
}

class _RequestMenuWidgetState extends State<RequestMenuWidget> {

  int pgIx = 0;

  @override
  Widget build(BuildContext context) {
    return Container(
          width: MediaQuery.of(context).size.width,
          height: 60,
          decoration: BoxDecoration(
            color: Colors.white,
            border: Border(
              bottom: BorderSide(
                color: Colors.grey.shade200
              ),
            ),
          ),
          child: Row(
            children: [
              Expanded(
                flex: 1,
                child: InkWell(
                  onTap: ()
                  {
                     widget.onTap(0);
                     setState(() {
                      pgIx = 0;
                     });
                  },
                  child: Padding(
                    padding: EdgeInsets.only(left:15,right: 15),
                    child: Container(
                      height: 60,
                      decoration: BoxDecoration(
                        color: Colors.white,
                        border: Border(
                          bottom: BorderSide(
                            color: pgIx == 0 ? TurklinkColors.mainColor : Colors.white,
                            width: 3.0,
                          ),
                        ),
                      ),
                      child: Center(child: Text("request_screen.open_request".tr(),textAlign: TextAlign.center,style: TextStyle(fontWeight: pgIx == 0 ? FontWeight.bold : FontWeight.normal),)),
                    ),
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(top:15,bottom: 15),
                child: Container(
                  decoration: BoxDecoration(
                          border: Border.all(
                            color: Colors.grey.shade200
                          ),
                        ),
                ),
              ),
              Expanded(
                flex: 1,
                child: InkWell(
                  onTap: ()
                  {
                     widget.onTap(1);
                     setState(() {
                      pgIx = 1;
                    });
                  },
                  child: Padding(
                    padding: EdgeInsets.only(left:15,right: 15),
                    child: Container(
                      height: 60,
                      decoration: BoxDecoration(
                        color: Colors.white,
                        border: Border(
                          bottom: BorderSide(
                            color: pgIx == 1 ? TurklinkColors.mainColor : Colors.white,
                            width: 3.0,
                          ),
                        ),
                      ),
                      child: Center(child: Text("request_screen.closed_request".tr(),textAlign: TextAlign.center,style: TextStyle(fontWeight: pgIx == 1 ? FontWeight.bold : FontWeight.normal),)),
                    ),
                  ),
                ),
              ),
            ],
          ),
        );
  }
}