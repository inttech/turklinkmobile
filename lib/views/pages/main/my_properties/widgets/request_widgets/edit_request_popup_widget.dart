import 'package:another_flushbar/flushbar_helper.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_datetime_picker/flutter_datetime_picker.dart';
import 'package:rounded_loading_button/rounded_loading_button.dart';
import 'package:turklink/models/key_value_model.dart';
import 'package:turklink/models/request_model.dart';
import 'package:turklink/services/request_services.dart';
import 'package:turklink/services/utility_services.dart';
import 'package:turklink/util/request_status.dart';
import 'package:turklink/util/turklink_colors.dart';
import 'package:turklink/util/utility.dart';
import 'package:turklink/views/pages/main/my_properties/widgets/request_widgets/closed_request_process_file.dart';
import 'package:turklink/views/pages/main/my_properties/widgets/request_widgets/new_request_popup_image_widget.dart';
import 'package:turklink/views/widgets/turklink_button.dart';
import 'package:turklink/views/widgets/turklink_dropdown_field.dart';
import 'package:turklink/views/widgets/turklink_textfield.dart';

class EditRequestPopupWidget extends StatefulWidget {
  final RequestModel requestModel;
  final BuildContext parentContext;
  EditRequestPopupWidget(
      {required this.requestModel, required this.parentContext});

  @override
  State<EditRequestPopupWidget> createState() => _EditRequestPopupWidgetState();
}

class _EditRequestPopupWidgetState extends State<EditRequestPopupWidget> {
  UtilityServices utilityServices = new UtilityServices();
  RequestServices requestServices = new RequestServices();
  var requestController = new RoundedLoadingButtonController();
  var payController = new RoundedLoadingButtonController();
  var cancelController = new RoundedLoadingButtonController();
  Color requestMainTypeColor = Colors.black87;
  Color requestSubTypeColor = Colors.black87;
  Color cleaningColor = Colors.black87;
  Color saleRequestColor = Colors.black87;
  KeyValueModel requestMainType = KeyValueModel(
      key: "VALUATION_REPORT", value: "request_type.VALUATION_REPORT".tr());
  KeyValueModel requestSubType = KeyValueModel();

  TextEditingController saleAmountController = TextEditingController();
  TextEditingController cleaningDateController = TextEditingController();
  TextEditingController serviceRequestDescriptionController =TextEditingController();
  var imageFile1 = NewRequestPopupImageWidget();
  var imageFile2 = NewRequestPopupImageWidget();
  var imageFile3 = NewRequestPopupImageWidget();
  var imageFile4 = NewRequestPopupImageWidget();

  var processFile1 = ClosedRequestProcessFile();
  var processFile2 = ClosedRequestProcessFile();
  var processFile3 = ClosedRequestProcessFile();
  var processFile4 = ClosedRequestProcessFile();

  @override
  Widget build(BuildContext context) {

    DateFormat formatterEstimatedDate = DateFormat('dd.MM.yyyy');
    requestMainType = widget.requestModel.mainRequestType!;
    requestSubType = widget.requestModel.subRequestType!;
    if (requestMainType.key! == "SALES_REQUEST")
      saleAmountController.text = Utility.convertCurrency(int.parse(widget.requestModel.subRequestType!.value!));
    if (requestMainType.key! == "CLEANING")
      cleaningDateController.text = widget.requestModel.subRequestType!.value!;
    if (requestMainType.key! == "SERVICE_REQUEST")
    {
      serviceRequestDescriptionController.text = widget.requestModel.description!;
      imageFile1.imagePath = widget.requestModel.imagePath1;
      imageFile2.imagePath = widget.requestModel.imagePath2;
      imageFile3.imagePath = widget.requestModel.imagePath3;
      imageFile4.imagePath = widget.requestModel.imagePath4;
    }

    if(widget.requestModel.status == RequestStatus.closed)
    {
      processFile1.fileUrl = widget.requestModel.processFilePath1;
      processFile2.fileUrl = widget.requestModel.processFilePath2;
      processFile3.fileUrl = widget.requestModel.processFilePath3;
      processFile4.fileUrl = widget.requestModel.processFilePath4;
    }


    return AlertDialog(
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(12)),
      titlePadding: EdgeInsets.symmetric(vertical: 20, horizontal: 20),
      contentPadding: EdgeInsets.symmetric(vertical: 10, horizontal: 20),
      title: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Text(
            "request_screen.update_show_request_title".tr(),
            style: TextStyle(fontSize: 15),
          ),
          InkWell(
            onTap: () {
              Navigator.of(widget.parentContext).pop();
            },
            child: Icon(
              Icons.cancel,
              color: Colors.red,
              size: 28,
            ),
          )
        ],
      ),
      content: Wrap(
        children: [AbsorbPointer(
          absorbing:  widget.requestModel.status! != RequestStatus.pendingOffer,
          child: Column(
              children: [
                SizedBox(
                  height: 52,
                  width: double.infinity,
                  child: TurklinkDropdownField(
                    labelText: "request_screen.request_main_type".tr(),
                    list: utilityServices.getRequestMainType(),
                    borderColor: requestMainTypeColor,
                    value: widget.requestModel.mainRequestType,
                    onChanged: (item) {
                      requestMainType = item;
                      imageFile1.imageFile = null;
                      imageFile2.imageFile = null;
                      imageFile3.imageFile = null;
                      imageFile4.imageFile = null;
                      saleAmountController.text = "";
                      serviceRequestDescriptionController.text = "";
                      setState(() {});
                    },
                  ),
                ),
                if (requestMainType.key! == "VALUATION_REPORT" ||
                    requestMainType.key! == "SERVICE_REQUEST" ||
                    requestMainType.key! == "EVICTION_REQUEST")
                  Container(
                    margin: EdgeInsets.only(top: 15),
                    child: SizedBox(
                      height: 52,
                      width: double.infinity,
                      child: TurklinkDropdownField(
                        labelText: "request_screen.request_sub_type".tr(),
                        list: utilityServices
                            .getRequestSubype(requestMainType.key!),
                        borderColor: requestSubTypeColor,
                        value: widget.requestModel.subRequestType,
                        onChanged: (item) {
                          requestSubType = item;
                          setState(() {

                          });
                        },
                      ),
                    ),
                  ),
                if (requestMainType.key! == "SALES_REQUEST")
                  Container(
                    margin: EdgeInsets.only(top: 15),
                    child: SizedBox(
                      height: 52,
                      width: double.infinity,
                      child: TurklinkTextField(
                        borderColor: saleRequestColor,
                        labelText: "request_screen.sale_amount".tr(),
                        obscureText: false,
                        textInputType: TextInputType.number,
                        controller: saleAmountController,
                      ),
                    ),
                  ),
                if (requestMainType.key! == "CLEANING")
                  Container(
                    margin: EdgeInsets.only(top: 15),
                    child: SizedBox(
                      height: 52,
                      width: double.infinity,
                      child: TurklinkTextField(
                        labelText: "request_screen.cleaning_day".tr(),
                        obscureText: false,
                        borderColor: cleaningColor,
                        onTap: () {
                          DatePicker.showDatePicker(context,
                              showTitleActions: true,
                              minTime:
                                  DateTime.now().add(new Duration(days: 1)),
                              onConfirm: (date) {
                            DateFormat formatter = DateFormat('yyyy-MM-dd');
                            cleaningDateController.text = formatter.format(date);
                            setState(() {});
                          },
                              currentTime: DateTime.now(),
                              locale: LocaleType.tr);
                        },
                        textInputType: TextInputType.number,
                        controller: cleaningDateController,
                      ),
                    ),
                  ),
                if (requestMainType.key! == "SERVICE_REQUEST")
                  Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Container(
                        margin: EdgeInsets.only(top: 15),
                        child: SizedBox(
                          height: 100,
                          width: double.infinity,
                          child: TurklinkTextField(
                            controller: serviceRequestDescriptionController,
                            labelText:
                                "request_screen.service_request_description"
                                    .tr(),
                            obscureText: false,
                            borderColor: Colors.black87,
                            maxLines: 4,
                          ),
                        ),
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          Expanded(
                            child: imageFile1,
                          ),
                          SizedBox(
                            width: 5,
                          ),
                          Expanded(
                            child: imageFile2,
                          ),
                          SizedBox(
                            width: 5,
                          ),
                          Expanded(
                            child: imageFile3,
                          ),
                          SizedBox(
                            width: 5,
                          ),
                          Expanded(
                            child: imageFile4,
                          ),
                        ],
                      ),
                    ],
                  ),
                   if ((requestMainType.key! == "VALUATION_REPORT" && requestSubType.key != null) || requestMainType.key! == "CLEANING")
                   Container(
                     margin: EdgeInsets.only(top: 15),
                     child: Row(
                       mainAxisAlignment: MainAxisAlignment.spaceBetween,
                       crossAxisAlignment: CrossAxisAlignment.center,
                       children: [
                         Text(("request_screen." + requestMainType.key! + "_amount_text").tr(),style: TextStyle(color: Colors.red,fontSize: 15,fontWeight: FontWeight.bold),),
                          if ((requestMainType.key! == "VALUATION_REPORT" && requestSubType.key != null))
                           Text(Utility.convertCurrency(utilityServices.getRequestAmount( requestSubType.key!, null))+ " TL",style: TextStyle(color: Colors.red,fontSize: 15,fontWeight: FontWeight.bold),),
                          if (requestMainType.key! == "CLEANING")
                           Text(Utility.convertCurrency(utilityServices.getRequestAmount("CLEANING", int.parse(widget.requestModel.propertiesModel!.propertySize!))) + " TL",style: TextStyle(color: Colors.red,fontSize: 15,fontWeight: FontWeight.bold),),
                       ],
                     ),
                   ),
                  if (requestMainType.key! == "VALUATION_REPORT" ||
                    requestMainType.key! == "CLEANING" ||
                    requestMainType.key! == "EVICTION_REQUEST" ||
                     requestMainType.key! == "SALES_REQUEST")
                    Container(
                       margin: EdgeInsets.only(top: 15),
                       child: Wrap(
                         children: [
                           Text(("request_screen." + requestMainType.key! + "_sub_text").tr(),style: TextStyle(color: Colors.blue.shade800,fontSize: 14,fontWeight: FontWeight.w800),)
                         ],
                       ),
                    ),
                  if(widget.requestModel.status == RequestStatus.onProgress && widget.requestModel.estimatedDate != null)
                    SizedBox(height: 10,),
                  if(widget.requestModel.status == RequestStatus.onProgress && widget.requestModel.estimatedDate != null)
                    Container(width: MediaQuery.of(context).size.width, child: Text("request_screen.estimated_date".tr(args: [formatterEstimatedDate.format(widget.requestModel.estimatedDate!)]),style: TextStyle(fontSize: 12,fontWeight: FontWeight.bold),textAlign: TextAlign.start,)),
              ],
            ),
        ),
          if(widget.requestModel.status == RequestStatus.closed)
                    Column(
                      children: [
                         SizedBox(
                          height: 20,
                        ),
                        Text("request_screen.process_response_file"
                                    .tr(),style: TextStyle(fontWeight: FontWeight.bold),textAlign: TextAlign.start,),
                        SizedBox(
                          height: 10,
                        ),
                        Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        Expanded(
                          child: widget.requestModel.processFilePath1 != null ? processFile1 : Container(),
                        ),
                        SizedBox(
                          width: 5,
                        ),
                        Expanded(
                          child: widget.requestModel.processFilePath2 != null ? processFile2 : Container(),
                        ),
                        SizedBox(
                          width: 5,
                        ),
                        Expanded(
                          child: widget.requestModel.processFilePath3 != null ? processFile3 : Container(),
                        ),
                        SizedBox(
                          width: 5,
                        ),
                        Expanded(
                          child: widget.requestModel.processFilePath4 != null ? processFile4 : Container(),
                        ),
                      ],
                    ),
                      ],
                    ),
        ],
      ),
      actionsAlignment: MainAxisAlignment.start,
      actionsPadding: EdgeInsets.only(left: 10, right: 10, bottom: 10),
      actions: widget.requestModel.status == RequestStatus.onProgress ? null :  <Widget>[
        if(widget.requestModel.status == RequestStatus.pendingOffer)
        SizedBox(
          height: 52,
          width: MediaQuery.of(context).size.width,
          child: TurklinkButton(
            width: MediaQuery.of(context).size.width,
            backgroundColor: TurklinkColors.mainColor,
            buttonText: "request_screen.approve_button".tr(),
            borderColor: TurklinkColors.mainColor,
            textColor: Colors.white,
            btnController: requestController,
            animatedOnTap: true,
            onPressed: () async {

              if(requestMainType.key! == "VALUATION_REPORT" || requestMainType.key! == "EVICTION_REQUEST" || requestMainType.key! == "SERVICE_REQUEST")
              {
                  if(requestSubType.key == null)
                  {
                     requestController.reset();
                     requestSubTypeColor = Colors.red;
                     setState(() {

                     });
                     return;
                  }
              }
              if(requestMainType.key! == "SALES_REQUEST")
              {
                  if(saleAmountController.text == "")
                  {
                    requestController.reset();
                    saleRequestColor = Colors.red;
                    setState(() {

                    });
                    return;
                  }
              }

              if(requestMainType.key! == "CLEANING")
              {
                 if(cleaningDateController.text == "")
                 {
                   requestController.reset();
                   cleaningColor = Colors.red;
                   setState(() {

                   });
                   return;
                 }
              }

               KeyValueModel subRequestType = new KeyValueModel();
                 if(requestMainType.key! == "CLEANING")
                 {
                   subRequestType.key = "CLEANING_DAY";
                   subRequestType.value = cleaningDateController.text;
                 }
                 else if(requestMainType.key! == "SALES_REQUEST")
                 {
                   subRequestType.key = "SALES_AMOUNT";
                   subRequestType.value = saleAmountController.text;
                 }
                 else
                 {
                    subRequestType = requestSubType;
                 }

                  RequestModel requestModel = widget.requestModel;
                  requestModel.mainRequestType = requestMainType;
                  requestModel.subRequestType = subRequestType;
                  requestModel.description = serviceRequestDescriptionController.text;
                  requestModel.imageFile1 = imageFile1.imageFile;
                  requestModel.imageFile2 = imageFile2.imageFile;
                  requestModel.imageFile3 = imageFile3.imageFile;
                  requestModel.imageFile4 = imageFile4.imageFile;
                 if(requestMainType.key! == "CLEANING")
                  requestModel.requestAmount = utilityServices.getRequestAmount("CLEANING", int.parse(widget.requestModel.propertiesModel!.propertySize!));
                 if(requestMainType.key! == "VALUATION_REPORT")
                  requestModel.requestAmount = utilityServices.getRequestAmount( requestSubType.key!, null);

                  if(requestModel.mainRequestType!.key == "VALUATION_REPORT" || requestModel.mainRequestType!.key == "CLEANING")
                    requestModel.status = RequestStatus.waitingPayment;
                  else if(requestModel.mainRequestType!.key == "SERVICE_REQUEST")
                    requestModel.status = RequestStatus.pendingOffer;
                  else
                    requestModel.status = RequestStatus.onProgress;

                 var result = await requestServices.updateRequest(requestModel);
                 requestController.reset();
                 if(!result)
                 {
                  FlushbarHelper.createError(
                    title: "popup_message.popup_error_title".tr(),
                    message: requestServices.errorMessage
                  ).show(widget.parentContext);
                 }
                 else
                 {
                  Navigator.of(widget.parentContext).pop();
                  await FlushbarHelper.createSuccess(
                    title: "popup_message.popup_success_title".tr(),
                    message: "popup_message.popup_success_message".tr(),
                  ).show(widget.parentContext);
                 }
            },
          ),
        ),
        if(widget.requestModel.status == RequestStatus.waitingPayment)
        SizedBox(height: 5,),
        if(widget.requestModel.status == RequestStatus.waitingPayment)
        SizedBox(
          height: 52,
          width: MediaQuery.of(context).size.width,
          child: TurklinkButton(
            width: MediaQuery.of(context).size.width,
            backgroundColor: Colors.red,
            buttonText: "request_screen.cancel_button".tr(),
            borderColor: Colors.red,
            textColor: Colors.white,
            btnController: cancelController,
            animatedOnTap: true,
            onPressed: () async {
              var result = await  requestServices.deleteRequest(widget.requestModel);
              cancelController.reset();
              if(!result)
              {
              FlushbarHelper.createError(
                title: "popup_message.popup_error_title".tr(),
                message: requestServices.errorMessage
              ).show(widget.parentContext);
              }
              else
              {
              Navigator.of(widget.parentContext).pop();
              await FlushbarHelper.createSuccess(
                title: "popup_message.popup_success_title".tr(),
                message: "popup_message.popup_success_message".tr(),
              ).show(widget.parentContext);
              }
            },
          ),
        )
      ],
    );
  }
}