import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:turklink/models/request_model.dart';
import 'package:turklink/models/user_model.dart';
import 'package:turklink/util/finance_type.dart';
import 'package:turklink/util/request_status.dart';
import 'package:turklink/util/turklink_colors.dart';
import 'package:turklink/util/utility.dart';

class RequestListItemWidget extends StatefulWidget {
  final RequestModel requestModel;

  RequestListItemWidget({required this.requestModel});

  @override
  State<RequestListItemWidget> createState() => _RequestListItemWidgetState();
}

class _RequestListItemWidgetState extends State<RequestListItemWidget> {

  DateFormat formatter = DateFormat('dd.MM.yyyy');

  @override
  Widget build(BuildContext context) {

    Locale local = context.locale;

    return Stack(
      children: [
        Container(
          width: MediaQuery.of(context).size.width,
          padding: EdgeInsets.all(15),
           margin: EdgeInsets.only(top: 10),
          decoration: BoxDecoration(
              color: Colors.white, borderRadius: BorderRadius.circular(15)),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Column(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Text(widget.requestModel.propertiesModel!.title!,style: TextStyle(fontSize: 13,fontWeight: FontWeight.bold),),
                  SizedBox(height: 5,),
                  Container(
                    padding: EdgeInsets.only(left:15,right:15,top: 5,bottom: 5),
                    child: Text( ("request_type." + widget.requestModel.mainRequestType!.key!).tr(),style: TextStyle(color: Colors.white,fontSize: 11),),
                    decoration: BoxDecoration(
                      color: Utility.getRequestTypeColor(widget.requestModel.mainRequestType!.key!).withOpacity(.6),
                      border: Border.all(
                        width: 1,
                        color:Utility.getRequestTypeColor(widget.requestModel.mainRequestType!.key!),
                      ),
                      borderRadius: BorderRadius.circular(12)
                    ),
                  ),
                  SizedBox(height: 5,),
                  if(widget.requestModel.mainRequestType!.key! == "CLEANING" || widget.requestModel.mainRequestType!.key! == "SALES_REQUEST")
                    Text(widget.requestModel.mainRequestType!.value!,style: TextStyle(fontSize: 13,fontWeight: FontWeight.bold),)
                  else
                    Text(("request_type." + widget.requestModel.subRequestType!.key!.split("_*_")[1]).tr(),style: TextStyle(fontSize: 13,fontWeight: FontWeight.bold),),
                ],
              ),
               Column(
                 mainAxisAlignment: MainAxisAlignment.end,
                 crossAxisAlignment: CrossAxisAlignment.end,
                 children: [
                    if(widget.requestModel.status! == RequestStatus.pendingOffer)
                       Text(formatter.format(widget.requestModel.createdDate!),style: TextStyle(fontSize: 13,fontWeight: FontWeight.bold),),
                    if(widget.requestModel.status! == RequestStatus.waitingPayment)
                       SizedBox(height: 10,),
                    if(widget.requestModel.status! == RequestStatus.waitingPayment)
                      Text(Utility.convertCurrency(widget.requestModel.requestAmount!) + " TL",style: TextStyle(fontSize: 12,color: Colors.grey.shade600,fontWeight:FontWeight.bold),),
                    if(widget.requestModel.status! == RequestStatus.waitingPayment)
                     MaterialButton (
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(12)),
                        onPressed: () async{
                          var userModel = await FirebaseFirestore.instance.collection("users").doc(FirebaseAuth.instance.currentUser!.uid).get();
                          var userData  = UserModel.fromJson(userModel.data()!);

                          int type = 0;
                          if(widget.requestModel.mainRequestType!.key == "CLEANING")
                            type = FinanceType.cleaning;
                          if(widget.requestModel.mainRequestType!.key == "VALUATION_REPORT")
                            type = FinanceType.report;
                          if(widget.requestModel.mainRequestType!.key == "SERVICE_REQUEST")
                            type = FinanceType.serviceReqeust;

                          await Utility.showPaymentMethodPopup(context,userData.tryBalance,type,widget.requestModel.requestAmount!,propertyId: widget.requestModel.propertiesId,requestId: widget.requestModel.id );
                        },
                        color: TurklinkColors.mainColor,
                        child: Text(
                          "my_properties_screen.pay_property".tr(),
                          style: TextStyle(color: Colors.white),
                        ),
                      ),
                      if(widget.requestModel.status! == RequestStatus.onProgress && widget.requestModel.mainRequestType!.key == "CLEANING" && widget.requestModel.mainRequestType!.key == "VALUATION_REPORT" && widget.requestModel.mainRequestType!.key == "SERVICE_REQUEST")
                       Text("request_screen.paid_text".tr(),style: TextStyle(color: Colors.green,fontSize: 14,fontWeight: FontWeight.bold),),
                      if(widget.requestModel.status! == RequestStatus.onProgress && widget.requestModel.estimatedDate != null)
                        Text("request_screen.estimated_date".tr(args: [formatter.format(widget.requestModel.estimatedDate!)]),style: TextStyle(fontSize: 9,fontWeight: FontWeight.bold))
                 ],
               ),
            ],
          ),
        ),
        if(widget.requestModel.status! == RequestStatus.pendingOffer)
          Positioned(
            right: local.languageCode != "ar" ? 10 : null,
            left: local.languageCode == "ar" ? 10 : null,
            top: 0,
            child: Container(
            padding: EdgeInsets.only(top: 5,bottom: 5,left: 15,right: 15),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(8),
                color: Color.fromARGB(255, 240, 144, 137)
              ),
              child: Text("request_screen.pending_offer".tr(),style: TextStyle(color: Colors.black,fontSize: 10,fontWeight: FontWeight.bold),),
            ),
          ),
         if(widget.requestModel.status! == RequestStatus.waitingPayment)
          Positioned(
            right: local.languageCode != "ar" ? 10 : null,
            left: local.languageCode == "ar" ? 10 : null,
            top: 0,
            child: Container(
            padding: EdgeInsets.only(top: 5,bottom: 5,left: 15,right: 15),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(8),
                color: Color.fromARGB(255, 240, 173, 96)
              ),
              child: Text("request_screen.waiting_payment".tr(),style: TextStyle(color: Colors.black,fontSize: 10,fontWeight: FontWeight.bold),),
            ),
          ),
          if(widget.requestModel.status! == RequestStatus.onProgress)
          Positioned(
            right: local.languageCode != "ar" ? 10 : null,
            left: local.languageCode == "ar" ? 10 : null,
            top: 0,
            child: Container(
            padding: EdgeInsets.only(top: 5,bottom: 5,left: 15,right: 15),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(8),
                color: Color.fromARGB(255, 121, 196, 121)
              ),
              child: Text("request_screen.on_progress".tr(),style: TextStyle(color: Colors.black,fontSize: 10,fontWeight: FontWeight.bold),),
            ),
          ),
           if(widget.requestModel.status! == RequestStatus.closed)
          Positioned(
            right: local.languageCode != "ar" ? 10 : null,
            left: local.languageCode == "ar" ? 10 : null,
            top: 0,
            child: Container(
            padding: EdgeInsets.only(top: 5,bottom: 5,left: 15,right: 15),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(8),
                color: Color.fromARGB(255, 143, 143, 143)
              ),
              child: Text("request_screen.closed".tr(),style: TextStyle(color: Colors.black,fontSize: 10,fontWeight: FontWeight.bold),),
            ),
          ),
      ],
    );
  }
}