import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:turklink/models/properties_model.dart';
import 'package:turklink/models/request_model.dart';
import 'package:turklink/util/request_status.dart';
import 'package:turklink/util/utility.dart';
import 'package:turklink/views/pages/main/my_properties/widgets/request_widgets/request_empty_widget.dart';
import 'package:turklink/views/pages/main/my_properties/widgets/request_widgets/request_list_item_widget.dart';

class ClosedRequestsWidget extends StatefulWidget {
  final PropertiesModel propertiesModel;
  ClosedRequestsWidget(this.propertiesModel);

  @override
  State<ClosedRequestsWidget> createState() => _ClosedRequestsWidgetState();
}

class _ClosedRequestsWidgetState extends State<ClosedRequestsWidget> {
  @override
  Widget build(BuildContext context) {
    return Padding(
       padding: EdgeInsets.only(left: 15,right: 15,top: 15),
       child: Container(
         child: StreamBuilder(
             stream: FirebaseFirestore.instance
                 .collection("requests")
                 .where('propertiesId', isEqualTo: widget.propertiesModel.id)
                 .where("isDeleted", isEqualTo: 0)
                 .where("status",whereIn: [RequestStatus.closed])
                 .orderBy("updatedDate", descending: true)
                 .snapshots(),
             builder: (BuildContext context,
                 AsyncSnapshot<QuerySnapshot> snapshot) {
               if (snapshot.connectionState == ConnectionState.active) {
                 if (snapshot.hasData && snapshot.data!.docs.length > 0) {
                   return  ListView.builder(
                           itemCount: snapshot.data!.docs.length,
                           itemBuilder: (context, index) {
                             Map<String, dynamic> data =
                                 snapshot.data!.docs[index].data()!
                                     as Map<String, dynamic>;
                             var requestModel =
                                 RequestModel.fromJson(data);
                             return Container(
                               margin: EdgeInsets.only(top:7,bottom: 7),
                               child: GestureDetector(
                                 onTap: () {
                                   Utility.showRequestEditPopup(context, requestModel);
                                 },
                                 child: RequestListItemWidget(
                                   requestModel: requestModel,
                                 ),
                               ),
                             );
                           },
                         );
                 } else {
                   return RequestEmptyWidget("request_screen.empty_list".tr());
                 }
               } else {
                 return Center(
                   child: CircularProgressIndicator(),
                 );
               }
             }),
       ),
    );
  }
}