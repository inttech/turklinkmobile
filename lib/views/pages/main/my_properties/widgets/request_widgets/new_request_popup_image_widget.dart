import 'dart:io';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:turklink/util/utility.dart';

class NewRequestPopupImageWidget extends StatefulWidget {
  File? imageFile;
  String? imagePath;

  @override
  State<NewRequestPopupImageWidget> createState() =>
      _NewRequestPopupImageWidgetState();
}

class _NewRequestPopupImageWidgetState
    extends State<NewRequestPopupImageWidget> {
  @override
  Widget build(BuildContext context) {
    return Container(
      height: 80,
      child: Stack(
        children: [
          Padding(
            padding: const EdgeInsets.only(top: 10),
            child: Container(
              height: 60,
              width: double.infinity,
              decoration: BoxDecoration(
                border: Border.all(width: .5, color: Colors.black54),
                borderRadius: BorderRadius.circular(15),
              ),
              child: widget.imageFile != null || widget.imagePath != null
                  ? Padding(
                    padding: const EdgeInsets.all(5.0),
                    child: widget.imageFile != null ? Image.file(
                        widget.imageFile!,
                        fit: BoxFit.contain,
                      ) : CachedNetworkImage(
                        imageUrl: widget.imagePath!,
                        fit: BoxFit.contain,
                        placeholder: (context, url) => Padding(
                          padding: const EdgeInsets.all(15.0),
                          child: CircularProgressIndicator(
                            strokeWidth: 1,
                            value: 1,
                          ),
                        ),
                        errorWidget: (context, url, error) => Icon(Icons.error),
                      ),
                  )
                  : GestureDetector(
                    onTap: (){
                      showPicker(context);
                    },
                    child: Center(
                        child: Icon(Icons.add),
                      ),
                  ),
            ),
          ),
          if (widget.imageFile != null || widget.imagePath != null)
            Positioned(
              child: GestureDetector(
                onTap: ()
                {
                  setState(() {
                    widget.imageFile = null;
                    widget.imagePath = null;
                  });
                },
                child: Container(
                  width: 15,
                  height: 15,
                    color: Colors.white,
                    child: Icon(
                      Icons.cancel,
                      color: Colors.red,
                    )),
              ),
              right: 0,
              top: 0,
            )
        ],
      ),
    );
  }

  showPicker(context) {
    showModalBottomSheet(
        context: context,
        builder: (BuildContext bc) {
          return SafeArea(
            child: Container(
              child: new Wrap(
                children: <Widget>[
                  new ListTile(
                      leading: new Icon(Icons.photo_library),
                      title: new Text("add_property_screen.from_gallery".tr()),
                      onTap: () async {
                        var file = await Utility.imgFromGallery(context);
                        Navigator.of(context).pop();
                        setState(() {
                          widget.imageFile = file;
                        });
                      }),
                  new ListTile(
                    leading: new Icon(Icons.photo_camera),
                    title: new Text("add_property_screen.take_camera".tr()),
                    onTap: () async {
                      var file = await Utility.imgFromCamera(context);
                      Navigator.of(context).pop();
                      setState(() {
                        widget.imageFile = file;
                      });
                    },
                  ),
                ],
              ),
            ),
          );
        });
  }

}
