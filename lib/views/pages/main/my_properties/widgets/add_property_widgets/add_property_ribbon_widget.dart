import 'package:flutter/material.dart';
import 'package:turklink/views/pages/main/my_properties/widgets/add_property_widgets/add_property_triangle_widget.dart';

class AddPropertyRibbonWidget extends StatelessWidget {
  final Color color;
  final Widget notification;
  const AddPropertyRibbonWidget({required this.color,required this.notification});

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Container(
          width: 20,
          height: 20,
          color: color,
          child: Container(
                padding: EdgeInsets.only(top: 1),
                child: this.notification),
        ),
        Container(
          width: 20 ,
          height: 8,
          color: color,
          child: ClipPath(
          clipper: DrawTriangleShape(),
          child: Container(
            color: Colors.white,
            height: 6,
            width: 20,
          ),
        )
        )
      ],
    );
  }
}
