import 'package:flutter/material.dart';
import 'package:r_dotted_line_border/r_dotted_line_border.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:turklink/turklink_router.dart';

class EmptyPropertyScreen extends StatefulWidget {
  const EmptyPropertyScreen({Key? key}) : super(key: key);

  @override
  _EmptyPropertyScreenState createState() => _EmptyPropertyScreenState();
}

class _EmptyPropertyScreenState extends State<EmptyPropertyScreen> {
  @override
  Widget build(BuildContext context) {
    return Center(
      child: InkWell(
        onTap: ()
        {
          Navigator.pushNamed(context, TurklinkRouter.AddProperty);
        },
        child: Container(
          height: 200,
          padding: EdgeInsets.all(20),
          decoration: BoxDecoration(
              border: RDottedLineBorder.all(width: 1, color: Colors.grey),
              borderRadius: BorderRadius.all(Radius.circular(10)),
              color: Colors.white),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Icon(
                Icons.add,
                size: 50,
                color: Colors.grey[400],
              ),
              SizedBox(
                height: 10,
              ),
              Text(
                "my_properties_screen.has_no_property".tr(),
                style: TextStyle(fontWeight: FontWeight.w400, fontSize: 20),
                textAlign: TextAlign.center,
              ),
            ],
          ),
        ),
      ),
    );
  }
}
