import 'dart:io';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:r_dotted_line_border/r_dotted_line_border.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:turklink/util/utility.dart';

class PropertyImageWidget extends StatefulWidget {
  File? imageFile;
  String title;
  bool? isError;
  String? imagePath;
  PropertyImageWidget(this.title, this.isError);

  @override
  State<PropertyImageWidget> createState() => _PropertyImageWidgetState();
}

class _PropertyImageWidgetState extends State<PropertyImageWidget> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      width: MediaQuery.of(context).size.width,
      height: MediaQuery.of(context).size.height / 5,
      child: widget.imageFile == null && widget.imagePath == null
          ? GestureDetector(
              onTap: () {
                showPicker(context);
              },
              child: Container(
                decoration: BoxDecoration(
                    border: RDottedLineBorder.all(
                        width: 1,
                        color: widget.isError! ? Colors.red : Colors.grey),
                    borderRadius: BorderRadius.all(Radius.circular(10)),
                    color: Colors.white),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Icon(
                      Icons.image_outlined,
                      size: 50,
                      color: widget.isError! ? Colors.red : Colors.grey[400],
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    Text(
                      widget.title,
                      style: widget.isError!
                          ? TextStyle(
                              fontWeight: FontWeight.bold,
                              fontSize: 13,
                              color: Colors.red)
                          : TextStyle(
                              fontWeight: FontWeight.bold,
                              fontSize: 13,
                            ),
                      textAlign: TextAlign.center,
                    ),
                  ],
                ),
              ),
            )
          : GestureDetector(
              child: Stack(children: [
                Container(
                  width: MediaQuery.of(context).size.width,
                  height: MediaQuery.of(context).size.height / 5,
                    padding: EdgeInsets.all(10),
                    decoration: BoxDecoration(
                      color: Colors.white,
                      border:
                          RDottedLineBorder.all(width: 1, color: Colors.grey),
                      borderRadius: BorderRadius.all(Radius.circular(10)),
                    ),
                    child: widget.imageFile != null
                        ? Image.file(
                            widget.imageFile!,
                            fit: BoxFit.cover,
                          )
                        : CachedNetworkImage(
                            imageUrl: widget.imagePath!,
                            fit: BoxFit.cover,
                            placeholder: (context, url) => Padding(
                              padding: const EdgeInsets.all(15.0),
                              child: CircularProgressIndicator(
                                strokeWidth: 1,
                                value: 1,
                              ),
                            ),
                            errorWidget: (context, url, error) =>
                                Icon(Icons.error),
                          )),
                Align(
                  alignment: Alignment.topRight,
                  child: Container(
                    width: 45,
                    height: 45,
                    child: Padding(
                      padding: EdgeInsets.only(top:5,right: 5),
                      child: Center(
                        child: Icon(
                          Icons.cancel,
                          color: Colors.red.shade400,
                        ),
                      ),
                    ),
                  ),
                ),
              ]),
              onTap: () {
                setState(() {
                          widget.imageFile = null;
                          widget.isError = false;
                        });
              },
            ),
    );
  }

  showPicker(context) {
    showModalBottomSheet(
        context: context,
        builder: (BuildContext bc) {
          return SafeArea(
            child: Container(
              child: new Wrap(
                children: <Widget>[
                  new ListTile(
                      leading: new Icon(Icons.photo_library),
                      title: new Text("add_property_screen.from_gallery".tr()),
                      onTap: () async {
                        var file = await Utility.imgFromGallery(context);
                        Navigator.of(context).pop();
                        setState(() {
                          widget.imageFile = file;
                          widget.isError = false;
                        });
                      }),
                  new ListTile(
                    leading: new Icon(Icons.photo_camera),
                    title: new Text("add_property_screen.take_camera".tr()),
                    onTap: () async {
                      var file = await Utility.imgFromCamera(context);
                      Navigator.of(context).pop();
                      setState(() {
                        widget.imageFile = file;
                        widget.isError = false;
                      });
                    },
                  ),
                ],
              ),
            ),
          );
        });
  }
}
