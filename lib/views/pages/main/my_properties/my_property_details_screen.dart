import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:turklink/models/properties_model.dart';
import 'package:turklink/util/turklink_colors.dart';
import 'package:turklink/views/pages/main/appbar/home_appbar.dart';
import 'package:turklink/views/pages/main/my_properties/widgets/property_details_widgets/my_poperty_details_address_widget.dart';
import 'package:turklink/views/pages/main/my_properties/widgets/property_details_widgets/my_property_details_earthquake_insurance_widget.dart';
import 'package:turklink/views/pages/main/my_properties/widgets/property_details_widgets/my_property_details_finances_widget.dart';
import 'package:turklink/views/pages/main/my_properties/widgets/property_details_widgets/my_property_details_image_widget.dart';
import 'package:turklink/views/pages/main/my_properties/widgets/property_details_widgets/my_property_details_property_tax_widget.dart';
import 'package:turklink/views/pages/main/my_properties/widgets/property_details_widgets/my_property_details_request_widget.dart';
import 'package:turklink/views/pages/main/my_properties/widgets/property_details_widgets/my_property_rental_details_widget.dart';

class MyPropertyDetailsScreen extends StatefulWidget {
  final PropertiesModel propertiesModel;
  MyPropertyDetailsScreen(this.propertiesModel);

  @override
  _MyPropertyDetailsScreenState createState() =>
      _MyPropertyDetailsScreenState();
}

class _MyPropertyDetailsScreenState extends State<MyPropertyDetailsScreen> {


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
      automaticallyImplyLeading: true,
      backgroundColor: TurklinkColors.mainColor,
      elevation: 0,
      centerTitle: true,
      title: Text(
        widget.propertiesModel.title!,
        style: TextStyle(color: Colors.white, fontWeight: FontWeight.w400),
      ),
    ),
      backgroundColor: TurklinkColors.grayBackground,
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.only(left: 15, right: 15,),
          child: Column(
            children: [
              SizedBox(height: 15,),
              MyPropertiesDetailsImageWidget(widget.propertiesModel.imagePath!),
              SizedBox(height: 10,),
              MyPropertyDetailsAddressWidget(widget.propertiesModel),
              SizedBox(height: 10,),
              MyPropertyRentalDetailsWidget(widget.propertiesModel),
              SizedBox(height: 10,),
              MyPrpertyDetailsPropertyTaxWidget(widget.propertiesModel),
              SizedBox(height: 10,),
              MyPropertyDetailsEarthquakeInsuranceWidget(widget.propertiesModel),
              SizedBox(height: 10,),
              MyPropertyDetailsRequestWidget(widget.propertiesModel),
              SizedBox(height: 10,),
              MyPropertyDetailsFinancesWidget(widget.propertiesModel),
              SizedBox(height: 10,),
            ],
          ),
        ),
      ),
    );
  }
}
