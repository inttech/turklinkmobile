import 'package:another_flushbar/flushbar.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_statusbarcolor_ns/flutter_statusbarcolor_ns.dart';
import 'package:turklink/models/properties_model.dart';
import 'package:turklink/util/turklink_colors.dart';
import 'package:turklink/views/pages/main/my_properties/widgets/request_widgets/closed_requests_widget.dart';
import 'package:turklink/views/pages/main/my_properties/widgets/request_widgets/open_requests_widget.dart';
import 'package:turklink/views/pages/main/my_properties/widgets/request_widgets/request_menu_widget.dart';

class RequestScreen extends StatefulWidget {
  final PropertiesModel propertiesModel;
  RequestScreen(this.propertiesModel);

  @override
  State<RequestScreen> createState() => _RequestScreenState();
}

class _RequestScreenState extends State<RequestScreen> {

  int pgId = 0;

  @override
  void initState() {
    changeStatusBar();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: TurklinkColors.grayBackground,
      appBar: AppBar(
        automaticallyImplyLeading: true,
        backgroundColor: TurklinkColors.mainColor,
        elevation: 0,
        centerTitle: true,
        title: Text(
          "request_screen.request_title".tr(),
          style: TextStyle(color: Colors.white, fontWeight: FontWeight.w400),
        ),
      ),
      body: Column(
        children: [
          RequestMenuWidget(onTap: (index){
           setState(() {
             pgId = index;
           });
          },),

          if(pgId == 0)
            Expanded(child: OpenRequestsWidget(widget.propertiesModel)),
          if(pgId == 1)
            Expanded(child: ClosedRequestsWidget(widget.propertiesModel)),

        ],
      ),
    );
  }

  changeStatusBar() async {
    await FlutterStatusbarcolor.setStatusBarColor(TurklinkColors.mainColor, animate: true);
    await FlutterStatusbarcolor.setStatusBarWhiteForeground(true);
  }
}