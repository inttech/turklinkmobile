import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:turklink/models/properties_model.dart';
import 'package:turklink/turklink_router.dart';
import 'package:turklink/util/turklink_colors.dart';
import 'package:turklink/views/pages/main/my_properties/widgets/empty_property_screen_widget.dart';
import 'package:turklink/views/pages/main/my_properties/widgets/my_properties_list_item_widget.dart';
import 'package:turklink/views/widgets/turklink_empty_screen.dart';
import 'package:easy_localization/easy_localization.dart';

class MyPropertiesScreen extends StatefulWidget {
  @override
  _MyPropertiesScreenState createState() => _MyPropertiesScreenState();
}

class _MyPropertiesScreenState extends State<MyPropertiesScreen> {
  bool hasProperty = true;

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(left: 15, top: 15, right: 15),
      child: Container(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            SizedBox(
              height: 15,
            ),
            Expanded(
              child: StreamBuilder(
                  stream: FirebaseFirestore.instance
                      .collection("properties")
                      .where('userId',
                          isEqualTo: FirebaseAuth.instance.currentUser!.uid)
                      .where("isDeleted", isEqualTo: 0)
                      .orderBy("updatedDate", descending: true)
                      .snapshots(),
                  builder: (BuildContext context,
                      AsyncSnapshot<QuerySnapshot> snapshot) {
                    if (snapshot.connectionState == ConnectionState.active) {
                      if (snapshot.hasData && snapshot.data!.docs.length > 0) {
                        return Scaffold(
                          backgroundColor: TurklinkColors.grayBackground,
                          floatingActionButton: FloatingActionButton(
                            onPressed: () {
                              Navigator.of(context)
                                  .pushNamed(TurklinkRouter.AddProperty);
                            },
                            backgroundColor: TurklinkColors.mainColor,
                            child: Icon(Icons.add),
                          ),
                          body: ListView.builder(
                              itemCount: snapshot.data!.docs.length,
                              itemBuilder: (context, index) {
                                Map<String, dynamic> data =
                                    snapshot.data!.docs[index].data()!
                                        as Map<String, dynamic>;
                                var propertiyModel =
                                    PropertiesModel.fromJson(data);
                                return Container(
                                  margin: EdgeInsets.only(bottom: 15),
                                  child: GestureDetector(
                                    onTap: () {
                                      Navigator.pushNamed(context,
                                          TurklinkRouter.MyPropertyDetails);
                                    },
                                    child: MyPropertiesListItemWidget(
                                      propertiesModel: propertiyModel,
                                    ),
                                  ),
                                );
                              }),
                        );
                      } else {
                        hasProperty = false;
                        return EmptyPropertyScreen();
                      }
                    } else {
                      return Center(
                        child: CircularProgressIndicator(),
                      );
                    }
                  }),
            ),
          ],
        ),
      ),
    );
  }
}
