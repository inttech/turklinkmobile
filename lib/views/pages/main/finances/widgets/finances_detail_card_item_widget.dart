import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:turklink/util/utility.dart';

class FinancesDetailCardItemWidget extends StatelessWidget {
  final String date;
  final String title;
  final String amount;
  final Color amountColor;
  final String? invoicePath;
  const FinancesDetailCardItemWidget(
      {required this.date,
      required this.title,
      required this.amount,
      required this.amountColor,
      this.invoicePath});

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(right: 5),
      margin: EdgeInsets.only(bottom: 5),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Expanded(
            child: Text(
              this.date,
              style: TextStyle(fontSize: 11, color: Colors.grey),
            ),
          ),
          Expanded(
            child: Text(
              this.title,
              style: TextStyle(fontSize: 11, color: Colors.grey),
            ),
          ),
          Expanded(
            child: Container(
              child: GestureDetector(
                onTap: (){
                  if (this.invoicePath != null)
                  {
                     Utility.showDocumentShowDownloadPopup(context, this.invoicePath!);
                  }
                },
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: [
                    Text(
                      this.amount,
                      style: TextStyle(fontSize: 11, color: this.amountColor),
                    ),
                    if (this.invoicePath != null)
                      SizedBox(
                        width: 5,
                      ),
                    if (this.invoicePath != null)
                      Icon(FontAwesomeIcons.eye, size: 13, color: Colors.grey)
                  ],
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
