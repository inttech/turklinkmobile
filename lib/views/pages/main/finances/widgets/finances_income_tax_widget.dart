import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:turklink/util/turklink_colors.dart';
import 'package:turklink/views/pages/main/main_screen.dart';

class FinancesIncomeTaxWdiget extends StatelessWidget {
  final String? incomeTax;
  const FinancesIncomeTaxWdiget({this.incomeTax});

  @override
  Widget build(BuildContext context) {
    return Container(
      width: MediaQuery.of(context).size.width,
      padding: EdgeInsets.all(15),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(15),
        color: Colors.white,
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text("finances_screen.last_year_income".tr(),style: TextStyle(color: TurklinkColors.mainColor,fontWeight: FontWeight.bold),),
              SizedBox(height: 5,),
              Text(incomeTax!,style: TextStyle(color: Colors.grey,fontSize: 12,fontWeight: FontWeight.bold),),
            ],
          ),
        ],
      ),
    );
  }
}