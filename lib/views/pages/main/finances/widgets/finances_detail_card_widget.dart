import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:turklink/models/finances_model.dart';
import 'package:turklink/util/turklink_colors.dart';
import 'package:turklink/util/utility.dart';
import 'package:turklink/views/pages/main/finances/widgets/finances_detail_card_item_widget.dart';
import 'package:turklink/views/widgets/turklink_empty_screen.dart';
import 'package:easy_localization/easy_localization.dart';

class FinancesDetailCardWidget extends StatelessWidget {
  const FinancesDetailCardWidget();

  @override
  Widget build(BuildContext context) {
    return Container(
      width: MediaQuery.of(context).size.width,
      padding: EdgeInsets.all(15),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(15),
        color: Colors.white,
      ),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            "finances_screen.finances_details".tr(),
            style: TextStyle(
                color: TurklinkColors.mainColor, fontWeight: FontWeight.bold),
          ),
          SizedBox(
            height: 10,
          ),
          Expanded(
            child: StreamBuilder(
                stream: FirebaseFirestore.instance
                    .collection("finances")
                    .where('userId',
                        isEqualTo: FirebaseAuth.instance.currentUser!.uid)
                        .orderBy("operationDate", descending: true)
                    .snapshots(),
                builder: (BuildContext  context, AsyncSnapshot<QuerySnapshot> snapshot) {
                  if (snapshot.connectionState == ConnectionState.active) {
                    if (snapshot.hasData && snapshot.data!.docs.length > 0) {
                      return ListView.builder(
                          itemCount: snapshot.data!.docs.length,
                          itemBuilder: (context, index) {
                             Map<String, dynamic> data = snapshot.data!.docs[index].data()! as Map<String, dynamic>;
                            var financesModel = FinancesModel.fromJson(data);

                            DateFormat formatter = DateFormat('dd MMM yyyy');
                            return FinancesDetailCardItemWidget(
                              date: formatter.format(financesModel.operationDate!),
                              title: ("finances_screen." + financesModel.financeDescription!).tr(),
                              amount: (financesModel.direction == "IN" ? "+" : "-") + " " + Utility.convertCurrency(financesModel.amount!) + " TL" ,
                              amountColor: financesModel.direction == "IN" ? TurklinkColors.mainColor2 : Colors.orange,
                              invoicePath: financesModel.invoicePath,
                            );
                          });
                    } else {
                      return TurklinkEmptyScreen(
                        message: "finances_screen.has_no_property".tr(),
                      );
                    }
                  } else {
                    return Center(
                      child: CircularProgressIndicator(),
                    );
                  }
                }),
          ),
        ],
      ),
    );
  }
}
