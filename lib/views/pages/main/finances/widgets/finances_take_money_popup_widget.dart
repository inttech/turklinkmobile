import 'package:another_flushbar/flushbar_helper.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:rounded_loading_button/rounded_loading_button.dart';
import 'package:turklink/models/user_model.dart';
import 'package:turklink/services/finance_services.dart';
import 'package:turklink/util/turklink_colors.dart';
import 'package:turklink/views/widgets/turklink_button.dart';

class FinancesTakeMoneyPopupWidget extends StatefulWidget {
   final BuildContext parentContext;
  final UserModel userModel;
  FinancesTakeMoneyPopupWidget(
      {required this.parentContext, required this.userModel});

  @override
  State<FinancesTakeMoneyPopupWidget> createState() => _FinancesTakeMoneyPopupWidgetState();
}

class _FinancesTakeMoneyPopupWidgetState extends State<FinancesTakeMoneyPopupWidget> {

  var sendController = new RoundedLoadingButtonController();
  FinanceServices financeServices = new FinanceServices();


  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(12)),
      titlePadding: EdgeInsets.symmetric(vertical: 20, horizontal: 20),
      contentPadding: EdgeInsets.symmetric(vertical: 10, horizontal: 20),
      title: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Text(
            "finances_screen.take_money_title".tr(),
            style: TextStyle(fontSize: 15),
          ),
          InkWell(
            onTap: () {
              Navigator.of(widget.parentContext).pop();
            },
            child: Icon(
              Icons.cancel,
              color: Colors.red,
              size: 28,
            ),
          )
        ],
      ),
      content: Wrap(
        children: [
          Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Center(child: Text("finances_screen.approve_take_money_message".tr(args: ["TR" + widget.userModel.iban!]),textAlign: TextAlign.center,)),
            ],
          )
        ],
      ),
      actionsAlignment: MainAxisAlignment.start,
      actionsPadding: EdgeInsets.only(left: 10, right: 10, bottom: 10),
      actions: [
        SizedBox(
          height: 52,
          width: MediaQuery.of(context).size.width,
          child: TurklinkButton(
            width: MediaQuery.of(context).size.width,
            backgroundColor: TurklinkColors.mainColor,
            buttonText: "request_screen.approve_button".tr(),
            borderColor: TurklinkColors.mainColor2,
            textColor: Colors.white,
            btnController: sendController,
            animatedOnTap: true,
            onPressed: () async {

            var result = await financeServices.doTakeMoney(widget.userModel.id!);
            sendController.reset();
            if (!result) {
              Navigator.of(widget.parentContext).pop();
              FlushbarHelper.createError(
                      title: "popup_message.popup_error_title".tr(),
                      message: financeServices.errorMessage.tr())
                  .show(widget.parentContext);
            } else {
              Navigator.of(widget.parentContext).pop();
              await FlushbarHelper.createSuccess(
                title: "popup_message.popup_success_title".tr(),
                message: "popup_message.popup_success_message".tr(),
              ).show(widget.parentContext);
            }
            },
          ),
        ),
      ],
    );
  }
}