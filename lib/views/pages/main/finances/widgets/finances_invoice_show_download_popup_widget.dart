import 'dart:async';
import 'dart:io';
import 'dart:isolate';
import 'dart:ui';

import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_downloader/flutter_downloader.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:path_provider/path_provider.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:url_launcher/url_launcher.dart';

class FinancesInvoiceShowDownloadPopupWidget extends StatefulWidget {
  final String? invoicePath;
  final BuildContext parentContext;
  FinancesInvoiceShowDownloadPopupWidget(
      this.invoicePath,this.parentContext);

  @override
  State<FinancesInvoiceShowDownloadPopupWidget> createState() => _FinancesInvoiceShowDownloadPopupWidgetState();
}

class _FinancesInvoiceShowDownloadPopupWidgetState extends State<FinancesInvoiceShowDownloadPopupWidget> {

   ReceivePort _port = ReceivePort();

  @override
  void initState() {
    super.initState();

    IsolateNameServer.registerPortWithName(
        _port.sendPort, 'downloader_send_port');
    _port.listen((dynamic data) async{
      String id = data[0];
      DownloadTaskStatus status = data[1];
      int progress = data[2];
      if (status == DownloadTaskStatus.complete && Platform.isIOS) {
         await Future.delayed(const Duration(seconds: 1));
        unawaited(FlutterDownloader.open(taskId: id));
      }
      setState(() {});
    });

    FlutterDownloader.registerCallback(downloadCallback);
  }

  @override
  void dispose() {
    IsolateNameServer.removePortNameMapping('downloader_send_port');
    super.dispose();
  }

  static void downloadCallback(
      String id, DownloadTaskStatus status, int progress) {
    final SendPort send =
        IsolateNameServer.lookupPortByName('downloader_send_port')!;
    send.send([id, status, progress]);
  }

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(12)),
      titlePadding: EdgeInsets.symmetric(vertical: 20, horizontal: 20),
      contentPadding: EdgeInsets.symmetric(vertical: 10, horizontal: 20),
      title: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Text(
            "utility.document_info".tr(),
            style: TextStyle(fontSize: 15),
          ),
          InkWell(
            onTap: () {
              Navigator.of(widget.parentContext).pop();
            },
            child: Icon(
              Icons.cancel,
              color: Colors.red,
              size: 28,
            ),
          )
        ],
      ),
      content: Wrap(
        children: [
          Column(
            children: [
              ListTile(
                onTap: () async{
                   if (widget.invoicePath != null && await canLaunch(widget.invoicePath!)){
                          await launch(widget.invoicePath!);
                   }
                },
                contentPadding: EdgeInsets.zero,
                leading: Icon(FontAwesomeIcons.eye),
                title: Text("utility.document_show".tr(),style: TextStyle(fontSize: 12,fontWeight: FontWeight.bold),),
              ),
              Divider(),
              ListTile(
                 onTap: () async{
                   if (widget.invoicePath != null && Platform.isAndroid)
                      download(widget.invoicePath!);
                  else if (widget.invoicePath != null && await canLaunch(widget.invoicePath!))
                      await launch(widget.invoicePath!);
                },
                 contentPadding: EdgeInsets.zero,
                leading: Icon(Icons.download),
                title: Text("utility.document_download".tr(),style: TextStyle(fontSize: 12,fontWeight: FontWeight.bold),),

              ),
            ],
          )
        ],
      ),
    );
  }

  void download(String url) async {
    final status = await Permission.storage.request();

    if (status.isGranted) {
      final externalDir = await getDownloadPath();

      final id = await FlutterDownloader.enqueue(
          url: url,
          savedDir: externalDir!,
          showNotification: true,
          openFileFromNotification: true,
          saveInPublicStorage: true);
    }
  }


  Future<String?> getDownloadPath() async {
    Directory? directory;
    try {
      directory = await getApplicationDocumentsDirectory();
    } catch (err, stack) {
      print("Cannot get download folder path");
    }
    return directory?.path;
  }

}