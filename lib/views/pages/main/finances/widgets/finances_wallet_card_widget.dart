import 'package:another_flushbar/flushbar_helper.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:rounded_loading_button/rounded_loading_button.dart';
import 'package:turklink/models/user_model.dart';
import 'package:turklink/util/turklink_colors.dart';
import 'package:turklink/util/utility.dart';
import 'package:turklink/views/widgets/turklink_button.dart';
import 'package:turklink/views/widgets/turklink_icon_button.dart';

class FinancesWalletCardWidget extends StatelessWidget {

  final String tryBalance;
  final String usdBalance;
  final UserModel? userModel;

  FinancesWalletCardWidget({required this.tryBalance,required this.usdBalance,this.userModel});

  var btnController = new RoundedLoadingButtonController();

  @override
  Widget build(BuildContext context) {
    return Container(
      width: MediaQuery.of(context).size.width,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(15),
        color: Colors.white,
      ),
      child: Column(
        children: [
          Padding(
            padding: EdgeInsets.only(left: 15, right: 15, top: 30),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  "finances_screen.turkish_lira_balance".tr(),
                  style: TextStyle(
                      color: TurklinkColors.mainColor,
                      fontSize: 16,
                      fontWeight: FontWeight.bold),
                ),
                Column(
                  mainAxisAlignment: MainAxisAlignment.end,
                  crossAxisAlignment: CrossAxisAlignment.end,
                  children: [
                    Text(
                     this.tryBalance,
                      style: TextStyle(
                          color: Colors.grey[700],
                          fontSize: 18,
                          fontWeight: FontWeight.bold),
                    ),
                  ],
                )
              ],
            ),
          ),
          SizedBox(
            height: 10,
          ),
          Padding(
            padding: EdgeInsets.only(left: 15, right: 15),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  "finances_screen.usd_balance".tr(),
                  style: TextStyle(
                      color: TurklinkColors.mainColor,
                      fontSize: 12,
                      fontWeight: FontWeight.bold),
                ),
                Column(
                  mainAxisAlignment: MainAxisAlignment.end,
                  crossAxisAlignment: CrossAxisAlignment.end,
                  children: [
                    Text(
                      this.usdBalance,
                      style: TextStyle(
                          color: Colors.grey[700],
                          fontSize: 12,
                          fontWeight: FontWeight.bold),
                    ),
                  ],
                )
              ],
            ),
          ),
          SizedBox(
            height: 30,
          ),
          SizedBox(
            height: 50,
            width: double.infinity,
            child: TurklinkButton(
              backgroundColor: TurklinkColors.mainColor,
              buttonText: "finances_screen.transfer_to_my_bank_account".tr(),
              borderColor: TurklinkColors.mainColor,
              textColor: Colors.white,
              btnController: btnController,
              animatedOnTap: false,
              width: MediaQuery.of(context).size.width,
              onPressed: () {
                if(userModel != null)
                {
                  if(userModel!.tryBalance! <= 0)
                  {
                      FlushbarHelper.createError(
                        title: "popup_message.popup_error_title".tr(),
                        message: "finances_screen.not_enough_money_for_transfer_title".tr())
                        .show(context);
                  }
                  else
                  {
                    if(userModel!.iban != null && userModel!.iban != "")
                    {
                       Utility.showTransferBankAccountPopup(context,userModel!);
                    }
                    else
                      Utility.showChangeBankAccountPopup(context, userModel!);
                  }
                }


              },
            ),
          )
        ],
      ),
    );
  }
}
