import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:turklink/util/turklink_colors.dart';
import 'package:webview_flutter/webview_flutter.dart';

class PayTrCreditCardScreen extends StatefulWidget {
  final String creditCardPaymentId;
  const PayTrCreditCardScreen({required this.creditCardPaymentId});

  @override
  State<PayTrCreditCardScreen> createState() => _PayTrCreditCardScreenState();
}

class _PayTrCreditCardScreenState extends State<PayTrCreditCardScreen> {

  @override
   void initState() {
     super.initState();
   }

bool isLoading = true;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        automaticallyImplyLeading: true,
        backgroundColor: TurklinkColors.mainColor,
        elevation: 0,
        centerTitle: true,
        title: Text(
          "finances_screen.paytr_creditcard".tr(),
          style: TextStyle(color: Colors.white, fontWeight: FontWeight.w400),
        ),
      ),
      body: Stack(
        children: [
          WebView(
            initialUrl: 'https://us-central1-turk-link.cloudfunctions.net/main/paytr?creditCardPaymentId=' + widget.creditCardPaymentId,
            zoomEnabled: true,
            javascriptMode: JavascriptMode.unrestricted,
            onPageStarted: (started){
              setState(() {
                isLoading = false;
              });
            },
          ),
          isLoading
              ? const Center(
                  child: CircularProgressIndicator(),
                )
              : Stack(),
        ],
      ),
    );
  }
}