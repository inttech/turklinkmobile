import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:turklink/models/user_model.dart';
import 'package:turklink/services/utility_services.dart';
import 'package:turklink/util/utility.dart';
import 'package:turklink/views/pages/main/finances/widgets/finances_detail_card_widget.dart';
import 'package:turklink/views/pages/main/finances/widgets/finances_income_tax_widget.dart';
import 'package:turklink/views/pages/main/finances/widgets/finances_wallet_card_widget.dart';
import 'package:easy_localization/easy_localization.dart';

class FinancesScreen extends StatefulWidget {
  @override
  _FinancesScreenState createState() => _FinancesScreenState();
}

class _FinancesScreenState extends State<FinancesScreen> {

  double usdValue = 0;
  UtilityServices utilityServices = new UtilityServices();

  @override
  void initState() {
    getUsdValue();
    super.initState();
  }

  getUsdValue() async
  {

    var dlv = await utilityServices.getUsdValue();
    setState(() {
      usdValue = dlv;
    });
  }


  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(left: 15,right: 15,),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          SizedBox(height: 15,),
          StreamBuilder(
            stream : FirebaseFirestore.instance.collection("users").doc(FirebaseAuth.instance.currentUser!.uid).snapshots(),
            builder: (BuildContext context, AsyncSnapshot snapshot) {
              if(snapshot.hasData)
              {
                var data  = UserModel.fromJson(snapshot.data.data());
                return FinancesWalletCardWidget(
                  tryBalance: (Utility.convertCurrency(data.tryBalance!) +  " TRY"),
                  usdBalance: ("~ "+ (usdValue != 0 ? Utility.convertCurrency((data.tryBalance!/usdValue).toInt()) : Utility.convertCurrency(0))+" USD"),
                  userModel: data,
                );
              }
              return FinancesWalletCardWidget(
                tryBalance : "0 TRY",
                usdBalance : "0 USD",
                userModel: null,
              );
            },
          ),
          SizedBox(height: 15,),
          StreamBuilder(
            stream : FirebaseFirestore.instance.collection("users").doc(FirebaseAuth.instance.currentUser!.uid).snapshots(),
            builder: (BuildContext context, AsyncSnapshot snapshot) {
              if(snapshot.hasData)
              {
                var data  = UserModel.fromJson(snapshot.data.data());
                return FinancesIncomeTaxWdiget(
                  incomeTax: data.incomeTax != null && data.incomeTax! > 0 ?(Utility.convertCurrency(data.incomeTax!) +  " TRY") : "finances_screen.not_calculate_yet".tr(),
                );
              }
              return FinancesIncomeTaxWdiget(
                incomeTax : "finances_screen.not_calculate_yet".tr(),
              );
            },
          ),
          SizedBox(height: 15,),
          Expanded(child: FinancesDetailCardWidget()),
        ],
      ),
    );
  }
}
