import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter_statusbarcolor_ns/flutter_statusbarcolor_ns.dart';
import 'package:turklink/models/notification_model.dart';
import 'package:turklink/services/utility_services.dart';
import 'package:turklink/util/turklink_colors.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:turklink/views/widgets/turklink_empty_screen.dart';

class NotificationScreen extends StatefulWidget {
  @override
  _NotificationScreenState createState() => _NotificationScreenState();
}

class _NotificationScreenState extends State<NotificationScreen> {
  @override
  void initState() {
    changeStatusBar(TurklinkColors.mainColor);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {

     Locale localLang = context.locale;
     UtilityServices utilityServices = new UtilityServices();

    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        leading: IconButton(
            icon: Icon(Icons.arrow_back, color: Colors.white),
            onPressed: () async{
              await changeStatusBar(TurklinkColors.grayBackground);
              Navigator.of(context).pop();
            }),
        backgroundColor: TurklinkColors.mainColor,
        elevation: 0,
        centerTitle: true,
        title: Text(
          "notification_screen.notification_title".tr(),
          style: TextStyle(color: Colors.white, fontWeight: FontWeight.w400),
        ),
      ),
      body: Container(
        padding: EdgeInsets.only(left: 15, right: 15),
        child: StreamBuilder(
            stream: FirebaseFirestore.instance
                .collection("notifications")
                .where('userId',
                    isEqualTo: FirebaseAuth.instance.currentUser!.uid)
                .limit(15)
                .orderBy("notificationDate", descending: true)
                .snapshots(),
            builder:
                (BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot) {
              if (snapshot.connectionState == ConnectionState.active) {
                if (snapshot.hasData && snapshot.data!.docs.length > 0) {
                  return ListView.builder(
                      itemCount: snapshot.data!.docs.length,
                      itemBuilder: (context, index) {
                        Map<String, dynamic> data = snapshot.data!.docs[index]
                            .data()! as Map<String, dynamic>;
                        var notificaitonModel =
                            NotificaitonModel.fromJson(data);
                        return ListTile(
                      onTap: () async {

                      },
                      leading: Container(
                        width: 50,
                        height: 50,
                        decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(20),
                color: TurklinkColors.mainColor,
              ),
              child: Center(
                child: Text(notificaitonModel.title_ar!.substring(0,1).toUpperCase(),style: TextStyle(color: Colors.white,fontSize: 18),),
              ),
                      ),
                      contentPadding: EdgeInsets.only(top: 5, bottom: 5),
                      title: Text(
                        utilityServices.localNotificationTitle(notificaitonModel, localLang.languageCode),
                        style: TextStyle(
                            color: Colors.grey),
                      ),
                      subtitle: Text(
                        utilityServices.localNotificationMessages(notificaitonModel, localLang.languageCode),
                        style: TextStyle(
                            color: Colors.grey,fontSize: 11),
                      ),
                    );
                      });
                } else {
                  return TurklinkEmptyScreen(
                    message: "notification_screen.has_no_notification".tr(),
                  );
                }
              } else {
                return Center(
                  child: CircularProgressIndicator(),
                );
              }
            }),
      ),
    );
  }

  changeStatusBar(Color color) async {
    await FlutterStatusbarcolor.setStatusBarColor(color, animate: true);
    if (color == TurklinkColors.mainColor)
      FlutterStatusbarcolor.setStatusBarWhiteForeground(true);
    else
      FlutterStatusbarcolor.setStatusBarWhiteForeground(false);
  }
}
