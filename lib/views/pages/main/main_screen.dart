import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_statusbarcolor_ns/flutter_statusbarcolor_ns.dart';
import 'package:turklink/util/turklink_colors.dart';
import 'package:turklink/views/pages/main/appbar/home_appbar.dart';
import 'package:turklink/views/pages/main/finances/finances_screen.dart';
import 'package:turklink/views/pages/main/home/home_screen.dart';
import 'package:turklink/views/pages/main/my_properties/my_properties_screen.dart';
import 'package:turklink/views/pages/main/service_center/service_center_screen.dart';
import 'package:turklink/views/pages/main/settings/settings_screen.dart';
import 'package:turklink/views/widgets/turklink_bottom_navigation.dart';
import 'package:easy_localization/easy_localization.dart';

class MainScreen extends StatefulWidget {

  @override
  _MainScreenState createState() => _MainScreenState();
}

class _MainScreenState extends State<MainScreen> {
  int bottomNavigationIndex = 0;

  @override
  Widget build(BuildContext context) {

    
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: PreferredSize(child: getAppBar(),preferredSize: Size.fromHeight(bottomNavigationIndex == 3 ? 40 : 50)),
      body: Container(width: double.infinity,height: double.infinity, child: getBodyMain(),color: TurklinkColors.grayBackground,),
      bottomNavigationBar: TurklinkBottomNavigaiton(
        currentIndex: bottomNavigationIndex,
        onTap: (value) {
          setState(() {
            bottomNavigationIndex = value;
          });
        },
      ),
    );
  }

  Widget getAppBar() {
    var widget;
    if (bottomNavigationIndex == 0)
      widget = HomeAppBar.mainAppBar(context);
    else if (bottomNavigationIndex == 1)
      widget = HomeAppBar.titleAppBar("main_screen.my_properties_menu".tr());
    else if (bottomNavigationIndex == 2)
      widget = HomeAppBar.titleAppBar("main_screen.finances_menu".tr());
    else if (bottomNavigationIndex == 3)
      widget = HomeAppBar.titleAppBar("main_screen.settings_menu".tr());
    else if (bottomNavigationIndex == 4)
      widget = HomeAppBar.titleAppBar("main_screen.service_center_menu".tr());
    else
      widget = HomeAppBar.mainAppBar(context);

    if (bottomNavigationIndex == 0)
      changeStatusBar(TurklinkColors.grayBackground);
    else
      changeStatusBar(TurklinkColors.mainColor);

    return widget;
  }

  Widget getBodyMain() {
    if (bottomNavigationIndex == 0)
      return HomeScreen();
    else if (bottomNavigationIndex == 1)
      return MyPropertiesScreen();
    else if (bottomNavigationIndex == 2)
      return FinancesScreen();
    else if (bottomNavigationIndex == 3)
      return SettingsScreen();
    else if (bottomNavigationIndex == 4)
      return ServiceCenterScreen();
    else
      return HomeScreen();
  }

  changeStatusBar(Color color) async {
    await FlutterStatusbarcolor.setStatusBarColor(color, animate: true);
    FlutterStatusbarcolor.setNavigationBarWhiteForeground(true);
    FlutterStatusbarcolor.setNavigationBarColor(Colors.white);
    if(color == TurklinkColors.mainColor)
      FlutterStatusbarcolor.setStatusBarWhiteForeground(true);
    else
      FlutterStatusbarcolor.setStatusBarWhiteForeground(false);
  }
}
