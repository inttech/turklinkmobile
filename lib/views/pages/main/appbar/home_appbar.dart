import 'package:flutter/material.dart';
import 'package:turklink/turklink_router.dart';
import 'package:turklink/util/turklink_colors.dart';

class HomeAppBar {

static mainAppBar(BuildContext context)
{
  return AppBar(
        automaticallyImplyLeading: false,
        backgroundColor: TurklinkColors.grayBackground,
        elevation: 0,
        title: Image.asset(
          "assets/images/logo.png",
          height: 40,
        ),
        centerTitle: false,
        actions: [
          IconButton(
              icon: Icon(
                Icons.notifications_outlined,
                color: TurklinkColors.mainColor,
              ),
              onPressed: () {
                Navigator.pushNamed(context, TurklinkRouter.Notification);
              }),
        ],
  );
}

static titleAppBar(String title)
{
  return AppBar(
      automaticallyImplyLeading: false,
      backgroundColor: TurklinkColors.mainColor,
      elevation: 0,
      centerTitle: true,
      title: Text(
        title,
        style: TextStyle(color: Colors.white, fontWeight: FontWeight.w400),
      ),
    );
}

}
