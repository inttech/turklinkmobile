import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter_statusbarcolor_ns/flutter_statusbarcolor_ns.dart';
import 'package:turklink/models/user_model.dart';
import 'package:turklink/util/turklink_colors.dart';
import 'package:turklink/views/pages/main/settings/widgets/settings_profile_info_widget.dart';

class ProfileSettingsScreen extends StatefulWidget {
  const ProfileSettingsScreen({ Key? key }) : super(key: key);

  @override
  State<ProfileSettingsScreen> createState() => _ProfileSettingsScreenState();
}

changeStatusColor() async
{
  await FlutterStatusbarcolor.setStatusBarColor(TurklinkColors.mainColor, animate: true);
  FlutterStatusbarcolor.setStatusBarWhiteForeground(true);
}

class _ProfileSettingsScreenState extends State<ProfileSettingsScreen> {

  @override
  void initState() {
    changeStatusColor();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        automaticallyImplyLeading: true,
        backgroundColor: TurklinkColors.mainColor,
        elevation: 0,
        centerTitle: true,
        title: Text(
          "setting_screen.profile_update_title".tr(),
          style: TextStyle(color: Colors.white, fontWeight: FontWeight.w400),
        ),

      ),
      backgroundColor: TurklinkColors.grayBackground,
      body: SingleChildScrollView(
        child: Column(
          children: [
            Container(
            padding: EdgeInsets.only(top: 25, bottom: 25),
            child: StreamBuilder(
                  stream : FirebaseFirestore.instance.collection("users").doc(FirebaseAuth.instance.currentUser!.uid).snapshots(),
                  builder: (BuildContext  context, AsyncSnapshot<DocumentSnapshot> snapshot) {

                    if(snapshot.hasData && snapshot.connectionState == ConnectionState.active)
                    {
                      Map<String, dynamic> dataMap = snapshot.data!.data()! as Map<String, dynamic>;
                      var data  = UserModel.fromJson(dataMap);
                      return SettingsProfileInfoWidget(userModel: data,isEditProfile: true,);
                    }

                    return SettingsProfileInfoWidget(userModel: null,isEditProfile: true,isLoading: true,);
                  },
            ),
          )
          ],
        ),
      ),
    );
  }
}