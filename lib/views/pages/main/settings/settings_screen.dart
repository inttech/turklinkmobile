import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:turklink/models/user_model.dart';
import 'package:turklink/services/user_services.dart';
import 'package:turklink/services/utility_services.dart';
import 'package:turklink/turklink_router.dart';
import 'package:turklink/util/turklink_colors.dart';
import 'package:turklink/util/utility.dart';
import 'package:turklink/views/pages/main/settings/profile_settings_screen.dart';
import 'package:turklink/views/pages/main/settings/widgets/settings_profile_info_widget.dart';


class SettingsScreen extends StatefulWidget {
  @override
  _SettingsScreenState createState() => _SettingsScreenState();
}

class _SettingsScreenState extends State<SettingsScreen> {

  UserServices userServices = new UserServices();
  bool isNotification = true;
  UserModel? userModel;

  @override
  void initState() {
    FirebaseFirestore.instance.collection("users").doc(FirebaseAuth.instance.currentUser!.uid).get().then((data) {
      if(data.exists)
      {
         userModel  = UserModel.fromJson(data.data()!);
         if(userModel != null && userModel!.isShowNotification != null)
          {
            setState(() {
               isNotification = userModel!.isShowNotification!;
            });
          }
      }
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.white,
      height: double.infinity,
      child: SingleChildScrollView(
        child: Column(
          children: [
            Container(
              padding: EdgeInsets.only(top: 25, bottom: 25),
              color: TurklinkColors.mainColor,
              child: StreamBuilder(
                    stream : FirebaseFirestore.instance.collection("users").doc(FirebaseAuth.instance.currentUser!.uid).snapshots(),
                    builder: (BuildContext  context, AsyncSnapshot<DocumentSnapshot> snapshot) {

                      if(snapshot.hasData)
                      {
                        Map<String, dynamic> dataMap = snapshot.data!.data()! as Map<String, dynamic>;
                        var data  = UserModel.fromJson(dataMap);
                        return SettingsProfileInfoWidget(userModel: data,);
                      }

                      return SettingsProfileInfoWidget(userModel: null,isLoading: true,);
                    },
              ),
            ),
            Container(
              width: double.infinity,
              color: Colors.white,
              child: ListTile(
                onTap: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => ProfileSettingsScreen(),
                    ),
                  );
                },
                leading: Icon(Icons.person),
                title: Text("setting_screen.profile_settings".tr()),
                subtitle: Text(
                  "setting_screen.profile_info_text".tr(),
                  style: TextStyle(fontSize: 11),
                ),
              ),
            ),
           if(userModel != null && (userModel!.citizenImagePath == null || userModel!.citizenImagePath == "" || userModel!.verificationStatus == 2 ) && (userModel!.isVerificationUser == null || !userModel!.isVerificationUser!))
            Container(
              width: double.infinity,
              color: Colors.white,
              child: ListTile(
                onTap: () {
                  if(userModel != null)
                    Utility.showApproveCitizenPopup(context, userModel!);
                },
                leading: Icon(Icons.check_box_rounded),
                title: Text("setting_screen.user_verification_settings".tr()),
                subtitle: Text(
                  "setting_screen.user_verification_text".tr(),
                  style: TextStyle(fontSize: 11),
                ),
              ),
            ),
            if(userModel != null && (userModel!.isVerificationUser != null && userModel!.isVerificationUser!))
            Container(
              width: double.infinity,
              color: Colors.white,
              child: ListTile(
                leading: Icon(Icons.check_box_rounded,color: TurklinkColors.mainColor2,),
                title: Text("setting_screen.user_verification_settings".tr()),
                subtitle: Text(
                  "setting_screen.user_verification_approve_text".tr(),
                  style: TextStyle(fontSize: 11),
                ),
              ),
            ),
            if(userModel != null && userModel!.citizenImagePath != null && userModel!.citizenImagePath != "" && (userModel!.isVerificationUser != null && !userModel!.isVerificationUser! && userModel!.verificationStatus == 0))
            Container(
              width: double.infinity,
              color: Colors.white,
              child: ListTile(
                onTap: () {
                },
                leading: Icon(Icons.lock_clock,color: Colors.orange,),
                title: Text("setting_screen.user_verification_settings".tr()),
                subtitle: Text(
                  "setting_screen.user_verification_waiting_text".tr(),
                  style: TextStyle(fontSize: 11),
                ),
              ),
            ),
            Container(
              width: double.infinity,
              color: Colors.white,
              child: ListTile(
                onTap: (){
                  Utility.showResetPasswordPopup(context);
                },
                leading: Icon(Icons.password),
                title: Text("setting_screen.password_settings".tr()),
                subtitle: Text(
                  "setting_screen.password_info_text".tr(),
                  style: TextStyle(fontSize: 11),
                ),
              ),
            ),

            Container(
              width: double.infinity,
              color: Colors.white,
              child: ListTile(
                onTap: (){
                  if(userModel != null)
                    Utility.showChangeBankAccountPopup(context,this.userModel!);
                },
                leading: Icon(Icons.account_balance_wallet),
                title: Text("setting_screen.bank_account_settings".tr()),
                subtitle: Text(
                  "setting_screen.bank_account_info_text".tr(),
                  style: TextStyle(fontSize: 11),
                ),
              ),
            ),

            Container(
              width: double.infinity,
              color: Colors.white,
              child: ListTile(
                onTap: (){
                  if(userModel != null)
                    Utility.showChangeLangPopup(context,this.userModel!);
                },
                leading: Icon(Icons.language),
                title: Text("setting_screen.lang_settings".tr()),
                subtitle: Text(
                  "setting_screen.lang_info_text".tr(),
                  style: TextStyle(fontSize: 11),
                ),
              ),
            ),

            Container(
              width: double.infinity,
              color: Colors.white,
              child: ListTile(
                leading: Icon(Icons.notifications),
                title: Text("setting_screen.notification_settings".tr()),
                subtitle: Text(
                  "setting_screen.notification_info_text".tr(),
                  style: TextStyle(fontSize: 11),
                ),
                trailing: CupertinoSwitch(
                  value: isNotification,
                  activeColor: TurklinkColors.mainColor,
                  onChanged: (value) async{
                    if(userModel != null)
                    {
                        userModel!.isShowNotification = value;
                        userServices.updateUser(userModel!);

                         setState(() {
                            isNotification = value;
                          });
                    }
                  },
                ),
              ),
            ),
            Container(
              width: double.infinity,
              color: Colors.white,
              child: ListTile(
                leading: Icon(Icons.remove_done_outlined),
                title: Text("setting_screen.remove_user_settings".tr()),
                subtitle: Text(
                  "setting_screen.remove_user_info_text".tr(),
                  style: TextStyle(fontSize: 11),
                ),
                onTap: () async {
                  if(userModel != null)
                    Utility.showRemoveUserAccountPopup(context,this.userModel!);
                },
              ),
            ),
            Container(
              width: double.infinity,
              color: Colors.white,
              child: ListTile(
                leading: Icon(Icons.exit_to_app),
                title: Text("setting_screen.logout".tr()),
                subtitle: Text(
                  "setting_screen.logout_info_text".tr(),
                  style: TextStyle(fontSize: 11),
                ),
                onTap: () async {
                  await UserServices().logout();
                  Navigator.pushReplacementNamed(
                        context, TurklinkRouter.Splash);
                },
              ),
            ),
          ],
        ),
      ),
    );
  }
}