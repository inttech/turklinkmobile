import 'package:another_flushbar/flushbar_helper.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:rounded_loading_button/rounded_loading_button.dart';
import 'package:turklink/models/user_model.dart';
import 'package:turklink/services/user_services.dart';
import 'package:turklink/turklink_router.dart';
import 'package:turklink/util/turklink_colors.dart';
import 'package:turklink/views/widgets/turklink_button.dart';

class SettingsRemoveUserAccountPopupWidget extends StatefulWidget {
  final BuildContext parentContext;
  final UserModel userModel;
  SettingsRemoveUserAccountPopupWidget(
      {required this.parentContext, required this.userModel});

  @override
  State<SettingsRemoveUserAccountPopupWidget> createState() => _SettingsRemoveUserAccountPopupWidgetState();
}

class _SettingsRemoveUserAccountPopupWidgetState extends State<SettingsRemoveUserAccountPopupWidget> {
  
  var sendController = new RoundedLoadingButtonController();
  UserServices userServices = new UserServices();

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(12)),
      titlePadding: EdgeInsets.symmetric(vertical: 20, horizontal: 20),
      contentPadding: EdgeInsets.symmetric(vertical: 10, horizontal: 20),
      title: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Text(
            "setting_screen.remove_user_settings".tr(),
            style: TextStyle(fontSize: 15),
          ),
          InkWell(
            onTap: () {
              Navigator.of(widget.parentContext).pop();
            },
            child: Icon(
              Icons.cancel,
              color: Colors.red,
              size: 28,
            ),
          )
        ],
      ),
      content: Wrap(
        children: [
          Container(
            child: Center(
              child: Text(
                "setting_screen.remove_user_info_description"
                    .tr(),style: TextStyle(fontSize: 13),textAlign: TextAlign.center,
              ),
            ),
          )
        ],
      ),
      actionsAlignment: MainAxisAlignment.start,
      actionsPadding: EdgeInsets.only(left: 10, right: 10, bottom: 10),
      actions: [
        SizedBox(
          height: 52,
          width: MediaQuery.of(context).size.width,
          child: TurklinkButton(
            width: MediaQuery.of(context).size.width,
            backgroundColor: TurklinkColors.mainColor,
            buttonText: "setting_screen.approve_remove_user_button".tr(),
            borderColor: TurklinkColors.mainColor2,
            textColor: Colors.white,
            btnController: sendController,
            animatedOnTap: true,
            onPressed: () async {
              var result = await userServices
                  .delete();
              sendController.reset();
              if (!result) {
                FlushbarHelper.createError(
                        title: "popup_message.popup_error_title".tr(),
                        message: userServices.errorMessage)
                    .show(widget.parentContext);
              } else {
                await UserServices().logout();
                  Navigator.pushReplacementNamed(
                        context, TurklinkRouter.Splash);
                await FlushbarHelper.createSuccess(
                  title: "popup_message.popup_success_title".tr(),
                  message: "popup_message.popup_success_message".tr(),
                ).show(widget.parentContext);
              }
            },
          ),
        ),
      ],
    );
  }

}