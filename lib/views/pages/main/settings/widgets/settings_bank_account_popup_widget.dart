import 'package:another_flushbar/flushbar_helper.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:mask_text_input_formatter/mask_text_input_formatter.dart';
import 'package:rounded_loading_button/rounded_loading_button.dart';
import 'package:turklink/models/user_model.dart';
import 'package:turklink/services/user_services.dart';
import 'package:turklink/util/turklink_colors.dart';
import 'package:turklink/views/widgets/turklink_button.dart';

class SettingsBankAccountPopupWidget extends StatefulWidget {
  final BuildContext parentContext;
  final UserModel userModel;
  SettingsBankAccountPopupWidget(
      {required this.parentContext, required this.userModel});

  @override
  State<SettingsBankAccountPopupWidget> createState() =>
      _SettingsBankAccountPopupWidgetState();
}

class _SettingsBankAccountPopupWidgetState
    extends State<SettingsBankAccountPopupWidget> {
  var sendController = new RoundedLoadingButtonController();
  UserServices userServices = new UserServices();

  var maskFormatter = new MaskTextInputFormatter(
    mask: '## #### #### #### #### #### ##',
    filter: { "#": RegExp(r'[0-9]') },
    type: MaskAutoCompletionType.lazy
  );

  var ibanController = TextEditingController();
   String? ibanErrorMessage;

  @override
  Widget build(BuildContext context) {

    if(widget.userModel.iban != null)
      ibanController.text = widget.userModel.iban!;


    return AlertDialog(
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(12)),
      titlePadding: EdgeInsets.symmetric(vertical: 20, horizontal: 20),
      contentPadding: EdgeInsets.symmetric(vertical: 10, horizontal: 20),
      title: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Text(
            "setting_screen.bank_account_change".tr(),
            style: TextStyle(fontSize: 15),
          ),
          InkWell(
            onTap: () {
              Navigator.of(widget.parentContext).pop();
            },
            child: Icon(
              Icons.cancel,
              color: Colors.red,
              size: 28,
            ),
          )
        ],
      ),
      content: Wrap(
        children: [
          Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Center(child: Text("setting_screen.bank_account_top_text".tr(),textAlign: TextAlign.center,)),
              SizedBox(height: 15,),
              Container(
                child: TextField(
                  inputFormatters: [maskFormatter],
                  keyboardType: TextInputType.number,
                  controller: this.ibanController,
                  style: TextStyle(height: 1,fontSize: 14),
                  decoration: InputDecoration(
                    prefix: Text("TR"),
                    errorText: ibanErrorMessage,
                      errorStyle: TextStyle(color: Colors.red.withOpacity(.7)),
                      focusedErrorBorder: OutlineInputBorder(
                          gapPadding: 0,
                          borderSide: BorderSide(
                            color: Colors.red.withOpacity(.7),
                          )),
                      errorBorder: OutlineInputBorder(
                          gapPadding: 0,
                          borderSide: BorderSide(
                            color: Colors.red.withOpacity(.7),
                          )),
                      filled: true,
                      fillColor: Colors.white,
                      labelText: "setting_screen.iban_text".tr(),
                      focusedBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(12),
                        borderSide: BorderSide(
                          color: TurklinkColors.mainColor,
                          width: 2.0,
                        ),
                      ),
                      border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(12),
                        borderSide: BorderSide(
                          color: TurklinkColors.mainColor,
                          width: 2.0,
                        ),
                      ),
                      labelStyle: TextStyle(color: Colors.black54)),
                ),
              ),
            ],
          )
        ],
      ),
      actionsAlignment: MainAxisAlignment.start,
      actionsPadding: EdgeInsets.only(left: 10, right: 10, bottom: 10),
      actions: [
        SizedBox(
          height: 52,
          width: MediaQuery.of(context).size.width,
          child: TurklinkButton(
            width: MediaQuery.of(context).size.width,
            backgroundColor: TurklinkColors.mainColor,
            buttonText: "setting_screen.send_email_button".tr(),
            borderColor: TurklinkColors.mainColor2,
            textColor: Colors.white,
            btnController: sendController,
            animatedOnTap: true,
            onPressed: () async {

             if(this.ibanController.text.isEmpty)
             {
               sendController.reset();
               setState(() {
                 ibanErrorMessage = "error_messages.iban_empty".tr();
               });
               return;
             }

            UserModel userModelReq = widget.userModel;
            userModelReq.iban = this.ibanController.text;

            var result = await userServices.updateUser(userModelReq);
            sendController.reset();
            if (!result) {
              FlushbarHelper.createError(
                      title: "popup_message.popup_error_title".tr(),
                      message: userServices.errorMessage)
                  .show(widget.parentContext);
            } else {
              Navigator.of(widget.parentContext).pop();
              await FlushbarHelper.createSuccess(
                title: "popup_message.popup_success_title".tr(),
                message: "popup_message.popup_success_message".tr(),
              ).show(widget.parentContext);
            }
            },
          ),
        ),
      ],
    );
  }
}
