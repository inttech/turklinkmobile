import 'dart:io';

import 'package:another_flushbar/flushbar_helper.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:r_dotted_line_border/r_dotted_line_border.dart';
import 'package:rounded_loading_button/rounded_loading_button.dart';
import 'package:turklink/models/user_model.dart';
import 'package:turklink/services/user_services.dart';
import 'package:turklink/util/turklink_colors.dart';
import 'package:turklink/util/utility.dart';
import 'package:turklink/views/widgets/turklink_button.dart';

class SettingsApproveCitizenPopupWidget extends StatefulWidget {
  final BuildContext parentContext;
  final UserModel userModel;
  SettingsApproveCitizenPopupWidget(
      {required this.parentContext, required this.userModel});

  @override
  State<SettingsApproveCitizenPopupWidget> createState() =>
      _SettingsApproveCitizenPopupWidgetState();
}

class _SettingsApproveCitizenPopupWidgetState
    extends State<SettingsApproveCitizenPopupWidget> {
  var sendController = new RoundedLoadingButtonController();
  File? imageFile;
  bool isError = false;

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(12)),
      titlePadding: EdgeInsets.symmetric(vertical: 20, horizontal: 20),
      contentPadding: EdgeInsets.symmetric(vertical: 10, horizontal: 20),
      title: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Text(
            "setting_screen.user_citizen_verify_text_small".tr(),
            style: TextStyle(fontSize: 15),
          ),
          InkWell(
            onTap: () {
              Navigator.of(widget.parentContext).pop();
            },
            child: Icon(
              Icons.cancel,
              color: Colors.red,
              size: 28,
            ),
          )
        ],
      ),
      content: SingleChildScrollView(
        child: 
          Column(
            children: [
              Center(
                child: Text("setting_screen.user_citizen_verify_text".tr()),
              ),
              SizedBox(
                height: 10,
              ),
              Container(
                width: MediaQuery.of(context).size.width,
                height: MediaQuery.of(context).size.height / 5,
                child: GestureDetector(
                  child: this.imageFile == null
                      ? Container(
                          decoration: BoxDecoration(
                              border: RDottedLineBorder.all(
                                  width: 1, color: Colors.grey),
                              borderRadius:
                                  BorderRadius.all(Radius.circular(10)),
                              color: Colors.white),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Icon(
                                Icons.image_outlined,
                                size: 50,
                                color: this.isError
                                    ? Colors.red
                                    : Colors.grey[400],
                              ),
                              SizedBox(
                                height: 10,
                              ),
                              Text(
                                "setting_screen.user_citizen_verify_text_small"
                                    .tr(),
                                style: this.isError
                                    ? TextStyle(
                                        fontWeight: FontWeight.bold,
                                        fontSize: 13,
                                        color: Colors.red)
                                    : TextStyle(
                                        fontWeight: FontWeight.bold,
                                        fontSize: 13,
                                      ),
                                textAlign: TextAlign.center,
                              ),
                            ],
                          ),
                        )
                      : Container(
                          padding: EdgeInsets.all(10),
                          decoration: BoxDecoration(
                            color: Colors.white,
                            border: RDottedLineBorder.all(
                                width: 1, color: Colors.grey),
                            borderRadius: BorderRadius.all(Radius.circular(10)),
                          ),
                          child: Image.file(
                            this.imageFile!,
                            fit: BoxFit.fill,
                          ),
                        ),
                  onTap: () {
                    showPicker(context);
                  },
                ),
              )
            ],
          )
        ,
      ),
      actionsAlignment: MainAxisAlignment.start,
      actionsPadding: EdgeInsets.only(left: 10, right: 10, bottom: 10),
      actions: [
        TurklinkButton(
          width: MediaQuery.of(context).size.width,
          backgroundColor: TurklinkColors.mainColor,
          buttonText: "setting_screen.send_email_button".tr(),
          borderColor: TurklinkColors.mainColor2,
          textColor: Colors.white,
          btnController: sendController,
          animatedOnTap: true,
          onPressed: () async {
            if(this.imageFile == null)
            {
              this.isError = true;
              sendController.reset();
              setState(() {});
              return;
            }

            UserModel userModelReq = widget.userModel;
            userModelReq.passportFile = this.imageFile;
            UserServices userServices = new UserServices();
            var result = await userServices.updateUser(userModelReq);
            sendController.reset();
            if (!result) {
              FlushbarHelper.createError(
                      title: "popup_message.popup_error_title".tr(),
                      message: userServices.errorMessage)
                  .show(widget.parentContext);
            } else {
              Navigator.of(widget.parentContext).pop();
              await FlushbarHelper.createSuccess(
                title: "popup_message.popup_success_title".tr(),
                message: "popup_message.popup_success_message".tr(),
              ).show(widget.parentContext);
            }

          },
        ),
      ],
    );
  }

  showPicker(context) {
    showModalBottomSheet(
        context: context,
        builder: (BuildContext bc) {
          return SafeArea(
            child: Container(
              child: new Wrap(
                children: <Widget>[
                  new ListTile(
                      leading: new Icon(Icons.photo_library),
                      title: new Text("add_property_screen.from_gallery".tr()),
                      onTap: () async {
                        var file = await Utility.imgFromGallery(context);
                        Navigator.of(context).pop();
                        setState(() {
                          this.imageFile = file;
                        });
                      }),
                  new ListTile(
                    leading: new Icon(Icons.photo_camera),
                    title: new Text("add_property_screen.take_camera".tr()),
                    onTap: () async {
                      var file = await Utility.imgFromCamera(context);
                      Navigator.of(context).pop();
                      setState(() {
                        this.imageFile = file;
                      });
                    },
                  ),
                ],
              ),
            ),
          );
        });
  }
}
