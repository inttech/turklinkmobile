import 'dart:io';

import 'package:another_flushbar/flushbar_helper.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_otp_text_field/flutter_otp_text_field.dart';
import 'package:rounded_loading_button/rounded_loading_button.dart';
import 'package:timer_count_down/timer_count_down.dart';
import 'package:turklink/models/user_model.dart';
import 'package:turklink/services/user_services.dart';
import 'package:turklink/util/turklink_colors.dart';
import 'package:turklink/util/utility.dart';
import 'package:turklink/views/widgets/turklink_button.dart';
import 'package:turklink/views/widgets/turklink_phone_textfield.dart';
import 'package:turklink/views/widgets/turklink_textfield.dart';

class SettingsProfileInfoWidget extends StatefulWidget {
  final UserModel? userModel;
  final bool? isEditProfile;
  final bool? isLoading;
  const SettingsProfileInfoWidget(
      {this.userModel, this.isEditProfile, this.isLoading});

  @override
  State<SettingsProfileInfoWidget> createState() =>
      _SettingsProfileInfoWidgetState();
}

class _SettingsProfileInfoWidgetState extends State<SettingsProfileInfoWidget> {
  var fullNameController = TextEditingController();
  var btnController = new RoundedLoadingButtonController();
  var phoneBtnController = new RoundedLoadingButtonController();

  File? imageFile;
  UserServices userServices = new UserServices();
  bool isFirstCall = true;

  var phoneController = TextEditingController();

  String? nameErrorMessage;
  String? phoneErrorMessage;

  var phoneChangeValue = "";
  String phoneRequestId = "";
  String phoneVerificationCode = "";
  bool phoneVerifiactionVisible = false;
  String? initialCountryCode;
  bool? isChangePhoneNumber = false;

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    if (widget.isEditProfile != null &&
        widget.userModel != null &&
        isFirstCall) {
      fullNameController.text = widget.userModel!.name!;
      isFirstCall = false;
      initialCountryCode = widget.userModel!.initialCountryCode != null
          ? widget.userModel!.initialCountryCode!
          : "TR";
      phoneController.text = widget.userModel!.phone!;
      phoneChangeValue = widget.userModel!.phone!;
    }

    return Column(
      children: [
        Center(
          child: Container(
            width: 100,
            height: 100,
            decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(15), color: Colors.white),
            child: (widget.userModel != null &&
                        widget.userModel!.imagePath != null) ||
                    this.imageFile != null
                ? ClipRRect(
                    borderRadius: BorderRadius.circular(15),
                    child: this.imageFile == null
                        ? CachedNetworkImage(
                            imageUrl: widget.userModel!.imagePath!,
                            fit: BoxFit.cover,
                            placeholder: (context, url) => Padding(
                              padding: const EdgeInsets.all(15.0),
                              child: CircularProgressIndicator(
                                strokeWidth: 1,
                                value: 1,
                              ),
                            ),
                            errorWidget: (context, url, error) =>
                                Icon(Icons.error),
                          )
                        : ClipRRect(
                            borderRadius: BorderRadius.circular(15),
                            child: Image.file(
                              this.imageFile!,
                              fit: BoxFit.cover,
                            ),
                          ),
                  )
                : (widget.isLoading != null
                    ? Container()
                    : ClipRRect(
                        borderRadius: BorderRadius.circular(15),
                        child: Image(
                          image: AssetImage(
                            "assets/images/user.png",
                          ),
                        ),
                      )),
          ),
        ),
        SizedBox(
          height: 20,
        ),
        if (widget.isEditProfile == null)
          Center(
            child: Text(
              widget.userModel != null ? widget.userModel!.name! : "",
              style: TextStyle(color: Colors.white, fontSize: 17),
            ),
          ),
        if (widget.isEditProfile != null)
          Padding(
            padding: const EdgeInsets.only(left: 20, right: 20),
            child: Column(
              children: [
                GestureDetector(
                  onTap: () {
                    showPicker(context);
                  },
                  child: Center(
                    child: Text(
                      "setting_screen.profile_update_profile_image".tr(),
                      style: TextStyle(color: Colors.blue, fontSize: 17),
                    ),
                  ),
                ),
                SizedBox(
                  height: 40,
                ),
                AbsorbPointer(
                  absorbing: widget.userModel != null &&
                      widget.userModel!.isVerificationUser != null &&
                      widget.userModel!.isVerificationUser!,
                  child: TurklinkTextField(
                    labelText: "register_screen.name_surname_input".tr(),
                    controller: fullNameController,
                    errorText: nameErrorMessage,
                  ),
                ),
                SizedBox(
                  height: 20,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Expanded(
                      child: TurklinkPhoneTextFiled(
                        controller: phoneController,
                        errorText: phoneErrorMessage,
                        initialCountryCode: initialCountryCode,
                        phoneNumberValueChange: (phone) {
                          phoneChangeValue = phone.completeNumber;
                          initialCountryCode = phone.countryCode;
                          isChangePhoneNumber = true;
                          setState(() {});
                        },
                      ),
                    ),
                    if(isChangePhoneNumber!)
                    SizedBox(
                      width: 10,
                    ),
                    if(isChangePhoneNumber!)
                    SizedBox(
                      width: 60,
                      child: !phoneVerifiactionVisible
                          ? TurklinkButton(
                              width: 60,
                              backgroundColor: TurklinkColors.mainColor,
                              buttonText: "register_screen.approve_button".tr(),
                              fontSize: 10,
                              borderColor: TurklinkColors.mainColor,
                              textColor: Colors.white,
                              btnController: phoneBtnController,
                              onPressed: () async {
                                var result = await userServices
                                    .sendOtpSms(phoneChangeValue);
                                phoneBtnController.reset();
                                if (result != null) {
                                  setState(() {
                                    phoneVerifiactionVisible = true;
                                    phoneRequestId = result;
                                  });
                                }
                              },
                            )
                          : Center(
                              child: Countdown(
                                seconds: 300,
                                build: (BuildContext context, double time) {
                                  var minutes =
                                      '${(Duration(seconds: time.toInt()))}'
                                          .split('.')[0]
                                          .padLeft(8, '0')
                                          .split(":");
                                  return Text(
                                    minutes[1] + ":" + minutes[2],
                                    style: TextStyle(
                                        fontSize: 16,
                                        color: Colors.red.shade700),
                                  );
                                },
                                interval: Duration(seconds: 1),
                                onFinished: () {
                                  setState(() {
                                    phoneVerifiactionVisible = false;
                                  });
                                },
                              ),
                            ),
                    ),
                  ],
                ),
                Visibility(
                  visible: phoneVerifiactionVisible,
                  child: Column(
                    children: [
                      SizedBox(
                        height: 25,
                      ),
                      OtpTextField(
                        numberOfFields: 4,
                        borderColor: TurklinkColors.mainColor,
                        fillColor: Colors.white,
                        filled: true,
                        disabledBorderColor: Colors.white,
                        enabledBorderColor: Colors.white,
                        borderRadius: BorderRadius.circular(12),
                        textStyle: TextStyle(fontSize: 16),
                        showFieldAsBox: true,
                        fieldWidth: 80,
                        onCodeChanged: (String code) {},
                        onSubmit: (String verificationCode) {
                          phoneVerificationCode = verificationCode;
                        },
                      ),
                    ],
                  ),
                ),
                SizedBox(
                  height: 20,
                ),
                Container(
                  height: 52,
                  width: double.infinity,
                  child: TurklinkButton(
                    width: MediaQuery.of(context).size.width,
                    backgroundColor: TurklinkColors.mainColor,
                    buttonText: "setting_screen.save_button".tr(),
                    borderColor: TurklinkColors.mainColor,
                    textColor: Colors.white,
                    animatedOnTap: true,
                    btnController: btnController,
                    onPressed: () async {
                      FocusScope.of(context).unfocus();
                      if (fullNameController.text.isEmpty) {
                        setState(() {
                          nameErrorMessage = "error_messages.register_name_empty".tr();
                        });
                        btnController.reset();
                        return;
                      }

                      if(phoneController.text.isEmpty)
                      {
                        setState(() {
                          phoneErrorMessage = "error_messages.register_phoneNumber_empty".tr();
                        });
                        btnController.reset();
                        return;
                      }

                      if(isChangePhoneNumber! && phoneVerificationCode.isEmpty)
                      {
                        setState(() {
                          phoneErrorMessage = "error_messages.register_phoneVerification_empty".tr();
                        });
                        btnController.reset();
                        return;
                      }

                      if(isChangePhoneNumber! && phoneVerificationCode != phoneRequestId)
                      {
                        setState(() {
                          phoneErrorMessage = "error_messages.register_phoneVerification_not_valid".tr();
                        });
                        btnController.reset();
                        return;
                      }




                      UserModel userModelReq = widget.userModel!;
                      userModelReq.name = fullNameController.text;
                      userModelReq.imageFile = this.imageFile;
                      userModelReq.initialCountryCode = initialCountryCode;
                      userModelReq.phone = phoneController.text;
                      var result = await userServices.updateUser(userModelReq);
                      btnController.reset();
                      setState(() {
                        nameErrorMessage = null;
                      });
                      if (!result) {
                        FlushbarHelper.createError(
                                title: "popup_message.popup_error_title".tr(),
                                message: userServices.errorMessage)
                            .show(context);
                      } else {
                        await FlushbarHelper.createSuccess(
                          title: "popup_message.popup_success_title".tr(),
                          message: "popup_message.popup_success_message".tr(),
                        ).show(context);
                      }
                    },
                  ),
                ),
              ],
            ),
          ),
      ],
    );
  }

  showPicker(context) {
    showModalBottomSheet(
        context: context,
        builder: (BuildContext bc) {
          return SafeArea(
            child: Container(
              child: new Wrap(
                children: <Widget>[
                  new ListTile(
                      leading: new Icon(Icons.photo_library),
                      title: new Text("add_property_screen.from_gallery".tr()),
                      onTap: () async {
                        var file = await Utility.imgFromGallery(context);
                        Navigator.of(context).pop();
                        setState(() {
                          this.imageFile = file;
                        });
                      }),
                  new ListTile(
                    leading: new Icon(Icons.photo_camera),
                    title: new Text("add_property_screen.take_camera".tr()),
                    onTap: () async {
                      var file = await Utility.imgFromCamera(context);
                      Navigator.of(context).pop();
                      setState(() {
                        this.imageFile = file;
                      });
                    },
                  ),
                ],
              ),
            ),
          );
        });
  }
}
