import 'package:another_flushbar/flushbar_helper.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:rounded_loading_button/rounded_loading_button.dart';
import 'package:turklink/services/user_services.dart';
import 'package:turklink/util/turklink_colors.dart';
import 'package:turklink/views/widgets/turklink_button.dart';

class SettingsResetPasswordPopupWidget extends StatefulWidget {
  final BuildContext parentContext;
  SettingsResetPasswordPopupWidget({required this.parentContext});

  @override
  State<SettingsResetPasswordPopupWidget> createState() =>
      _SettingsResetPasswordPopupWidgetState();
}

class _SettingsResetPasswordPopupWidgetState
    extends State<SettingsResetPasswordPopupWidget> {
  var sendController = new RoundedLoadingButtonController();
  UserServices userServices = new UserServices();

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(12)),
      titlePadding: EdgeInsets.symmetric(vertical: 20, horizontal: 20),
      contentPadding: EdgeInsets.symmetric(vertical: 10, horizontal: 20),
      title: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Text(
            "setting_screen.reset_password_title".tr(),
            style: TextStyle(fontSize: 15),
          ),
          InkWell(
            onTap: () {
              Navigator.of(widget.parentContext).pop();
            },
            child: Icon(
              Icons.cancel,
              color: Colors.red,
              size: 28,
            ),
          )
        ],
      ),
      content: Wrap(
        children: [
          Container(
            child: Center(
              child: Text(
                "setting_screen.reset_password_text"
                    .tr(args: [FirebaseAuth.instance.currentUser!.email!]),style: TextStyle(fontSize: 13),textAlign: TextAlign.center,
              ),
            ),
          )
        ],
      ),
      actionsAlignment: MainAxisAlignment.start,
      actionsPadding: EdgeInsets.only(left: 10, right: 10, bottom: 10),
      actions: [
        SizedBox(
          height: 52,
          width: MediaQuery.of(context).size.width,
          child: TurklinkButton(
            width: MediaQuery.of(context).size.width,
            backgroundColor: TurklinkColors.mainColor,
            buttonText: "setting_screen.send_email_button".tr(),
            borderColor: TurklinkColors.mainColor2,
            textColor: Colors.white,
            btnController: sendController,
            animatedOnTap: true,
            onPressed: () async {
              var result = await userServices
                  .resetPassword(FirebaseAuth.instance.currentUser!.email!);
              sendController.reset();
              if (!result) {
                FlushbarHelper.createError(
                        title: "popup_message.popup_error_title".tr(),
                        message: userServices.errorMessage)
                    .show(widget.parentContext);
              } else {
                Navigator.of(widget.parentContext).pop();
                await FlushbarHelper.createSuccess(
                  title: "popup_message.popup_success_title".tr(),
                  message: "popup_message.popup_success_message".tr(),
                ).show(widget.parentContext);
              }
            },
          ),
        ),
      ],
    );
  }
}
