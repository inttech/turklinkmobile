import 'package:another_flushbar/flushbar_helper.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flag/flag.dart';
import 'package:flutter/material.dart';
import 'package:rounded_loading_button/rounded_loading_button.dart';
import 'package:turklink/models/user_model.dart';
import 'package:turklink/services/user_services.dart';
import 'package:turklink/util/turklink_colors.dart';
import 'package:turklink/views/widgets/turklink_button.dart';

class SettingsChangeLangPoupWidget extends StatefulWidget {
  final BuildContext parentContext;
  final UserModel userModel;
  SettingsChangeLangPoupWidget(
      {required this.parentContext, required this.userModel});

  @override
  State<SettingsChangeLangPoupWidget> createState() =>
      _SettingsChangeLangPoupWidgetState();
}

class _SettingsChangeLangPoupWidgetState
    extends State<SettingsChangeLangPoupWidget> {
  var sendController = new RoundedLoadingButtonController();
  UserServices userServices = new UserServices();

  String? langCode;

  @override
  void initState() {
    langCode = widget.userModel.langCode;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(12)),
      titlePadding: EdgeInsets.symmetric(vertical: 20, horizontal: 20),
      contentPadding: EdgeInsets.symmetric(vertical: 10, horizontal: 20),
      title: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Text(
            "setting_screen.change_lang_title".tr(),
            style: TextStyle(fontSize: 15),
          ),
          InkWell(
            onTap: () {
              Navigator.of(widget.parentContext).pop();
            },
            child: Icon(
              Icons.cancel,
              color: Colors.red,
              size: 28,
            ),
          )
        ],
      ),
      content: Wrap(
        children: [
          Column(
            children: [
              Container(
                child: ListTile(
                  onTap: (){
                    setState(() {
                      langCode = "tr";
                    });
                  },
                  leading: Flag.fromCode(
                    FlagsCode.TR,
                    width: 30,
                    height: 30,
                  ),
                  title: Text("setting_screen.turkish_lang".tr()),
                  trailing: langCode != null && langCode == "tr" ? Icon(Icons.check,color: Colors.green,) : Container(width: 5,height:5),
                ),
              ),
              SizedBox(
                height: 10,
              ),
              Container(
                child: ListTile(
                  onTap: (){
                    setState(() {
                      langCode = "en";
                    });
                  },
                  leading: Flag.fromCode(
                    FlagsCode.US,
                    width: 30,
                    height: 30,
                  ),
                  title: Text("setting_screen.english_lang".tr()),
                  trailing: langCode != null && langCode == "en" ? Icon(Icons.check,color: Colors.green,) : Container(width: 5,height:5),
                ),
              ),
              SizedBox(
                height: 10,
              ),
              /*Container(
                child: ListTile(
                  onTap: (){
                    setState(() {
                      langCode = "ar";
                    });
                  },
                  leading: Flag.fromCode(
                    FlagsCode.SA,
                    width: 30,
                    height: 30,
                  ),
                  title: Text("setting_screen.arabic_lang".tr()),
                  trailing: langCode != null && langCode == "ar" ? Icon(Icons.check,color: Colors.green,) : Container(width: 5,height:5),
                ),
              )*/
            ],
          )
        ],
      ),
      actionsAlignment: MainAxisAlignment.start,
      actionsPadding: EdgeInsets.only(left: 10, right: 10, bottom: 10),
      actions: [
        SizedBox(
          height: 52,
          width: MediaQuery.of(context).size.width,
          child: TurklinkButton(
            width: MediaQuery.of(context).size.width,
            backgroundColor: TurklinkColors.mainColor,
            buttonText: "setting_screen.send_email_button".tr(),
            borderColor: TurklinkColors.mainColor2,
            textColor: Colors.white,
            btnController: sendController,
            animatedOnTap: true,
            onPressed: () async {
              UserModel userModelReq = widget.userModel;
              userModelReq.langCode = langCode;
              var result = await userServices.updateUser(userModelReq);
              sendController.reset();
              if (!result) {
                FlushbarHelper.createError(
                        title: "popup_message.popup_error_title".tr(),
                        message: userServices.errorMessage)
                    .show(widget.parentContext);
              } else {

                Locale localLang = context.locale;
                if(langCode == "tr")
                  localLang = Locale("tr","TR");
                if(langCode == "en")
                  localLang = Locale("en","US");
                if(langCode == "ar")
                  localLang = Locale("ar","DZ");

                context.locale = localLang;

                Navigator.of(widget.parentContext).pop();
                await FlushbarHelper.createSuccess(
                  title: "popup_message.popup_success_title".tr(),
                  message: "popup_message.popup_success_message".tr(),
                ).show(widget.parentContext);
              }
            },
          ),
        ),
      ],
    );
  }
}
