import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:turklink/models/messages_model.dart';
import 'package:turklink/services/request_services.dart';
import 'package:turklink/util/request_status.dart';
import 'package:turklink/util/turklink_colors.dart';
import 'package:turklink/util/utility.dart';
import 'package:turklink/views/pages/main/service_center/service_center_new_chat.dart';
import 'package:turklink/views/pages/main/service_center/widgets/service_center_list_item_widget.dart';
import 'package:turklink/views/widgets/turklink_empty_screen.dart';

class ServiceCenterScreen extends StatefulWidget {
  @override
  _ServiceCenterScreenState createState() => _ServiceCenterScreenState();
}

class _ServiceCenterScreenState extends State<ServiceCenterScreen> {
  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      height: double.infinity,
      child: Scaffold(
        body: StreamBuilder(
          stream: FirebaseFirestore.instance
              .collection("messages")
              .where('userId',
                  isEqualTo: FirebaseAuth.instance.currentUser!.uid)
              .where("isDeleted", isEqualTo: 0)
              .where("status", isEqualTo: 0)
              .orderBy("updatedDate", descending: true)
              .snapshots(),
          builder:
              (BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot) {
            if (snapshot.connectionState == ConnectionState.active) {
              if (snapshot.hasData && snapshot.data!.docs.length > 0) {
                return ListView.builder(
                  itemCount: snapshot.data!.docs.length,
                  itemBuilder: (context, index) {
                    Map<String, dynamic> data = snapshot.data!.docs[index]
                        .data()! as Map<String, dynamic>;
                    var messagesModel = MessagesModel.fromJson(data);
                    return GestureDetector(
                      onTap: () {
                         Navigator.push(
                          context,
                          MaterialPageRoute(
                            builder: (context) => ServiceCenterNewChat(reasonType: messagesModel.title!,messageId: messagesModel.id!),
                          ),
                        );
                      },
                      child: ServiceCenterListItemWidget(messagesModel),
                    );
                  },
                );
              } else {
                return TurklinkEmptyScreen(
                  message: "service_center_screen.has_no_property".tr(),
                );
              }
            } else {
              return Center(child: CircularProgressIndicator());
            }
          },
        ),
        floatingActionButton: FloatingActionButton(
          onPressed: () async {
            RequestServices requestServices = new RequestServices();
            var requests = await requestServices.getRequestsByUser(FirebaseAuth.instance.currentUser!.uid);
            Utility.showChatReasonPopup(context,requests);
          },
          child: Icon(Icons.message),
          backgroundColor: TurklinkColors.mainColor,
        ),
      ),
    );
  }
}
