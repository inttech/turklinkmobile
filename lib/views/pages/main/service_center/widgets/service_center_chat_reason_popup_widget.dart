import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:rounded_loading_button/rounded_loading_button.dart';
import 'package:turklink/models/key_value_model.dart';
import 'package:turklink/models/request_model.dart';
import 'package:turklink/services/utility_services.dart';
import 'package:turklink/util/turklink_colors.dart';
import 'package:turklink/views/pages/main/service_center/service_center_new_chat.dart';
import 'package:turklink/views/widgets/turklink_button.dart';
import 'package:turklink/views/widgets/turklink_dropdown_field.dart';

class ServiceCenterChatReasonPopupWidget extends StatefulWidget {
  final BuildContext parentContext;
  final List<RequestModel>? requests;
  ServiceCenterChatReasonPopupWidget(
      {required this.parentContext,required this.requests});

  @override
  State<ServiceCenterChatReasonPopupWidget> createState() =>
      _ServiceCenterChatReasonPopupWidgetState();
}

class _ServiceCenterChatReasonPopupWidgetState
    extends State<ServiceCenterChatReasonPopupWidget> {

  var sendController = new RoundedLoadingButtonController();
  UtilityServices utilityServices = new UtilityServices();
  Color reasonColor = Colors.black87;
  Color requestColor = Colors.black87;
  String? reasonType;
  KeyValueModel? requestDetail;

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {


    return AlertDialog(
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(12)),
      titlePadding: EdgeInsets.symmetric(vertical: 20, horizontal: 20),
      contentPadding: EdgeInsets.symmetric(vertical: 10, horizontal: 20),
      title: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Text(
            "service_center_screen.chat_reason".tr(),
            style: TextStyle(fontSize: 15),
          ),
          InkWell(
            onTap: () {
              Navigator.of(widget.parentContext).pop();
            },
            child: Icon(
              Icons.cancel,
              color: Colors.red,
              size: 28,
            ),
          )
        ],
      ),
      content: Wrap(
        children: [
          SizedBox(
               height: 52,
               width: double.infinity,
               child: TurklinkDropdownField(
                 labelText: "service_center_screen.message_reason_type".tr(),
                 list: utilityServices.getMessageReason(widget.requests != null && widget.requests!.length > 0),
                 borderColor: reasonColor,
                 onChanged: (item) {
                   reasonType = item.key;
                   setState(() {

                   });
                 },
               ),
             ),
             Container(height: 10,),
             if(reasonType == "CURRENT_PROBLEM"  && widget.requests != null && widget.requests!.length > 0)
             SizedBox(
               height: 52,
               width: double.infinity,
               child: TurklinkDropdownField(
                 labelText: "service_center_screen.message_request_select".tr(),
                 list: utilityServices.convertRequestKeyValue(widget.requests!),
                 borderColor: requestColor,
                 onChanged: (item) {
                   requestDetail = item;
                 },
               ),
             ),
        ],
      ),
      actionsAlignment: MainAxisAlignment.start,
      actionsPadding: EdgeInsets.only(left: 10, right: 10, bottom: 10),
      actions: [
        SizedBox(
          height: 52,
          width: MediaQuery.of(context).size.width,
          child: TurklinkButton(
            width: MediaQuery.of(context).size.width,
            backgroundColor: TurklinkColors.mainColor,
            buttonText: "service_center_screen.continue".tr(),
            borderColor: TurklinkColors.mainColor2,
            textColor: Colors.white,
            btnController: sendController,
            animatedOnTap: false,
            onPressed: () async {

              if(reasonType ==  null)
              {
                setState(() {
                  reasonColor = Colors.red;
                });
                return;
              }

              if(reasonType == "CURRENT_PROBLEM" && widget.requests != null && widget.requests!.length > 0 && requestDetail ==  null)
              {
                setState(() {
                  requestColor = Colors.red;
                });
                return;
              }

               Navigator.of(widget.parentContext).pop();
               Navigator.push(
                widget.parentContext,
                MaterialPageRoute(
                  builder: (context) => ServiceCenterNewChat(reasonType: reasonType!,request: requestDetail,),
                ),
              );
            },
          ),
        ),
      ],
    );
  }
}
