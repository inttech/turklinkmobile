import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:turklink/models/messages_model.dart';
import 'package:turklink/util/turklink_colors.dart';

class ServiceCenterListItemWidget extends StatefulWidget {
  final MessagesModel messagesModel;
  const ServiceCenterListItemWidget(this.messagesModel);

  @override
  State<ServiceCenterListItemWidget> createState() =>
      _ServiceCenterListItemWidgetState();
}

class _ServiceCenterListItemWidgetState
    extends State<ServiceCenterListItemWidget> {
  @override
  Widget build(BuildContext context) {
    return Padding(
        padding: const EdgeInsets.only(left:15,right: 15,top: 15),
        child: Container(
          width: MediaQuery.of(context).size.width,
          padding: EdgeInsets.all(15),
          decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.circular(15),
          ),
          child: ListTile(
            contentPadding: EdgeInsets.all(0),
            leading: Container(
              width: 50,
              height: 50,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(20),
                color: TurklinkColors.mainColor,
              ),
              child: Center(
                child: Text(("utility." + widget.messagesModel.title!).tr().substring(0,1).toUpperCase(),style: TextStyle(color: Colors.white,fontSize: 18),),
              ),
            ),
            title: Text(("utility." + widget.messagesModel.title!).tr(),style: TextStyle(fontWeight: FontWeight.bold),),
            subtitle: Text(widget.messagesModel.lastMessage!.length > 12 ? widget.messagesModel.lastMessage!.substring(0,12) : widget.messagesModel.lastMessage!,style: TextStyle(fontSize: 11),)

          ),
        ),
      );
  }
}
