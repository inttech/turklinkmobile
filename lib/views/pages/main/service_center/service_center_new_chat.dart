import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter_chat_bubble/bubble_type.dart';
import 'package:flutter_chat_bubble/chat_bubble.dart';
import 'package:flutter_chat_bubble/clippers/chat_bubble_clipper_5.dart';
import 'package:turklink/models/chats_model.dart';
import 'package:turklink/models/key_value_model.dart';
import 'package:turklink/services/user_services.dart';
import 'package:turklink/util/turklink_colors.dart';

class ServiceCenterNewChat extends StatefulWidget {
  final String reasonType;
  final String? messageId;
  final KeyValueModel? request;
  const ServiceCenterNewChat({required this.reasonType, this.messageId,this.request});

  @override
  State<ServiceCenterNewChat> createState() => _ServiceCenterNewChatState();
}

class _ServiceCenterNewChatState extends State<ServiceCenterNewChat> {
  UserServices userServices = new UserServices();
  var messageController = TextEditingController();

  DateTime now = DateTime.now();
  DateFormat formatter = DateFormat('yyyyMMddHms');
  String messageInId = "";

  @override
  void initState() {
    if(widget.messageId == null)
      messageInId =  FirebaseAuth.instance.currentUser!.uid + "_" + formatter.format(now);
    else
      messageInId = widget.messageId!;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: TurklinkColors.grayBackground,
      appBar: AppBar(
        automaticallyImplyLeading: true,
        backgroundColor: TurklinkColors.mainColor,
        elevation: 0,
        centerTitle: true,
        title: Text(
          "service_center_screen.new_chat".tr(),
          style: TextStyle(color: Colors.white, fontWeight: FontWeight.w400),
        ),
      ),
      body: SafeArea(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.end,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Expanded(
              child: Container(
                padding: EdgeInsets.only(left: 10, right: 10, bottom: 5),
                child: messageInId != ""
                    ? StreamBuilder(
                        stream: FirebaseFirestore.instance
                            .collection("messages/" + messageInId + "/chats")
                            .where("isDeleted", isEqualTo: 0)
                            .orderBy("date", descending: true)
                            .snapshots(),
                        builder: (BuildContext context,
                            AsyncSnapshot<QuerySnapshot> snapshot) {
                          if (snapshot.connectionState ==
                              ConnectionState.active) {
                            if (snapshot.hasData &&
                                snapshot.data!.docs.length > 0) {
                              return ListView.builder(
                                reverse: true,
                                itemCount: snapshot.data!.docs.length,
                                itemBuilder: (context, index) {
                                  Map<String, dynamic> data =
                                      snapshot.data!.docs[index].data()!
                                          as Map<String, dynamic>;
                                  var messagesModel = ChatsModel.fromJson(data);
                                  return ChatBubble(
                                     clipper: ChatBubbleClipper5(type: messagesModel.userId == FirebaseAuth.instance.currentUser!.uid ? BubbleType.sendBubble : BubbleType.receiverBubble ),
                                    alignment: messagesModel.userId == FirebaseAuth.instance.currentUser!.uid ? Alignment.topRight : Alignment.topLeft,
                                    margin: EdgeInsets.only(top: 20),
                                    backGroundColor: messagesModel.userId == FirebaseAuth.instance.currentUser!.uid ? TurklinkColors.mainColor : TurklinkColors.mainColor2,
                                    child: Container(
                                      constraints: BoxConstraints(
                                        maxWidth:
                                            MediaQuery.of(context).size.width *
                                                0.7,
                                      ),
                                      child: Text(
                                        messagesModel.message!,
                                        style: TextStyle(color:  messagesModel.userId == FirebaseAuth.instance.currentUser!.uid ? Colors.white : Colors.black),
                                      ),
                                    ),
                                  );
                                },
                              );
                            } else {
                              return Text("");
                            }
                          } else {
                            return Center(child: CircularProgressIndicator());
                          }
                        },
                      )
                    : Container(),
              ),
            ),
            Container(
              height: 60,
              margin: EdgeInsets.only(top: 5),
              width: double.infinity,
              child: Padding(
                padding: const EdgeInsets.all(5),
                child: Row(
                  children: [
                    Expanded(
                      child: Container(
                        padding: EdgeInsets.only(left: 20, right: 10),
                        decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.circular(25),
                        ),
                        child: TextField(
                          controller: messageController,
                          decoration: InputDecoration(
                            hintText: "Mesaj",
                            border: InputBorder.none,
                          ),
                        ),
                      ),
                    ),
                    SizedBox(
                      width: 5,
                    ),
                    Container(
                      width: 50,
                      height: 50,
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(50),
                          color: TurklinkColors.mainColor),
                      child: Center(
                        child: IconButton(
                            onPressed: () async {
                              if (messageController.text.isNotEmpty) {
                                String message =  messageController.text;
                                messageController.text = "";
                                await userServices.sendMessage(
                                    widget.reasonType,
                                    message,
                                    (widget.messageId != null
                                        ? widget.messageId!
                                        : messageInId),
                                    true , widget.request);
                              }
                            },
                            icon: Icon(
                              Icons.send,
                              color: Colors.white,
                            )),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
