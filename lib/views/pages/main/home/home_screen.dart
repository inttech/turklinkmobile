import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter_statusbarcolor_ns/flutter_statusbarcolor_ns.dart';
import 'package:turklink/models/user_model.dart';
import 'package:turklink/services/utility_services.dart';
import 'package:turklink/turklink_router.dart';
import 'package:turklink/util/request_status.dart';
import 'package:turklink/util/turklink_colors.dart';
import 'package:turklink/util/utility.dart';
import 'package:turklink/views/pages/main/home/widgets/home_menu_card_widget.dart';
import 'package:turklink/views/pages/main/home/widgets/home_profile_card_widget.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

class HomeScreen extends StatefulWidget {
  @override
  _HomeScreenState createState() => _HomeScreenState();
}


class _HomeScreenState extends State<HomeScreen> {

  double usdValue = 0;
  UtilityServices utilityServices = new UtilityServices();

  @override
  void initState() {
    getUsdValue();
    super.initState();
  }



   getUsdValue() async
  {

    var dlv = await utilityServices.getUsdValue();
    setState(() {
      usdValue = dlv;
    });
  }

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Padding(
        padding: const EdgeInsets.only(left: 20, right: 20),
        child: Column(
          children: [
            SizedBox(
              height: 10,
            ),
            StreamBuilder(
              stream : FirebaseFirestore.instance.collection("users").doc(FirebaseAuth.instance.currentUser!.uid).snapshots(),
              builder: (BuildContext context, AsyncSnapshot snapshot) {

                if(snapshot.hasData)
                {
                  var data  = UserModel.fromJson(snapshot.data.data());
                  return HomeProfileCardWidget(userModel: data,);
                }

                return HomeProfileCardWidget(isLoading: true,);
              },
            ),
            SizedBox(
              height: 35,
            ),
            StreamBuilder(
              stream : FirebaseFirestore.instance.collection("properties").where('userId', isEqualTo: FirebaseAuth.instance.currentUser!.uid).snapshots(),
              builder: (BuildContext context, AsyncSnapshot snapshot) {

                if(snapshot.hasData)
                {
                  return HomeMenuCardWidget(
                    leftIcon: Icons.dynamic_feed_outlined,
                    title: "home_screen.my_properties".tr(),
                    right1Description: snapshot.data.docs.length.toString(),
                    right2Description: "",
                  );
                }

                return HomeMenuCardWidget(
                  leftIcon: Icons.dynamic_feed_outlined,
                  title: "home_screen.my_properties".tr(),
                  right1Description: "",
                  right2Description: "",
                );
              },
            ),
            SizedBox(
              height: 15,
            ),
            StreamBuilder(
              stream : FirebaseFirestore.instance.collection("users").doc(FirebaseAuth.instance.currentUser!.uid).snapshots(),
              builder: (BuildContext context, AsyncSnapshot snapshot) {

                if(snapshot.hasData)
                {
                  var data  = UserModel.fromJson(snapshot.data.data());
                  return HomeMenuCardWidget(
                      leftIcon: Icons.credit_card_outlined,
                      title: "home_screen.finances".tr(),
                      right1Description: (Utility.convertCurrency(data.tryBalance!) +  " TRY"),
                      right2Description: ("~ "+ (usdValue != 0 ? Utility.convertCurrency((data.tryBalance!/usdValue).toInt()) : Utility.convertCurrency(0))+" USD"),
                    );
                }

                return HomeMenuCardWidget(
                      leftIcon: Icons.credit_card_outlined,
                      title: "home_screen.finances".tr(),
                      right1Description: "0 TRY",
                      right2Description: "~ 0 USD",
                );
              },
            ),
            SizedBox(
              height: 15,
            ),
            StreamBuilder(
              stream : FirebaseFirestore.instance.collection("requests").where('userId', isEqualTo: FirebaseAuth.instance.currentUser!.uid).where("isDeleted", isEqualTo: 0)
                    .where("status",whereIn: [RequestStatus.pendingOffer,RequestStatus.waitingPayment,RequestStatus.onProgress]).snapshots(),
              builder: (BuildContext context, AsyncSnapshot snapshot) {

                if(snapshot.hasData)
                {
                  return  InkWell(
                    onTap: (){
                      
                    },
                    child: HomeMenuCardWidget(
                      leftIcon: Icons.email_outlined,
                      title: "home_screen.request_orders".tr(),
                      right1Description: snapshot.data.docs.length.toString(),
                      right2Description: "",
                    ),
                  );
                }

                return  HomeMenuCardWidget(
                  leftIcon: Icons.email_outlined,
                  title: "home_screen.request_orders".tr(),
                  right1Description: "",
                  right2Description: "",
                );
              },
            )
          ],
        ),
      ),
    );

  }
}
