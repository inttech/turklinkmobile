import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:turklink/models/user_model.dart';
import 'package:turklink/util/turklink_colors.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:turklink/views/pages/main/settings/profile_settings_screen.dart';

class HomeProfileCardWidget extends StatefulWidget {
  final UserModel? userModel;
  final bool? isLoading;

  const HomeProfileCardWidget({this.userModel, this.isLoading});

  @override
  _HomeProfileCardWidgetState createState() => _HomeProfileCardWidgetState();
}

class _HomeProfileCardWidgetState extends State<HomeProfileCardWidget> {
  @override
  Widget build(BuildContext context) {

    Locale local = context.locale;

    return Container(
      decoration: BoxDecoration(
          image: DecorationImage(
            image: new AssetImage('assets/images/bg.jpeg'),
            fit: BoxFit.cover,
          )
      ),
      child: Container(
        padding: EdgeInsets.all(20),
        height: MediaQuery.of(context).size.width / 2,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Expanded(
              child: Stack(
                children: [
                  Container(
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        Container(
                          width: 75,
                          height: 75,
                          margin: EdgeInsets.only(right: 15),
                          decoration: BoxDecoration(
                            color: Colors.white,
                            borderRadius: BorderRadius.circular(15),
                          ),
                          child: widget.userModel != null &&
                                  widget.userModel!.imagePath != null
                              ? ClipRRect(
                                  borderRadius: BorderRadius.circular(15),
                                  child: CachedNetworkImage(
                                    imageUrl: widget.userModel!.imagePath!,
                                    fit: BoxFit.cover,
                                    placeholder: (context, url) => Padding(
                                      padding: const EdgeInsets.all(15.0),
                                      child: CircularProgressIndicator(
                                        strokeWidth: 1,
                                        value: 1,
                                      ),
                                    ),
                                    errorWidget: (context, url, error) =>
                                        Icon(Icons.error),
                                  ),
                                )
                              : (widget.isLoading != null
                                  ? Container()
                                  : ClipRRect(
                                      borderRadius: BorderRadius.circular(15),
                                      child: Image(
                                        image: AssetImage(
                                          "assets/images/user.png",
                                        ),
                                      ),
                                    )),
                        ),
                        SizedBox(
                          width: 10,
                        ),
                        Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            SizedBox(
                              height: 10,
                            ),
                            Text(
                              "home_screen.hi_message".tr() +
                                  " " +
                                  (widget.userModel != null
                                      ? widget.userModel!.name!
                                      : ""),
                              style: TextStyle(color: Colors.white, fontSize: 15),
                            ),
                            if(widget.userModel != null && (widget.userModel!.citizenImagePath == null  || widget.userModel!.citizenImagePath == ""  || (widget.userModel!.isVerificationUser == null  || !widget.userModel!.isVerificationUser!)))
                            Padding(padding: EdgeInsets.only(
                              top: 0,
                              bottom: 2,                            
                            ),
                            child: Text("home_screen.waiting_verifiaction_message".tr(),style: TextStyle(color: Colors.red.withOpacity(.8), fontSize: 10),),
                            ),
                            Row(
                              children: [
                                Visibility(
                                  visible: (widget.userModel != null &&
                                      widget.userModel!.address != ""),
                                  child: Icon(
                                    Icons.location_pin,
                                    size: 13,
                                    color: Colors.white.withOpacity(.8),
                                  ),
                                ),
                                SizedBox(
                                  width: 5,
                                ),
                                Text(
                                  (widget.userModel != null
                                      ? widget.userModel!.address!
                                      : ""),
                                  style: TextStyle(
                                      color: Colors.white.withOpacity(.8),
                                      fontSize: 11,
                                      fontWeight: FontWeight.w100),
                                ),
                              ],
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
                  Positioned(
                    child: Container(
                      width: 20,
                      height: 20,
                      child: InkWell(
                          onTap: (){
                             Navigator.push(
                              context,
                              MaterialPageRoute(
                                builder: (context) => ProfileSettingsScreen(),
                              ),
                            );
                          },
                          child: Container(
                                            child: Align(
                            alignment: Alignment.topRight,
                            child: Icon(
                              Icons.edit_sharp,
                              color: Colors.white,
                              size: 18,
                            )),
                                          ),
                        ),
                    ),
                    top: 0,
                    left: local.languageCode == "ar" ? 0: null,
                    right: local.languageCode != "ar" ? 0: null,
                  )
                ],
              ),
            ),
            Expanded(
              child: Container(
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  crossAxisAlignment: CrossAxisAlignment.end,
                  children: [
                    Text(
                      (widget.userModel != null
                          ? widget.userModel!.cardNumber!
                          : ""),
                      style: TextStyle(
                          color: Colors.white.withOpacity(.8), fontSize: 11),
                    ),
                    Column(
                      mainAxisAlignment: MainAxisAlignment.end,
                      crossAxisAlignment: CrossAxisAlignment.end,
                      children: [
                        Text(
                          (widget.userModel != null
                              ? widget.userModel!.phone!
                              : ""),
                          style: TextStyle(
                              color: Colors.white.withOpacity(.8), fontSize: 11),
                        ),
                        Text(
                          (widget.userModel != null
                              ? widget.userModel!.email!
                              : ""),
                          style: TextStyle(
                              color: Colors.white.withOpacity(.8), fontSize: 11),
                        )
                      ],
                    )
                  ],
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
