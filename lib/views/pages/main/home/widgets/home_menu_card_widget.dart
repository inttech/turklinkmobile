import 'package:flutter/material.dart';
import 'package:turklink/util/turklink_colors.dart';

class HomeMenuCardWidget extends StatefulWidget {
  final IconData leftIcon;
  final String title;
  final String right1Description;
  final String right2Description;

  HomeMenuCardWidget(
      {required this.leftIcon,
      required this.title,
      required this.right1Description,
      required this.right2Description});

  @override
  _HomeMenuCardWidgetState createState() => _HomeMenuCardWidgetState();
}

class _HomeMenuCardWidgetState extends State<HomeMenuCardWidget> {
  @override
  Widget build(BuildContext context) {
    return Container(
      height: (MediaQuery.of(context).size.width / 16) * 5,
      decoration: BoxDecoration(
          color: Colors.white, borderRadius: BorderRadius.circular(15)),
      child: Align(
        alignment: Alignment.centerLeft,
        child: ListTile(
          leading: Container(
            width: 60,
            height: 60,
            decoration: BoxDecoration(
              color: TurklinkColors.mainColor,
              borderRadius: BorderRadius.circular(15),
            ),
            child: Icon(
              this.widget.leftIcon,
              color: Colors.blue
            ),
          ),
          title: Text(
            this.widget.title,
            style: TextStyle(
                color: TurklinkColors.mainColor,
                fontWeight: FontWeight.bold,
                fontSize: 18),
          ),
          trailing: this.widget.right2Description != ""
              ? Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.end,
                  children: [
                    Text(
                      this.widget.right1Description,
                      style: TextStyle(
                          color: Colors.grey,
                          fontSize: 18,
                          fontWeight: FontWeight.bold),
                    ),
                    Text(
                      this.widget.right2Description,
                      style: TextStyle(
                          color: Colors.grey,
                          fontSize: 12,
                          fontWeight: FontWeight.bold),
                    )
                  ],
                )
              : Text(
                  this.widget.right1Description,
                  style: TextStyle(
                      color: Colors.grey,
                      fontSize: 18,
                      fontWeight: FontWeight.bold),
                ),
        ),
      ),
    );
  }
}
