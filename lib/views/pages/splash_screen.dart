import 'package:flutter/material.dart';
import 'package:flutter_statusbarcolor_ns/flutter_statusbarcolor_ns.dart';
import 'package:turklink/util/turklink_colors.dart';
import 'package:turklink/views/pages/auth/login_screen.dart';

class SplashScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  double opacity = 0;
  double padding = 0;
  bool align = false;
  String image = "assets/images/logo_splash.gif";
  double logoWidth = 500;
  @override
  void initState() {
    super.initState();
    changeStatusBar();
    Future.delayed(const Duration(milliseconds: 4500), () => _setAnimationLogo());
    Future.delayed(const Duration(milliseconds: 4500), () => _setAnimation());
    Future.delayed(const Duration(milliseconds: 5500), () => _setAnimationOpacity());
  }

  _setAnimationLogo() { setState(() {
        image = "assets/images/logo.png";
        logoWidth = 200;
      });

  }

  _setAnimation() { setState(() {
        align = !align;
      });

  }

  _setAnimationOpacity() { setState(() {
        opacity = opacity == 0 ? 1 : 0;
      });

  }

  @override
  void dispose() {
    super.dispose();
  }

  changeStatusBar() async {
    await FlutterStatusbarcolor.setStatusBarColor(
        TurklinkColors.grayBackground);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: TurklinkColors.grayBackground,
      body: Container(
        child: Column(
          children: [
            AnimatedContainer(
              width: align ? MediaQuery.of(context).size.width : MediaQuery.of(context).size.width,
              height:  align ? 150: MediaQuery.of(context).size.height,
              duration: const Duration(milliseconds: 2000),
              alignment: align ? Alignment.bottomCenter : Alignment.center,
              child: Image(
                image: AssetImage(
                  image,
                ),
                width: logoWidth,
              ),
            ),
             opacity != 0 ? Expanded(
              child: AnimatedOpacity(
                opacity: opacity,
                curve: Curves.easeOutCirc,
                duration: const Duration(milliseconds: 10000),
                child:  LoginScreen(),
                ),
              ) : Container(
                
              )
          ],
        ),
      ),
    );
  }
}
