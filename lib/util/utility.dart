import 'dart:io';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:turklink/models/properties_model.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:turklink/models/request_model.dart';
import 'package:turklink/models/user_model.dart';
import 'package:turklink/util/lease_status.dart';
import 'package:turklink/util/property_process_status.dart';
import 'package:turklink/util/property_status.dart';
import 'package:turklink/views/pages/auth/widgets/register_kvk_popup_widget.dart';
import 'package:turklink/views/pages/main/finances/widgets/finances_invoice_show_download_popup_widget.dart';
import 'package:turklink/views/pages/main/finances/widgets/finances_take_money_popup_widget.dart';
import 'package:turklink/views/pages/main/my_properties/widgets/property_details_widgets/my_property_cancel_rent_popup_widget.dart';
import 'package:turklink/views/pages/main/my_properties/widgets/property_details_widgets/my_property_lease_popup_widget.dart';
import 'package:turklink/views/pages/main/my_properties/widgets/property_details_widgets/my_property_rent_amounts_popup_widget.dart';
import 'package:turklink/views/pages/main/my_properties/widgets/request_widgets/edit_request_popup_widget.dart';
import 'package:turklink/views/pages/main/my_properties/widgets/request_widgets/new_request_popup_widget.dart';
import 'package:turklink/views/pages/main/service_center/widgets/service_center_chat_reason_popup_widget.dart';
import 'package:turklink/views/pages/main/settings/widgets/settings_approve_citizen_popup_widget.dart';
import 'package:turklink/views/pages/main/settings/widgets/settings_bank_account_popup_widget.dart';
import 'package:turklink/views/pages/main/settings/widgets/settings_change_lang_popup_widget.dart';
import 'package:turklink/views/pages/main/settings/widgets/settings_remove_user_account_popup_widget.dart';
import 'package:turklink/views/pages/main/settings/widgets/settings_reset_password_popup_widget.dart';
import 'package:turklink/views/widgets/turklink_payment_method_popup_widget.dart';

class Utility {
  static bool isValidEmail(String email) {
    return RegExp(
            r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$')
        .hasMatch(email);
  }

  static imgFromCamera(context) async {
    ImagePicker picker = ImagePicker();
    final pickedFile = await picker.getImage(source: ImageSource.camera);
    if (pickedFile != null) {
      File image = File(pickedFile.path);
      return image;
    }
    return null;
  }

  static imgFromGallery(context) async {
    ImagePicker picker = ImagePicker();
    final pickedFile = await picker.getImage(source: ImageSource.gallery);
    if (pickedFile != null) {
      File image = File(pickedFile.path);
      return image;
    }
    return null;
  }

  static String getPropertyItemSubTitle(PropertiesModel propertiesModel) {
    if (propertiesModel.propertyType == null) return "";

    if (propertiesModel.propertyType!.key! == "FLAT" ||
        propertiesModel.propertyType!.key! == "VILLA" ||
        propertiesModel.propertyType!.key! == "OFFICE") {
      if (propertiesModel.propertyRoom != null)
        return propertiesModel.propertyRoom!.value! +
            " " +
            ("property_type." + propertiesModel.propertyType!.key!).tr();
      else if (propertiesModel.propertySize != null)
        return propertiesModel.propertySize! +
            " " +
            ("property_type." + propertiesModel.propertyType!.key!).tr();
      else
        return ("property_type." + propertiesModel.propertyType!.key!).tr();
    } else {
      if (propertiesModel.propertySize != null)
        return propertiesModel.propertySize! +
            " " +
            ("property_type." + propertiesModel.propertyType!.key!).tr();
      else
        return ("property_type." + propertiesModel.propertyType!.key!).tr();
    }
  }

  static String getPropertyItemLeasedContent(PropertiesModel propertiesModel) {

    if(propertiesModel.status == PropertyStatus.pendingApproval)
      return "Verifikasyon bekleniyor";
    else
    {
      if (propertiesModel.propertyType!.key! == "FLAT" ||
        propertiesModel.propertyType!.key! == "VILLA" ||
        propertiesModel.propertyType!.key! == "OFFICE" ||
        propertiesModel.propertyType!.key! == "OFFICE")
      {
          if(propertiesModel.propertyIsPersonelUse != null && propertiesModel.propertyIsPersonelUse!.key == "Y")
          {
              return "Şahsi Kullanımda";
          }
          else
          {
             if(propertiesModel.propertyRentFinishDate != null && propertiesModel.propertyRentAmount != null && propertiesModel.propertyHirer!.key == "Y")
             {
                var parsedDate = DateTime.parse(propertiesModel.propertyRentFinishDate! + ' 00:00:00.000');
                DateFormat formatter = DateFormat('MMM yyyy');
                return ("my_properties_screen.leased_content").tr(args: [
                  formatter.format(parsedDate),
                  (propertiesModel.propertyRentAmount! / 100).toString()
                ]);
             }
             else
             {
                if (propertiesModel.leaseDate == null || propertiesModel.leaseStatus != LeaseStatus.leased ) 
                {
                   if(propertiesModel.leaseProcessStatus == PropertyProcessStatus.pendingInfoFromAdmin)
                   {
                    return "Fiyat araştırması yapılıyor";
                   }
                   if(propertiesModel.leaseProcessStatus == PropertyProcessStatus.pendingAproveFromUser)
                   {
                     return "Turklink ile en hızlı şekilde kirala";
                   }
                   if(propertiesModel.leaseProcessStatus == PropertyProcessStatus.pendingProcessFromAdmin)
                   {
                     return "Listelenme bekleniyor";
                   }
                   if(propertiesModel.leaseProcessStatus == PropertyProcessStatus.complate && propertiesModel.leaseStatus == LeaseStatus.onListing)
                   {
                     return "Kiralanmak için " + (propertiesModel.leaseAmount! / 100).toString() + " bedelle listelendi";
                   }
                   
                }
                else
                {
                   if(propertiesModel.leaseProcessStatus == PropertyProcessStatus.complate && propertiesModel.leaseStatus == LeaseStatus.leased)
                   {
                      DateFormat formatter = DateFormat('MMM yyyy');
                      return ("my_properties_screen.leased_content").tr(args: [
                        formatter.format(propertiesModel.leaseDate!),
                        (propertiesModel.leaseAmount! / 100).toString()
                      ]);
                   }
                }
             }
          }
      }
      else
      {
         return "Proje geliştirilebilir";
      }
    }

    return "";
  }

  static Color getRequestTypeColor(String requestType)
  {
    Color color = Colors.deepPurple;
    if(requestType == "VALUATION_REPORT")
      color = Colors.deepOrange;
    if(requestType == "SALES_REQUEST")
      color = Colors.pinkAccent;
    if(requestType == "SERVICE_REQUEST")
      color = Colors.green;
    if(requestType == "CLEANING")
      color = Colors.blue;
    if(requestType == "EVICTION_REQUEST")
      color = Colors.deepPurple;
    return color;
  }

  static String convertCurrency(int balance)
  {
    var formatCurrency = new NumberFormat.simpleCurrency(
      locale: "tr"
    );

    return formatCurrency.format((balance/100)).replaceAll("TL", "") ;
  }

  static showLeasePopup(BuildContext context,PropertiesModel propertiesModel) {
    showDialog(
      barrierDismissible: false,
      context: context,
      builder: (BuildContext context) {
        return MyPropertyLeasePopupWidget(
          parentContext: context,
          propertiesModel: propertiesModel,
        );
      },
    );
  }

  static showRequestPopup(BuildContext context,PropertiesModel propertiesModel) {
    showDialog(
      barrierDismissible: false,
      context: context,
      builder: (BuildContext context) {
        return NewRequestPopupWidget(
          parentContext: context,
          propertiesModel: propertiesModel,
        );
      },
    );
  }

  static showRequestEditPopup(BuildContext context,RequestModel requestModel) {
    showDialog(
      barrierDismissible: false,
      context: context,
      builder: (BuildContext context) {
        return EditRequestPopupWidget(
          parentContext: context,
          requestModel: requestModel,
        );
      },
    );
  }

  static showResetPasswordPopup(BuildContext context) {
    showDialog(
      barrierDismissible: false,
      context: context,
      builder: (BuildContext context) {
        return SettingsResetPasswordPopupWidget(
          parentContext: context,
        );
      },
    );
  }

  static showChangeLangPopup(BuildContext context,UserModel userModel) {
    showDialog(
      barrierDismissible: false,
      context: context,
      builder: (BuildContext context) {
        return SettingsChangeLangPoupWidget(
          parentContext: context,
          userModel: userModel,
        );
      },
    );
  }

  static showChatReasonPopup(BuildContext context, List<RequestModel>? requests) {
    showDialog(
      barrierDismissible: false,
      context: context,
      builder: (BuildContext context) {
        return ServiceCenterChatReasonPopupWidget(
          parentContext: context,
          requests: requests,
        );
      },
    );
  }

  static showPaymentMethodPopup(BuildContext context,int? turkLinkBalance,int? financeType,int amount,{String? propertyId,String? requestId}) async{

    await showDialog(
      barrierDismissible: false,
      context: context,
      builder: (BuildContext context) {
        return TurklinkPaymentMethodPopupWidget(
          parentContext: context,
          financeType: financeType,
          turkLinkBalance: turkLinkBalance,
          amount: amount,
          propertyId: propertyId,
          requestId: requestId
        );
      },
    );
  }

  static showApproveCitizenPopup(BuildContext context,UserModel userModel) {
    showDialog(
      barrierDismissible: false,
      context: context,
      builder: (BuildContext context) {
        return SettingsApproveCitizenPopupWidget(
          parentContext: context,
          userModel: userModel,
        );
      },
    );
  }

  static showChangeBankAccountPopup(BuildContext context,UserModel userModel) {
    showDialog(
      barrierDismissible: false,
      context: context,
      builder: (BuildContext context) {
        return SettingsBankAccountPopupWidget(
          parentContext: context,
          userModel: userModel,
        );
      },
    );
  }

  static showTransferBankAccountPopup(BuildContext context,UserModel userModel) {
    showDialog(
      barrierDismissible: false,
      context: context,
      builder: (BuildContext context) {
        return FinancesTakeMoneyPopupWidget(
          parentContext: context,
          userModel: userModel,
        );
      },
    );
  }

  static showCancelRentPropertyPopup(BuildContext context,PropertiesModel propertiesModel, Function callBack) {
    showDialog(
      barrierDismissible: false,
      context: context,
      builder: (BuildContext context) {
        return MyPropertyCancelRentPopupWidget(
          parentContext: context,
          propertiesModel: propertiesModel,
          callBack: callBack,
        );
      },
    );
  }

  static showDocumentShowDownloadPopup(BuildContext context,String invoicePath) {
    showDialog(
      barrierDismissible: false,
      context: context,
      builder: (BuildContext context) {
        return FinancesInvoiceShowDownloadPopupWidget(invoicePath,context);
      },
    );
  }

  static showContractPopup(BuildContext context,String contractType) {
    showDialog(
      barrierDismissible: false,
      context: context,
      builder: (BuildContext context) {
        return RegisterKvkkPopupWidget(
          parentContext: context,
          contractType: contractType,
        );
      },
    );
  }

  static showRemoveUserAccountPopup(BuildContext context,UserModel userModel) {
    showDialog(
      barrierDismissible: false,
      context: context,
      builder: (BuildContext context) {
        return SettingsRemoveUserAccountPopupWidget(
          parentContext: context,
          userModel: userModel,
        );
      },
    );
  }

  static showRentPropertyAmountsPopup(BuildContext context,PropertiesModel propertiesModel,) {
    showDialog(
      barrierDismissible: false,
      context: context,
      builder: (BuildContext context) {
        return MyPropertyRentAmountsPopupWidget(
          parentContext: context,
          propertiesModel: propertiesModel,
        );
      },
    );
  }





}
