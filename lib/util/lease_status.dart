class LeaseStatus{
  static const pendingListing = 0;
  static const onListing = 1;
  static const leased = 2;
}