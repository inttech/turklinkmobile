class RequestStatus{
  static const pendingOffer = 0;
  static const waitingPayment = 1;
  static const onProgress = 2;
  static const closed = 3;
}