class PropertyProcessStatus{
  static const pendingInfoFromAdmin = 0;
  static const pendingAproveFromUser = 1;
  static const pendingProcessFromAdmin = 2;
  static const complate = 3;
}