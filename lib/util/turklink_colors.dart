import 'package:flutter/material.dart';

class TurklinkColors{
    static const grayBackground = Color(0xfff1f1f1);
    static const mainColor = Color(0xff352cab);
    static const mainColor2 = Color(0xff3dd39b);
}